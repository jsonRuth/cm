/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : cm

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

*/

USE `cm`;

SET FOREIGN_KEY_CHECKS=0;


-- 2019-07-02 niushaohui 修改tb_feedback表
ALTER TABLE tb_feedback ADD (
`read_status` BIT(1) DEFAULT b'0' COMMENT '状态：1已读，0未读',
`reply_content` VARCHAR(1024) DEFAULT NULL COMMENT '回复内容');
