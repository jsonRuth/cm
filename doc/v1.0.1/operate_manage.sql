/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : operate_manage

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

*/

USE `operate_manage`;

SET FOREIGN_KEY_CHECKS=0;


-- 2019-07-18 修改sys_log表
ALTER TABLE operate_manage.sys_log ADD (
  `operate_reason` VARCHAR(200) DEFAULT NULL COMMENT '操作理由',
  `operate_object` VARCHAR(55) DEFAULT NULL COMMENT '被操作对象',
  `is_test` BIT(1) DEFAULT NULL COMMENT '是否测试账号：0否，1是'
);

-- 2019-07-22 添加两个定时任务
insert into `sys_task` (`id`, `cronExpression`, `methodName`, `isConcurrent`, `description`, `updateBy`, `beanClass`, `createDate`, `jobStatus`, `jobGroup`, `updateDate`, `createBy`, `springBean`, `jobName`) values('12','0 0 0 * * ?','run1','1','每天凌晨执行',NULL,'com.shanli.job.jobs.GroupInfoJob','2019-07-17 11:02:18','1','group1','2019-07-17 11:02:23',NULL,NULL,'群组统计定时任务');
insert into `sys_task` (`id`, `cronExpression`, `methodName`, `isConcurrent`, `description`, `updateBy`, `beanClass`, `createDate`, `jobStatus`, `jobGroup`, `updateDate`, `createBy`, `springBean`, `jobName`) values('13','0 0 0 * * ?','run1','1','每天凌晨执行',NULL,'com.shanli.job.jobs.UserInfoJob',NULL,'1','group1',NULL,NULL,NULL,'用户统计定时任务');

-- 2019-07-22 添加菜单表新菜单项
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('216','0','业务管理','','','0','fa fa-briefcase','3',NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('217','216','群组统计管理','operate/groupStatistics','','1','fa fa-sitemap','0',NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('218','216','用户统计管理','operate/userStatistics','','1','fa fa-id-card','1',NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('219','217','查看','','operate:groupStatistics:groupStatistics','2','','1',NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('220','218','查看','','operate:userStatistics:userStatistics','2','','1',NULL,NULL);
