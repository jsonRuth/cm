/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : echat

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

*/

USE `echat`;

SET FOREIGN_KEY_CHECKS=0;

-- 2019-07-17 niushaohui 修改tb_user表
ALTER TABLE echat.tb_user ADD (
  `User_OldServiceBeginTime` DATETIME DEFAULT NULL COMMENT '帐户原始使用期限（起始日期）',
  `User_OldServiceEndTime` DATETIME DEFAULT NULL COMMENT '帐户原始使用期限（截至日期）'
);

-- 2019-07-25 修改tb_user表
ALTER TABLE echat.tb_user modify column service_month int(10) COMMENT '会员期限（单位：天）'
