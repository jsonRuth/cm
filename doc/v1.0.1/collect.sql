/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : collect

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

*/

USE `collect`;

SET FOREIGN_KEY_CHECKS=0;

-- 2019-07-19  新增表
CREATE TABLE `repo_groupinfo` (
  `id` bigint(20) NOT NULL,
  `group_name` varchar(64) DEFAULT NULL COMMENT '群组名称',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建账号',
  `group_users` int(10) DEFAULT NULL COMMENT '群组用户数',
  `group_type` tinyint(2) DEFAULT NULL COMMENT '群组类型：1.小于10人群组,2.10-30人,3.大于30人',
  `speech_status` bit(1) DEFAULT NULL COMMENT '对讲状态：0.禁言，1正常',
  `online_users` int(10) DEFAULT NULL COMMENT '群组在线用户数',
  `offline_users` int(10) DEFAULT NULL COMMENT '群组离线用户数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `isActive` tinyint(4) DEFAULT NULL COMMENT '删除标志1正常0删',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群组信息表';

CREATE TABLE `repo_userinfo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_account` varchar(32) NOT NULL COMMENT '用户帐号',
  `card_type` tinyint(2) DEFAULT NULL COMMENT '用户卡类型：1物联卡 2大王卡',
  `online_status` bit(1) DEFAULT b'0' COMMENT '用户状态：0离线，1在线',
  `province` varchar(30) DEFAULT NULL COMMENT '归属省份',
  `city` varchar(30) DEFAULT NULL COMMENT '归属城市',
  `carrier` varchar(30) DEFAULT NULL COMMENT '运营商名称：移动，联通，电信，其他',
  `sub_type` varchar(20) DEFAULT NULL COMMENT '渠道来源：3.普通用户,5.电渠用户,10001.toB来源用户,10002.网上商城销售卡,10003.线下渠道',
  `register_time` datetime DEFAULT NULL COMMENT '注册时间',
  `user_type` bit(1) DEFAULT b'0' COMMENT '用户类型：0免费，1会员',
  `service_type` tinyint(3) DEFAULT NULL COMMENT '付费类型:1) 3元/月, 2) 8元/季度, 3) 15元/半年, 4) 30元/年',
  `pay_time` datetime DEFAULT NULL COMMENT '付费时间',
  `account_status` bit(1) DEFAULT b'0' COMMENT '账号状态：0未禁用，1禁用',
  `is_test` bit(1) DEFAULT b'1' COMMENT '账号类型：0测试，1正式',
  `belong_group` int(10) DEFAULT NULL COMMENT '所属群组数',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `create_group` int(10) DEFAULT NULL COMMENT '创建群组数',
  `network_type` varchar(30) DEFAULT NULL COMMENT '网络类型，例如4G',
  `ip` varchar(30) DEFAULT NULL COMMENT 'IP',
  `login_fail` int(10) DEFAULT NULL COMMENT '连续登录失败次数',
  `risk_level` varchar(30) DEFAULT NULL COMMENT '风险等级',
  `sensitive_count` int(10) DEFAULT NULL COMMENT '敏感词次数',
  `device_id` varchar(100) DEFAULT NULL COMMENT '设备ID',
  `manufacturer` varchar(30) DEFAULT NULL COMMENT '设备品牌',
  `model` varchar(30) DEFAULT NULL COMMENT '设备品牌型号',
  `os` varchar(30) DEFAULT NULL COMMENT '设备系统',
  `os_version` varchar(30) DEFAULT NULL COMMENT '设备系统版本',
  `app_version` varchar(30) DEFAULT NULL COMMENT '软件版本',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `isActive` tinyint(4) DEFAULT NULL COMMENT '删除标志1正常0删',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_user_account` (`user_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';


