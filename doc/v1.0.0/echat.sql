/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : echat

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

*/

USE `echat`;

SET FOREIGN_KEY_CHECKS=0;

-- 2019-06-17 niushaohui 修改tb_user表
ALTER TABLE tb_user ADD (
  `is_test` BIT(1) DEFAULT b'0' COMMENT '是否测试账号：0否，1是',
  `account_type` TINYINT(4) DEFAULT '1' COMMENT '用户卡类型：1app账号，2物联卡',
  `expire_time` DATETIME DEFAULT NULL COMMENT '账号到期时间',
  `is_useable` BIT(1) DEFAULT b'0' COMMENT '是否禁用：0否，1是',
  `service_month` TINYINT(4) DEFAULT NULL COMMENT '会员期限（单位：月）'
)
