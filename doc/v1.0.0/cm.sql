/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : cm

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

*/

USE `cm`;

SET FOREIGN_KEY_CHECKS=0;


-- 2019-05-28 niushaohui 添加tb_importaccount表
CREATE TABLE `tb_importaccount` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account` varchar(15) DEFAULT NULL COMMENT '账号',
  `account_source` tinyint(1) DEFAULT NULL COMMENT '账号来源：1网上商城',
  `fee_type` tinyint(1) DEFAULT NULL COMMENT '功能费类型：1年卡会员-网上商城版',
  `buy_count` int(10) DEFAULT NULL COMMENT '购买数量',
  `account_status` tinyint(1) DEFAULT '0' COMMENT '状态：0未使用,1已使用',
  `is_delete` bit(1) DEFAULT b'0' COMMENT '是否删除：0未删除,1已删除',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='导入账号管理表'
