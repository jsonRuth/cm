/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : collect

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

*/

USE `collect`;

SET FOREIGN_KEY_CHECKS=0;

-- 2019-06-17 niushaohui 新增表
CREATE TABLE `repo_activegroup` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_count` int(10) DEFAULT NULL COMMENT '累计总数',
  `point_time` int(2) DEFAULT NULL COMMENT '时间点',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COMMENT='活跃群组报表';

CREATE TABLE `repo_feedback` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_count` int(10) DEFAULT NULL COMMENT '累计总数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='意见反馈报表';

CREATE TABLE `repo_onlineuser` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_count` int(10) DEFAULT NULL COMMENT '累计总数',
  `point_time` int(2) DEFAULT NULL COMMENT '时间点',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COMMENT='在线用户报表';

CREATE TABLE `repo_statisgroup` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_count` int(10) DEFAULT NULL COMMENT '累计总数',
  `data_type` tinyint(2) DEFAULT NULL COMMENT '类型：1.全部,2.小于10人群组,3.10-30人,4.大于30人',
  `statis_type` tinyint(2) DEFAULT NULL COMMENT '统计类型：1.到达值,2.活跃值',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='群组昨日统计报表';


CREATE TABLE `repo_statisuser` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_count` int(10) DEFAULT NULL COMMENT '累计总数',
  `data_type` tinyint(2) DEFAULT NULL COMMENT '类型：1.总用户,2.免费用户,3.会员用户',
  `statis_type` tinyint(2) DEFAULT NULL COMMENT '统计类型：1.到达值,2.活跃值,3新增值',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='用户昨日统计报表';

CREATE TABLE `repo_userlocklog` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_count` int(10) DEFAULT NULL COMMENT '累计总数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户登录失败锁定报表';

CREATE TABLE `repo_usersrouce` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_count` int(10) DEFAULT NULL COMMENT '累计总数',
  `sub_type` varchar(20) DEFAULT NULL COMMENT '同tb_customerinfo的SubType：3.普通用户,5.电渠用户,10001.toB来源用户,10003.网上商城销售卡,10004.线下渠道',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COMMENT='渠道来源统计报表';
