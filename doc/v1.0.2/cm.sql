/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : cm

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

*/

USE `cm`;

SET FOREIGN_KEY_CHECKS=0;


-- 2019-08-04  修改tb_feedback表
ALTER TABLE cm.`tb_feedback` ADD (
  `device_id` VARCHAR(100) DEFAULT NULL COMMENT '设备ID',
  `manufacturer` VARCHAR(30) DEFAULT NULL COMMENT '设备品牌',
  `model` VARCHAR(30) DEFAULT NULL COMMENT '设备品牌型号',
  `os` VARCHAR(30) DEFAULT NULL COMMENT '设备系统',
  `os_version` VARCHAR(30) DEFAULT NULL COMMENT '设备系统版本',
  `app_version` VARCHAR(30) DEFAULT NULL COMMENT '软件版本'
  );

-- 2019-08-07 修改tb_customerinfo表
ALTER TABLE `cm`.`tb_customerinfo` ADD (
`ProvinceName` VARCHAR(64) NULL COMMENT '客户归属省份名称',
`CityCode` VARCHAR(4) DEFAULT NULL COMMENT '客户所在城市编码'
);

-- 2019-08-12  修改tb_feedback表
ALTER TABLE cm.tb_feedback MODIFY COLUMN read_status INT(2) DEFAULT b'0'  COMMENT '状态：0未回复, 1已回复, 2 无需回复, 3 其他';

-- 2019-08-15 修改tb_feedback表
ALTER TABLE cm.`tb_feedback` ADD (
  `reply_account` VARCHAR(50) DEFAULT NULL COMMENT '回复账号',
  `recovery_time` DATETIME DEFAULT NULL COMMENT '回复时间'
  );

-- 2019-08-21 修改tb_customerinfo表
ALTER TABLE `cm`.`tb_customerinfo` ADD (
`IsYd` VARCHAR(3) NULL COMMENT '是否本网'
);