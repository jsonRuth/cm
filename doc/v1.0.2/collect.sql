/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : collect

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

*/

USE `collect`;

SET FOREIGN_KEY_CHECKS=0;

-- 2019-07-31  新增表
CREATE TABLE `repo_usercharge` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL COMMENT '用户id',
  `user_account` varchar(32) NOT NULL COMMENT '用户帐号',
  `card_type` tinyint(2) DEFAULT NULL COMMENT '用户卡类型：1物联卡 2大王卡',
  `province` varchar(30) DEFAULT NULL COMMENT '归属省份',
  `city` varchar(30) DEFAULT NULL COMMENT '归属城市',
  `carrier` varchar(30) DEFAULT NULL COMMENT '运营商名称：移动，联通，电信，其他',
  `sub_type` varchar(20) DEFAULT NULL COMMENT '渠道来源：3.普通用户,5.电渠用户,10001.toB来源用户,10002.网上商城销售卡,10003.线下渠道',
  `register_time` datetime DEFAULT NULL COMMENT '注册时间',
  `user_type` bit(1) DEFAULT b'0' COMMENT '用户类型：0免费，1会员',
  `service_type` tinyint(3) DEFAULT NULL COMMENT '付费类型:1) 3元/月, 2) 8元/季度, 3) 15元/半年, 4) 30元/年',
  `pay_time` datetime DEFAULT NULL COMMENT '付费时间',
  `is_test` bit(1) DEFAULT b'0' COMMENT '是否测试账号：0否，1是',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户缴费表';

CREATE TABLE `repo_userlogin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL COMMENT '用户id',
  `user_account` varchar(32) NOT NULL COMMENT '用户帐号',
  `card_type` tinyint(2) DEFAULT NULL COMMENT '用户卡类型：1物联卡 2大王卡',
  `province` varchar(30) DEFAULT NULL COMMENT '归属省份',
  `city` varchar(30) DEFAULT NULL COMMENT '归属城市',
  `carrier` varchar(30) DEFAULT NULL COMMENT '运营商名称：移动，联通，电信，其他',
  `sub_type` varchar(20) DEFAULT NULL COMMENT '渠道来源：3.普通用户,5.电渠用户,10001.toB来源用户,10002.网上商城销售卡,10003.线下渠道',
  `register_time` datetime DEFAULT NULL COMMENT '注册时间',
  `user_type` bit(1) DEFAULT b'0' COMMENT '用户类型：0免费，1会员',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `is_test` bit(1) DEFAULT b'0' COMMENT '是否测试账号：0否，1是',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户登录表';


-- 2019-08-01 用户缴费记录添加昨天之前的历史记录
INSERT INTO collect.`repo_usercharge` (user_id,user_account,card_type,province,city,carrier,sub_type,register_time,user_type,service_type,pay_time,is_test)
	SELECT user1.id AS user_id ,user1.`user_account` AS user_account ,user1.`card_type` AS  card_type ,user1.`province` AS province,user1.`city` AS city,
		user1.carrier AS carrier ,user1.`sub_type` AS sub_type ,user1.register_time AS register_time, user1.user_type AS user_type,
		h.service_type AS service_type,h.trans_time AS pay_time,user1.`is_test` AS is_test
	FROM collect.`repo_userinfo` user1,(
	SELECT g.`service_type`,g.`trans_time`,g.`OperatorPhone` FROM cm.`tb_orderinfo` g WHERE g.`status` IN (3,4,5) AND g.pay_status=3
		AND g.productCode <>'211000' AND DATEDIFF(g.create_time,NOW())<= -1 ORDER BY create_time DESC
		)h WHERE user1.`user_account` = h.OperatorPhone;


-- 2019-08-01 用户登录记录添加昨天之前的历史记录
INSERT INTO collect.`repo_userlogin` (user_id,user_account,card_type,province,city,carrier,sub_type,register_time,user_type,login_time,is_test)
  SELECT user1.id AS user_id ,user1.`User_Account` AS user_account ,user1.`card_type` AS card_type,
        pz.province AS province,pz.city AS city,(CASE WHEN pz.carrier = '移动' THEN '移动' WHEN pz.carrier ='联通' THEN '联通' WHEN pz.carrier ='电信' THEN '电信' ELSE '其他' END) carrier,
        user1.sub_type AS sub_type,user1.`register_time` AS register_time ,user1.`user_type` AS user_type
        ,pz.create_time AS login_time,user1.`is_test` AS is_test
        FROM collect.`repo_userinfo` user1
  JOIN (SELECT p.device_id,p.distinct_id,p.province,p.city,p.manufacturer,p.model,p.os,p.os_version,p.app_version,p.network_type,p.carrier,p.ip,p.create_time,p.succeed
        FROM (
        (SELECT a0.device_id,a0.distinct_id,a0.province,a0.city ,a0.manufacturer,a0.model,a0.os,a0.os_version,a0.app_version,a0.network_type,a0.carrier,ip,a0.create_time,a0.succeed
        FROM collect_0.`account_login_0` a0 ) UNION ALL
        (SELECT a1.device_id,a1.distinct_id,a1.province,a1.city ,a1.manufacturer,a1.model,a1.os,a1.os_version,a1.app_version,a1.network_type,a1.carrier,ip,a1.create_time,a1.succeed
        FROM collect_0.`account_login_1` a1 ) UNION ALL
        (SELECT a2.device_id,a2.distinct_id,a2.province,a2.city ,a2.manufacturer,a2.model,a2.os,a2.os_version,a2.app_version,a2.network_type,a2.carrier,ip,a2.create_time,a2.succeed
        FROM collect_0.`account_login_2` a2 ) UNION ALL
        (SELECT a3.device_id,a3.distinct_id,a3.province,a3.city ,a3.manufacturer,a3.model,a3.os,a3.os_version,a3.app_version,a3.network_type,a3.carrier,ip,a3.create_time,a3.succeed
        FROM collect_0.`account_login_3` a3 ) UNION ALL
        (SELECT a4.device_id,a4.distinct_id,a4.province,a4.city ,a4.manufacturer,a4.model,a4.os,a4.os_version,a4.app_version,a4.network_type,a4.carrier,ip,a4.create_time,a4.succeed
        FROM collect_0.`account_login_4` a4 ) UNION ALL
        (SELECT a5.device_id,a5.distinct_id,a5.province,a5.city ,a5.manufacturer,a5.model,a5.os,a5.os_version,a5.app_version,a5.network_type,a5.carrier,ip,a5.create_time,a5.succeed
        FROM collect_0.`account_login_5` a5 ) UNION ALL
        (SELECT a6.device_id,a6.distinct_id,a6.province,a6.city ,a6.manufacturer,a6.model,a6.os,a6.os_version,a6.app_version,a6.network_type,a6.carrier,ip,a6.create_time,a6.succeed
        FROM collect_0.`account_login_6` a6 ) UNION ALL
        (SELECT a7.device_id,a7.distinct_id,a7.province,a7.city ,a7.manufacturer,a7.model,a7.os,a7.os_version,a7.app_version,a7.network_type,a7.carrier,ip,a7.create_time,a7.succeed
        FROM collect_0.`account_login_7` a7 ) UNION ALL
        (SELECT a8.device_id,a8.distinct_id,a8.province,a8.city ,a8.manufacturer,a8.model,a8.os,a8.os_version,a8.app_version,a8.network_type,a8.carrier,ip,a8.create_time,a8.succeed
        FROM collect_0.`account_login_8` a8 ) UNION ALL
        (SELECT a9.device_id,a9.distinct_id,a9.province,a9.city ,a9.manufacturer,a9.model,a9.os,a9.os_version,a9.app_version,a9.network_type,a9.carrier,ip,a9.create_time,a9.succeed
        FROM collect_0.`account_login_9` a9 ) UNION ALL
        (SELECT b0.device_id,b0.distinct_id,b0.province,b0.city ,b0.manufacturer,b0.model,b0.os,b0.os_version,b0.app_version,b0.network_type,b0.carrier,ip,b0.create_time,b0.succeed
        FROM collect_1.`account_login_0` b0 ) UNION ALL
        (SELECT b1.device_id,b1.distinct_id,b1.province,b1.city ,b1.manufacturer,b1.model,b1.os,b1.os_version,b1.app_version,b1.network_type,b1.carrier,ip,b1.create_time,b1.succeed
        FROM collect_1.`account_login_1` b1 ) UNION ALL
        (SELECT b2.device_id,b2.distinct_id,b2.province,b2.city ,b2.manufacturer,b2.model,b2.os,b2.os_version,b2.app_version,b2.network_type,b2.carrier,ip,b2.create_time,b2.succeed
        FROM collect_1.`account_login_2` b2 ) UNION ALL
        (SELECT b3.device_id,b3.distinct_id,b3.province,b3.city ,b3.manufacturer,b3.model,b3.os,b3.os_version,b3.app_version,b3.network_type,b3.carrier,ip,b3.create_time,b3.succeed
        FROM collect_1.`account_login_3` b3 ) UNION ALL
        (SELECT b4.device_id,b4.distinct_id,b4.province,b4.city ,b4.manufacturer,b4.model,b4.os,b4.os_version,b4.app_version,b4.network_type,b4.carrier,ip,b4.create_time,b4.succeed
        FROM collect_1.`account_login_4` b4 ) UNION ALL
        (SELECT b5.device_id,b5.distinct_id,b5.province,b5.city ,b5.manufacturer,b5.model,b5.os,b5.os_version,b5.app_version,b5.network_type,b5.carrier,ip,b5.create_time,b5.succeed
        FROM collect_1.`account_login_5` b5 ) UNION ALL
        (SELECT b6.device_id,b6.distinct_id,b6.province,b6.city ,b6.manufacturer,b6.model,b6.os,b6.os_version,b6.app_version,b6.network_type,b6.carrier,ip,b6.create_time,b6.succeed
        FROM collect_1.`account_login_6` b6 ) UNION ALL
        (SELECT b7.device_id,b7.distinct_id,b7.province,b7.city ,b7.manufacturer,b7.model,b7.os,b7.os_version,b7.app_version,b7.network_type,b7.carrier,ip,b7.create_time,b7.succeed
        FROM collect_1.`account_login_7` b7 ) UNION ALL
        (SELECT b8.device_id,b8.distinct_id,b8.province,b8.city ,b8.manufacturer,b8.model,b8.os,b8.os_version,b8.app_version,b8.network_type,b8.carrier,ip,b8.create_time,b8.succeed
        FROM collect_1.`account_login_8` b8 ) UNION ALL
        (SELECT b9.device_id,b9.distinct_id,b9.province,b9.city ,b9.manufacturer,b9.model,b9.os,b9.os_version,b9.app_version,b9.network_type,b9.carrier,ip,b9.create_time,b9.succeed
        FROM collect_1.`account_login_9` b9 )
        )p WHERE p.succeed = 1 AND DATEDIFF(p.create_time,NOW()) <= -1 )pz
        ON pz.distinct_id = user1.`User_Account`;


-- 2019-08-01 修改repo_userinfo表
ALTER TABLE collect.repo_userinfo modify column is_test bit(1) DEFAULT b'0'  COMMENT '是否测试账号：0否，1是';


-- 2019-08-06 新增表
CREATE TABLE `repo_channelSource` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `province` VARCHAR(30) DEFAULT NULL COMMENT '归属省份',
  `three` int(10) DEFAULT b'0' COMMENT '3.普通用户',
  `five` int(10) DEFAULT b'0' COMMENT '5.电渠用户',
  `to_b` int(10) DEFAULT b'0' COMMENT '10001.toB来源用户',
  `ten_two` int(10) DEFAULT b'0' COMMENT '10002.网上商城销售卡',
  `ten_three` int(10) DEFAULT b'0'COMMENT '10003.线下渠道',
  `user_type` bit(1) DEFAULT b'0' COMMENT '用户类型：0免费，1会员',
  `create_time` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='渠道来源统计表';

-- -- 2019-08-06  新增表
-- CREATE TABLE `repo_CommonLogin` (
--   `id` INT(10) NOT NULL AUTO_INCREMENT,
--   `user_id` INT(10) NOT NULL COMMENT '用户id',
--   `user_account` VARCHAR(32) NOT NULL COMMENT '用户帐号',
--   `device_id` TINYINT(2) DEFAULT NULL COMMENT '设备ID',
--   `province` VARCHAR(30) DEFAULT NULL COMMENT '登录地区',
--   `local_create_time` DATETIME DEFAULT NULL COMMENT '地区创建时间',
--   `device_create_time` DATETIME DEFAULT NULL COMMENT '设备创建时间',
--   `create_time` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
--   `update_time` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
--   PRIMARY KEY (`id`)
-- ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='常用登录对照表';

-- 2019-08-15  新增表
CREATE TABLE `repo_activeGroupDetails` (
  `id` bigint(20) NOT NULL,
  `group_name` varchar(64) DEFAULT NULL COMMENT '群组名称',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建账号',
  `group_users` int(10) DEFAULT NULL COMMENT '群组用户数',
  `group_type` tinyint(2) DEFAULT NULL COMMENT '群组类型：1.小于10人群组,2.10-30人,3.大于30人',
  `speech_status` bit(1) DEFAULT NULL COMMENT '对讲状态：0.禁言，1正常',
  `online_users` int(10) DEFAULT NULL COMMENT '群组在线用户数',
  `offline_users` int(10) DEFAULT NULL COMMENT '群组离线用户数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `isActive` tinyint(4) DEFAULT NULL COMMENT '删除标志1正常0删',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群组活跃详情表';

-- 2019-08-16  新增表
CREATE TABLE `repo_activeUser` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `user_account` VARCHAR(32) NOT NULL COMMENT '用户帐号',
  `card_type` TINYINT(2) DEFAULT NULL COMMENT '用户卡类型：1物联卡 2大王卡',
  `online_status` BIT(1) DEFAULT b'0' COMMENT '用户状态：0离线，1在线',
  `province` VARCHAR(30) DEFAULT NULL COMMENT '归属省份',
  `city` VARCHAR(30) DEFAULT NULL COMMENT '归属城市',
  `carrier` VARCHAR(30) DEFAULT NULL COMMENT '运营商名称：移动，联通，电信，其他',
  `sub_type` VARCHAR(20) DEFAULT NULL COMMENT '渠道来源：3.普通用户,5.电渠用户,10001.toB来源用户,10002.网上商城销售卡,10003.线下渠道',
  `register_time` DATETIME DEFAULT NULL COMMENT '注册时间',
  `user_type` BIT(1) DEFAULT b'0' COMMENT '用户类型：0免费，1会员',
  `service_type` TINYINT(3) DEFAULT NULL COMMENT '付费类型:1) 3元/月, 2) 8元/季度, 3) 15元/半年, 4) 30元/年',
  `pay_time` DATETIME DEFAULT NULL COMMENT '付费时间',
  `account_status` BIT(1) DEFAULT b'0' COMMENT '账号状态：0未禁用，1禁用',
  `is_test` BIT(1) DEFAULT b'1' COMMENT '账号类型：0测试，1正式',
  `belong_group` INT(10) DEFAULT NULL COMMENT '所属群组数',
  `login_time` DATETIME DEFAULT NULL COMMENT '登录时间',
  `create_group` INT(10) DEFAULT NULL COMMENT '创建群组数',
  `network_type` VARCHAR(30) DEFAULT NULL COMMENT '网络类型，例如4G',
  `ip` VARCHAR(30) DEFAULT NULL COMMENT 'IP',
  `login_fail` INT(10) DEFAULT NULL COMMENT '连续登录失败次数',
  `risk_level` VARCHAR(30) DEFAULT NULL COMMENT '风险等级',
  `sensitive_count` INT(10) DEFAULT NULL COMMENT '敏感词次数',
  `device_id` VARCHAR(100) DEFAULT NULL COMMENT '设备ID',
  `manufacturer` VARCHAR(30) DEFAULT NULL COMMENT '设备品牌',
  `model` VARCHAR(30) DEFAULT NULL COMMENT '设备品牌型号',
  `os` VARCHAR(30) DEFAULT NULL COMMENT '设备系统',
  `os_version` VARCHAR(30) DEFAULT NULL COMMENT '设备系统版本',
  `app_version` VARCHAR(30) DEFAULT NULL COMMENT '软件版本',
  `create_time` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `isActive` TINYINT(4) DEFAULT NULL COMMENT '删除标志1正常0删',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_user_account` (`user_account`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='用户活跃详情表';


