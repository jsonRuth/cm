/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : echat

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

*/

USE `echat`;

SET FOREIGN_KEY_CHECKS=0;

-- 2019-07-29 shenliang 修改tb_chatgroup表
ALTER TABLE echat.tb_chatgroup ADD (
  `Cg_speech_status` BIT(1) DEFAULT b'1' NOT NULL COMMENT '禁言状态：0.禁言，1正常',
  `Cg_banned_time` DATETIME DEFAULT NULL COMMENT '群组禁言时间',
  `Cg_banned_length` INT(10) DEFAULT NULL COMMENT '群组禁言时长(小时)'
);

-- 2019-07-30 shenliang 修改tb_user表
ALTER TABLE echat.tb_user ADD (
  `User_banned_time` DATETIME DEFAULT NULL COMMENT '用户禁用时间',
  `User_banned_length` INT(10) DEFAULT NULL COMMENT '用户禁用时长(小时)'
);


-- 2019-08-09 shenliang 修改tb_user表
ALTER TABLE echat.tb_user ADD (
  `test_create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '测试账号创建时间'
);

-- 拷贝之前的创建时间
update echat.tb_user set test_create_time = create_time;
