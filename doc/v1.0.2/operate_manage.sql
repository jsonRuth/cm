/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : operate_manage

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

*/

USE `operate_manage`;

SET FOREIGN_KEY_CHECKS=0;



-- 2019-08-01 添加两个定时任务
insert into `sys_task` (`id`, `cronExpression`, `methodName`, `isConcurrent`, `description`, `updateBy`, `beanClass`, `createDate`, `jobStatus`, `jobGroup`, `updateDate`, `createBy`, `springBean`, `jobName`) values('14','0 0 1 * * ?','run1','1','每天1点凌晨执行',NULL,'com.shanli.job.jobs.UserLoginLogJob','2019-07-31 17:22:08','1','group1','2019-07-31 17:22:11',NULL,NULL,'用户登录统计定时任务');
insert into `sys_task` (`id`, `cronExpression`, `methodName`, `isConcurrent`, `description`, `updateBy`, `beanClass`, `createDate`, `jobStatus`, `jobGroup`, `updateDate`, `createBy`, `springBean`, `jobName`) values('15','0 0 1 * * ?','run1','1','每天1点凌晨执行',NULL,'com.shanli.job.jobs.UserChargeJob','2019-07-31 19:06:50','1','group1','2019-07-31 19:06:58',NULL,NULL,'用户缴费统计定时任务');
insert into `sys_task` (`id`, `cronExpression`, `methodName`, `isConcurrent`, `description`, `updateBy`, `beanClass`, `createDate`, `jobStatus`, `jobGroup`, `updateDate`, `createBy`, `springBean`, `jobName`) values('16','0 0 1 * * ?','run1','1','每天1点凌晨执行',NULL,'com.shanli.job.jobs.ChannelSourceJob','2019-08-07 14:10:27','1','group1','2019-08-07 14:10:34',NULL,NULL,'渠道来源统计定时任务');

-- 2019-08-01 添加菜单表新菜单项
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('223','217','解散群组','','operate:groupStatistics:dissolve','2','',NULL,NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('224','217','禁言','','operate:groupStatistics:banned','2','',NULL,NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('225','217','解禁','','operate:groupStatistics:removeBanned','2','',NULL,NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('226','218','禁用','','operate:userStatistics:userBanned','2','',NULL,NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('227','218','恢复','','operate:userStatistics:userRecover','2','',NULL,NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('228','91','用户登录日志','common/loginLog','common:loginLog','1','fa fa-thumbs-up','2',NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('229','228','查看','','common:loginLog:loginLog','2','','0',NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('230','91','用户缴费日志','common/captureLog','common:captureLog','1','fa fa-asl-interpreting','3',NULL,NULL);
insert into `sys_menu` (`id`, `parentId`, `name`, `url`, `perms`, `type`, `icon`, `orderNum`, `gmtCreate`, `gmtModified`) values('231','230','查看','','common:captureLog:captureLog','2','','0',NULL,NULL);


UPDATE operate_manage.sys_menu set name='组织管理' where name = '部门管理';
UPDATE operate_manage.sys_menu SET NAME='权限管理' WHERE NAME = '角色管理';
UPDATE operate_manage.sys_menu SET NAME='基本设置' WHERE NAME = '系统管理';

UPDATE operate_manage.`sys_menu` set orderNum = '0' where name = '业务管理' ;
UPDATE operate_manage.`sys_menu` SET orderNum = '1' WHERE NAME = '用户账号管理' ;
UPDATE operate_manage.`sys_menu` SET orderNum = '2' ,name = '行为记录' WHERE NAME = '系统监控' ;
UPDATE operate_manage.`sys_menu` SET orderNum = '3' WHERE NAME = '基本设置' ;
UPDATE operate_manage.`sys_menu` SET orderNum = '4' WHERE NAME = '基础管理' ;
UPDATE operate_manage.`sys_menu` SET orderNum = '5' WHERE NAME = '系统工具' ;
UPDATE operate_manage.`sys_menu` SET orderNum = '6' WHERE NAME = '图表管理' ;



-- 2019-08-01 修改sys_log表 字段
ALTER TABLE operate_manage.sys_log modify column is_test bit(1) DEFAULT b'0'  COMMENT '是否测试账号：0否，1是';

-- 2019-08-03 修改sys_log表 字段
ALTER TABLE operate_manage.sys_log modify column operate_object varchar(5000) null  COMMENT '被操作对象';



-- 2019-08-15 修改菜单名称
UPDATE operate_manage.`sys_menu` SET NAME = '基础设置'  WHERE NAME = '基本设置' ;

-- 2019-08-15 修改定时任务时间
UPDATE operate_manage.`sys_task` SET cronExpression = '0 0 1 * * ?' ,description ='每天1点凌晨执行' WHERE jobName = '群组昨日关键数据定时任务' ;
UPDATE operate_manage.`sys_task` SET cronExpression = '0 0 1 * * ?' ,description ='每天1点凌晨执行' WHERE jobName = '用户昨日关键数据定时任务' ;