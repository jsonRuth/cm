/*
Navicat MySQL Data Transfer

Source Server         : cm-manager
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : collect_1

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-19 15:05:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accept_friend_0
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_0`;
CREATE TABLE `accept_friend_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_0
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_1
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_1`;
CREATE TABLE `accept_friend_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_1
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_2
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_2`;
CREATE TABLE `accept_friend_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_2
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_3
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_3`;
CREATE TABLE `accept_friend_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_3
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_4
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_4`;
CREATE TABLE `accept_friend_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_4
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_5
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_5`;
CREATE TABLE `accept_friend_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_5
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_6
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_6`;
CREATE TABLE `accept_friend_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_6
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_7
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_7`;
CREATE TABLE `accept_friend_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_7
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_8
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_8`;
CREATE TABLE `accept_friend_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_8
-- ----------------------------

-- ----------------------------
-- Table structure for accept_friend_9
-- ----------------------------
DROP TABLE IF EXISTS `accept_friend_9`;
CREATE TABLE `accept_friend_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_friend_9
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_0
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_0`;
CREATE TABLE `accept_group_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_0
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_1
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_1`;
CREATE TABLE `accept_group_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_1
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_2
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_2`;
CREATE TABLE `accept_group_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_2
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_3
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_3`;
CREATE TABLE `accept_group_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_3
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_4
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_4`;
CREATE TABLE `accept_group_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_4
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_5
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_5`;
CREATE TABLE `accept_group_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_5
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_6
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_6`;
CREATE TABLE `accept_group_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_6
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_7
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_7`;
CREATE TABLE `accept_group_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_7
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_8
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_8`;
CREATE TABLE `accept_group_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_8
-- ----------------------------

-- ----------------------------
-- Table structure for accept_group_9
-- ----------------------------
DROP TABLE IF EXISTS `accept_group_9`;
CREATE TABLE `accept_group_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `acceptFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'acceptFriendID',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accept_group_9
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_0
-- ----------------------------
DROP TABLE IF EXISTS `account_login_0`;
CREATE TABLE `account_login_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_0
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_1
-- ----------------------------
DROP TABLE IF EXISTS `account_login_1`;
CREATE TABLE `account_login_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_1
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_2
-- ----------------------------
DROP TABLE IF EXISTS `account_login_2`;
CREATE TABLE `account_login_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_2
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_3
-- ----------------------------
DROP TABLE IF EXISTS `account_login_3`;
CREATE TABLE `account_login_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_3
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_4
-- ----------------------------
DROP TABLE IF EXISTS `account_login_4`;
CREATE TABLE `account_login_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_4
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_5
-- ----------------------------
DROP TABLE IF EXISTS `account_login_5`;
CREATE TABLE `account_login_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_5
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_6
-- ----------------------------
DROP TABLE IF EXISTS `account_login_6`;
CREATE TABLE `account_login_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_6
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_7
-- ----------------------------
DROP TABLE IF EXISTS `account_login_7`;
CREATE TABLE `account_login_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_7
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_8
-- ----------------------------
DROP TABLE IF EXISTS `account_login_8`;
CREATE TABLE `account_login_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_8
-- ----------------------------

-- ----------------------------
-- Table structure for account_login_9
-- ----------------------------
DROP TABLE IF EXISTS `account_login_9`;
CREATE TABLE `account_login_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `loginMethod` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录方式',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_login_9
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_0
-- ----------------------------
DROP TABLE IF EXISTS `account_register_0`;
CREATE TABLE `account_register_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_0
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_1
-- ----------------------------
DROP TABLE IF EXISTS `account_register_1`;
CREATE TABLE `account_register_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_1
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_2
-- ----------------------------
DROP TABLE IF EXISTS `account_register_2`;
CREATE TABLE `account_register_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_2
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_3
-- ----------------------------
DROP TABLE IF EXISTS `account_register_3`;
CREATE TABLE `account_register_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_3
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_4
-- ----------------------------
DROP TABLE IF EXISTS `account_register_4`;
CREATE TABLE `account_register_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_4
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_5
-- ----------------------------
DROP TABLE IF EXISTS `account_register_5`;
CREATE TABLE `account_register_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_5
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_6
-- ----------------------------
DROP TABLE IF EXISTS `account_register_6`;
CREATE TABLE `account_register_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_6
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_7
-- ----------------------------
DROP TABLE IF EXISTS `account_register_7`;
CREATE TABLE `account_register_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_7
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_8
-- ----------------------------
DROP TABLE IF EXISTS `account_register_8`;
CREATE TABLE `account_register_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_8
-- ----------------------------

-- ----------------------------
-- Table structure for account_register_9
-- ----------------------------
DROP TABLE IF EXISTS `account_register_9`;
CREATE TABLE `account_register_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否成功',
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_register_9
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_0
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_0`;
CREATE TABLE `add_friend_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_0
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_1
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_1`;
CREATE TABLE `add_friend_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_1
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_2
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_2`;
CREATE TABLE `add_friend_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_2
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_3
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_3`;
CREATE TABLE `add_friend_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_3
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_4
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_4`;
CREATE TABLE `add_friend_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_4
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_5
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_5`;
CREATE TABLE `add_friend_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_5
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_6
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_6`;
CREATE TABLE `add_friend_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_6
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_7
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_7`;
CREATE TABLE `add_friend_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_7
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_8
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_8`;
CREATE TABLE `add_friend_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_8
-- ----------------------------

-- ----------------------------
-- Table structure for add_friend_9
-- ----------------------------
DROP TABLE IF EXISTS `add_friend_9`;
CREATE TABLE `add_friend_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addFriendID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_friend_9
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_0
-- ----------------------------
DROP TABLE IF EXISTS `add_group_0`;
CREATE TABLE `add_group_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_0
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_1
-- ----------------------------
DROP TABLE IF EXISTS `add_group_1`;
CREATE TABLE `add_group_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_1
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_2
-- ----------------------------
DROP TABLE IF EXISTS `add_group_2`;
CREATE TABLE `add_group_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_2
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_3
-- ----------------------------
DROP TABLE IF EXISTS `add_group_3`;
CREATE TABLE `add_group_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_3
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_4
-- ----------------------------
DROP TABLE IF EXISTS `add_group_4`;
CREATE TABLE `add_group_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_4
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_5
-- ----------------------------
DROP TABLE IF EXISTS `add_group_5`;
CREATE TABLE `add_group_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_5
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_6
-- ----------------------------
DROP TABLE IF EXISTS `add_group_6`;
CREATE TABLE `add_group_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_6
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_7
-- ----------------------------
DROP TABLE IF EXISTS `add_group_7`;
CREATE TABLE `add_group_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_7
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_8
-- ----------------------------
DROP TABLE IF EXISTS `add_group_8`;
CREATE TABLE `add_group_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_8
-- ----------------------------

-- ----------------------------
-- Table structure for add_group_9
-- ----------------------------
DROP TABLE IF EXISTS `add_group_9`;
CREATE TABLE `add_group_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addGroupUserIDs` text COLLATE utf8mb4_unicode_ci COMMENT '邀请好友进组ids',
  `addGroupUserCount` text COLLATE utf8mb4_unicode_ci COMMENT '添加好友IDid个数',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of add_group_9
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_0
-- ----------------------------
DROP TABLE IF EXISTS `app_end_0`;
CREATE TABLE `app_end_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_0
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_1
-- ----------------------------
DROP TABLE IF EXISTS `app_end_1`;
CREATE TABLE `app_end_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_1
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_2
-- ----------------------------
DROP TABLE IF EXISTS `app_end_2`;
CREATE TABLE `app_end_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_2
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_3
-- ----------------------------
DROP TABLE IF EXISTS `app_end_3`;
CREATE TABLE `app_end_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_3
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_4
-- ----------------------------
DROP TABLE IF EXISTS `app_end_4`;
CREATE TABLE `app_end_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_4
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_5
-- ----------------------------
DROP TABLE IF EXISTS `app_end_5`;
CREATE TABLE `app_end_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_5
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_6
-- ----------------------------
DROP TABLE IF EXISTS `app_end_6`;
CREATE TABLE `app_end_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_6
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_7
-- ----------------------------
DROP TABLE IF EXISTS `app_end_7`;
CREATE TABLE `app_end_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_7
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_8
-- ----------------------------
DROP TABLE IF EXISTS `app_end_8`;
CREATE TABLE `app_end_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_8
-- ----------------------------

-- ----------------------------
-- Table structure for app_end_9
-- ----------------------------
DROP TABLE IF EXISTS `app_end_9`;
CREATE TABLE `app_end_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `event_duration` bigint(20) DEFAULT '0' COMMENT '停留时长',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_end_9
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_0
-- ----------------------------
DROP TABLE IF EXISTS `app_install_0`;
CREATE TABLE `app_install_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_0
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_1
-- ----------------------------
DROP TABLE IF EXISTS `app_install_1`;
CREATE TABLE `app_install_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_1
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_2
-- ----------------------------
DROP TABLE IF EXISTS `app_install_2`;
CREATE TABLE `app_install_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_2
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_3
-- ----------------------------
DROP TABLE IF EXISTS `app_install_3`;
CREATE TABLE `app_install_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_3
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_4
-- ----------------------------
DROP TABLE IF EXISTS `app_install_4`;
CREATE TABLE `app_install_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_4
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_5
-- ----------------------------
DROP TABLE IF EXISTS `app_install_5`;
CREATE TABLE `app_install_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_5
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_6
-- ----------------------------
DROP TABLE IF EXISTS `app_install_6`;
CREATE TABLE `app_install_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_6
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_7
-- ----------------------------
DROP TABLE IF EXISTS `app_install_7`;
CREATE TABLE `app_install_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_7
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_8
-- ----------------------------
DROP TABLE IF EXISTS `app_install_8`;
CREATE TABLE `app_install_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_8
-- ----------------------------

-- ----------------------------
-- Table structure for app_install_9
-- ----------------------------
DROP TABLE IF EXISTS `app_install_9`;
CREATE TABLE `app_install_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_install_9
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_0
-- ----------------------------
DROP TABLE IF EXISTS `app_start_0`;
CREATE TABLE `app_start_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_0
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_1
-- ----------------------------
DROP TABLE IF EXISTS `app_start_1`;
CREATE TABLE `app_start_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_1
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_2
-- ----------------------------
DROP TABLE IF EXISTS `app_start_2`;
CREATE TABLE `app_start_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_2
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_3
-- ----------------------------
DROP TABLE IF EXISTS `app_start_3`;
CREATE TABLE `app_start_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_3
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_4
-- ----------------------------
DROP TABLE IF EXISTS `app_start_4`;
CREATE TABLE `app_start_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_4
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_5
-- ----------------------------
DROP TABLE IF EXISTS `app_start_5`;
CREATE TABLE `app_start_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_5
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_6
-- ----------------------------
DROP TABLE IF EXISTS `app_start_6`;
CREATE TABLE `app_start_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_6
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_7
-- ----------------------------
DROP TABLE IF EXISTS `app_start_7`;
CREATE TABLE `app_start_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_7
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_8
-- ----------------------------
DROP TABLE IF EXISTS `app_start_8`;
CREATE TABLE `app_start_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_8
-- ----------------------------

-- ----------------------------
-- Table structure for app_start_9
-- ----------------------------
DROP TABLE IF EXISTS `app_start_9`;
CREATE TABLE `app_start_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  `is_first_time` bit(1) DEFAULT b'0' COMMENT '是否首次访问',
  `resume_from_background` bit(1) DEFAULT b'0' COMMENT '是否从后台唤醒',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_start_9
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_0
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_0`;
CREATE TABLE `app_view_screen_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_0
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_1
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_1`;
CREATE TABLE `app_view_screen_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_1
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_2
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_2`;
CREATE TABLE `app_view_screen_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_2
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_3
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_3`;
CREATE TABLE `app_view_screen_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_3
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_4
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_4`;
CREATE TABLE `app_view_screen_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_4
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_5
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_5`;
CREATE TABLE `app_view_screen_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_5
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_6
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_6`;
CREATE TABLE `app_view_screen_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_6
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_7
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_7`;
CREATE TABLE `app_view_screen_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_7
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_8
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_8`;
CREATE TABLE `app_view_screen_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_8
-- ----------------------------

-- ----------------------------
-- Table structure for app_view_screen_9
-- ----------------------------
DROP TABLE IF EXISTS `app_view_screen_9`;
CREATE TABLE `app_view_screen_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面标题',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of app_view_screen_9
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_0
-- ----------------------------
DROP TABLE IF EXISTS `change_name_0`;
CREATE TABLE `change_name_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_0
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_1
-- ----------------------------
DROP TABLE IF EXISTS `change_name_1`;
CREATE TABLE `change_name_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_1
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_2
-- ----------------------------
DROP TABLE IF EXISTS `change_name_2`;
CREATE TABLE `change_name_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_2
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_3
-- ----------------------------
DROP TABLE IF EXISTS `change_name_3`;
CREATE TABLE `change_name_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_3
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_4
-- ----------------------------
DROP TABLE IF EXISTS `change_name_4`;
CREATE TABLE `change_name_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_4
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_5
-- ----------------------------
DROP TABLE IF EXISTS `change_name_5`;
CREATE TABLE `change_name_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_5
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_6
-- ----------------------------
DROP TABLE IF EXISTS `change_name_6`;
CREATE TABLE `change_name_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_6
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_7
-- ----------------------------
DROP TABLE IF EXISTS `change_name_7`;
CREATE TABLE `change_name_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_7
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_8
-- ----------------------------
DROP TABLE IF EXISTS `change_name_8`;
CREATE TABLE `change_name_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_8
-- ----------------------------

-- ----------------------------
-- Table structure for change_name_9
-- ----------------------------
DROP TABLE IF EXISTS `change_name_9`;
CREATE TABLE `change_name_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `changeScenario` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改场景,修改自己显示名，修改群昵称，修改他人备注',
  `nameLength` int(10) DEFAULT '0' COMMENT '昵称字数',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of change_name_9
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_0
-- ----------------------------
DROP TABLE IF EXISTS `create_group_0`;
CREATE TABLE `create_group_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_0
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_1
-- ----------------------------
DROP TABLE IF EXISTS `create_group_1`;
CREATE TABLE `create_group_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_1
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_2
-- ----------------------------
DROP TABLE IF EXISTS `create_group_2`;
CREATE TABLE `create_group_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_2
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_3
-- ----------------------------
DROP TABLE IF EXISTS `create_group_3`;
CREATE TABLE `create_group_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_3
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_4
-- ----------------------------
DROP TABLE IF EXISTS `create_group_4`;
CREATE TABLE `create_group_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_4
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_5
-- ----------------------------
DROP TABLE IF EXISTS `create_group_5`;
CREATE TABLE `create_group_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_5
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_6
-- ----------------------------
DROP TABLE IF EXISTS `create_group_6`;
CREATE TABLE `create_group_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_6
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_7
-- ----------------------------
DROP TABLE IF EXISTS `create_group_7`;
CREATE TABLE `create_group_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_7
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_8
-- ----------------------------
DROP TABLE IF EXISTS `create_group_8`;
CREATE TABLE `create_group_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_8
-- ----------------------------

-- ----------------------------
-- Table structure for create_group_9
-- ----------------------------
DROP TABLE IF EXISTS `create_group_9`;
CREATE TABLE `create_group_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of create_group_9
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_0
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_0`;
CREATE TABLE `dismiss_group_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_0
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_1
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_1`;
CREATE TABLE `dismiss_group_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_1
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_2
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_2`;
CREATE TABLE `dismiss_group_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_2
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_3
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_3`;
CREATE TABLE `dismiss_group_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_3
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_4
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_4`;
CREATE TABLE `dismiss_group_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_4
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_5
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_5`;
CREATE TABLE `dismiss_group_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_5
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_6
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_6`;
CREATE TABLE `dismiss_group_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_6
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_7
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_7`;
CREATE TABLE `dismiss_group_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_7
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_8
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_8`;
CREATE TABLE `dismiss_group_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_8
-- ----------------------------

-- ----------------------------
-- Table structure for dismiss_group_9
-- ----------------------------
DROP TABLE IF EXISTS `dismiss_group_9`;
CREATE TABLE `dismiss_group_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dismiss_group_9
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_0
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_0`;
CREATE TABLE `end_callback_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_0
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_1
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_1`;
CREATE TABLE `end_callback_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_1
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_2
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_2`;
CREATE TABLE `end_callback_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_2
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_3
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_3`;
CREATE TABLE `end_callback_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_3
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_4
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_4`;
CREATE TABLE `end_callback_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_4
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_5
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_5`;
CREATE TABLE `end_callback_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_5
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_6
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_6`;
CREATE TABLE `end_callback_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_6
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_7
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_7`;
CREATE TABLE `end_callback_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_7
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_8
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_8`;
CREATE TABLE `end_callback_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_8
-- ----------------------------

-- ----------------------------
-- Table structure for end_callback_9
-- ----------------------------
DROP TABLE IF EXISTS `end_callback_9`;
CREATE TABLE `end_callback_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `succeed` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `talkLength` bigint(20) DEFAULT '0' COMMENT '对讲时长',
  `talkData` decimal(10,4) DEFAULT '0.0000' COMMENT '流量大小单位是M',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_callback_9
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_0
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_0`;
CREATE TABLE `end_share_location_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_0
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_1
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_1`;
CREATE TABLE `end_share_location_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_1
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_2
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_2`;
CREATE TABLE `end_share_location_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_2
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_3
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_3`;
CREATE TABLE `end_share_location_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_3
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_4
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_4`;
CREATE TABLE `end_share_location_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_4
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_5
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_5`;
CREATE TABLE `end_share_location_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_5
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_6
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_6`;
CREATE TABLE `end_share_location_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_6
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_7
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_7`;
CREATE TABLE `end_share_location_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_7
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_8
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_8`;
CREATE TABLE `end_share_location_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_8
-- ----------------------------

-- ----------------------------
-- Table structure for end_share_location_9
-- ----------------------------
DROP TABLE IF EXISTS `end_share_location_9`;
CREATE TABLE `end_share_location_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `shareLocationLength` bigint(20) DEFAULT '0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of end_share_location_9
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_0
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_0`;
CREATE TABLE `enter_group_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_0
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_1
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_1`;
CREATE TABLE `enter_group_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_1
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_2
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_2`;
CREATE TABLE `enter_group_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_2
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_3
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_3`;
CREATE TABLE `enter_group_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_3
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_4
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_4`;
CREATE TABLE `enter_group_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_4
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_5
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_5`;
CREATE TABLE `enter_group_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_5
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_6
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_6`;
CREATE TABLE `enter_group_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_6
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_7
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_7`;
CREATE TABLE `enter_group_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_7
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_8
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_8`;
CREATE TABLE `enter_group_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_8
-- ----------------------------

-- ----------------------------
-- Table structure for enter_group_9
-- ----------------------------
DROP TABLE IF EXISTS `enter_group_9`;
CREATE TABLE `enter_group_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of enter_group_9
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_0
-- ----------------------------
DROP TABLE IF EXISTS `join_group_0`;
CREATE TABLE `join_group_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_0
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_1
-- ----------------------------
DROP TABLE IF EXISTS `join_group_1`;
CREATE TABLE `join_group_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_1
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_2
-- ----------------------------
DROP TABLE IF EXISTS `join_group_2`;
CREATE TABLE `join_group_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_2
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_3
-- ----------------------------
DROP TABLE IF EXISTS `join_group_3`;
CREATE TABLE `join_group_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_3
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_4
-- ----------------------------
DROP TABLE IF EXISTS `join_group_4`;
CREATE TABLE `join_group_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_4
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_5
-- ----------------------------
DROP TABLE IF EXISTS `join_group_5`;
CREATE TABLE `join_group_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_5
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_6
-- ----------------------------
DROP TABLE IF EXISTS `join_group_6`;
CREATE TABLE `join_group_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_6
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_7
-- ----------------------------
DROP TABLE IF EXISTS `join_group_7`;
CREATE TABLE `join_group_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_7
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_8
-- ----------------------------
DROP TABLE IF EXISTS `join_group_8`;
CREATE TABLE `join_group_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_8
-- ----------------------------

-- ----------------------------
-- Table structure for join_group_9
-- ----------------------------
DROP TABLE IF EXISTS `join_group_9`;
CREATE TABLE `join_group_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of join_group_9
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_0
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_0`;
CREATE TABLE `key_button_click_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_0
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_1
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_1`;
CREATE TABLE `key_button_click_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_1
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_2
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_2`;
CREATE TABLE `key_button_click_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_2
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_3
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_3`;
CREATE TABLE `key_button_click_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_3
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_4
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_4`;
CREATE TABLE `key_button_click_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_4
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_5
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_5`;
CREATE TABLE `key_button_click_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_5
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_6
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_6`;
CREATE TABLE `key_button_click_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_6
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_7
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_7`;
CREATE TABLE `key_button_click_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_7
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_8
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_8`;
CREATE TABLE `key_button_click_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_8
-- ----------------------------

-- ----------------------------
-- Table structure for key_button_click_9
-- ----------------------------
DROP TABLE IF EXISTS `key_button_click_9`;
CREATE TABLE `key_button_click_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `buttonName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of key_button_click_9
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_0
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_0`;
CREATE TABLE `quit_group_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_0
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_1
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_1`;
CREATE TABLE `quit_group_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_1
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_2
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_2`;
CREATE TABLE `quit_group_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_2
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_3
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_3`;
CREATE TABLE `quit_group_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_3
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_4
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_4`;
CREATE TABLE `quit_group_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_4
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_5
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_5`;
CREATE TABLE `quit_group_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_5
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_6
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_6`;
CREATE TABLE `quit_group_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_6
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_7
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_7`;
CREATE TABLE `quit_group_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_7
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_8
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_8`;
CREATE TABLE `quit_group_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_8
-- ----------------------------

-- ----------------------------
-- Table structure for quit_group_9
-- ----------------------------
DROP TABLE IF EXISTS `quit_group_9`;
CREATE TABLE `quit_group_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of quit_group_9
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_0
-- ----------------------------
DROP TABLE IF EXISTS `send_location_0`;
CREATE TABLE `send_location_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_0
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_1
-- ----------------------------
DROP TABLE IF EXISTS `send_location_1`;
CREATE TABLE `send_location_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_1
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_2
-- ----------------------------
DROP TABLE IF EXISTS `send_location_2`;
CREATE TABLE `send_location_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_2
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_3
-- ----------------------------
DROP TABLE IF EXISTS `send_location_3`;
CREATE TABLE `send_location_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_3
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_4
-- ----------------------------
DROP TABLE IF EXISTS `send_location_4`;
CREATE TABLE `send_location_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_4
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_5
-- ----------------------------
DROP TABLE IF EXISTS `send_location_5`;
CREATE TABLE `send_location_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_5
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_6
-- ----------------------------
DROP TABLE IF EXISTS `send_location_6`;
CREATE TABLE `send_location_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_6
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_7
-- ----------------------------
DROP TABLE IF EXISTS `send_location_7`;
CREATE TABLE `send_location_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_7
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_8
-- ----------------------------
DROP TABLE IF EXISTS `send_location_8`;
CREATE TABLE `send_location_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_8
-- ----------------------------

-- ----------------------------
-- Table structure for send_location_9
-- ----------------------------
DROP TABLE IF EXISTS `send_location_9`;
CREATE TABLE `send_location_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of send_location_9
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_0
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_0`;
CREATE TABLE `server_aggregation_upload_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_0
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_1
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_1`;
CREATE TABLE `server_aggregation_upload_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_1
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_2
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_2`;
CREATE TABLE `server_aggregation_upload_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_2
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_3
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_3`;
CREATE TABLE `server_aggregation_upload_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_3
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_4
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_4`;
CREATE TABLE `server_aggregation_upload_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_4
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_5
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_5`;
CREATE TABLE `server_aggregation_upload_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_5
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_6
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_6`;
CREATE TABLE `server_aggregation_upload_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_6
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_7
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_7`;
CREATE TABLE `server_aggregation_upload_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_7
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_8
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_8`;
CREATE TABLE `server_aggregation_upload_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_8
-- ----------------------------

-- ----------------------------
-- Table structure for server_aggregation_upload_9
-- ----------------------------
DROP TABLE IF EXISTS `server_aggregation_upload_9`;
CREATE TABLE `server_aggregation_upload_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `number` bigint(20) DEFAULT '0' COMMENT '人数',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `simType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_aggregation_upload_9
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_0
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_0`;
CREATE TABLE `server_detail_upload_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_0
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_1
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_1`;
CREATE TABLE `server_detail_upload_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_1
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_2
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_2`;
CREATE TABLE `server_detail_upload_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_2
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_3
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_3`;
CREATE TABLE `server_detail_upload_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_3
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_4
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_4`;
CREATE TABLE `server_detail_upload_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_4
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_5
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_5`;
CREATE TABLE `server_detail_upload_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_5
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_6
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_6`;
CREATE TABLE `server_detail_upload_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_6
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_7
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_7`;
CREATE TABLE `server_detail_upload_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_7
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_8
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_8`;
CREATE TABLE `server_detail_upload_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_8
-- ----------------------------

-- ----------------------------
-- Table structure for server_detail_upload_9
-- ----------------------------
DROP TABLE IF EXISTS `server_detail_upload_9`;
CREATE TABLE `server_detail_upload_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组ID',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `uploadType` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上传类型',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of server_detail_upload_9
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_0
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_0`;
CREATE TABLE `share_group_gode_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_0
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_1
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_1`;
CREATE TABLE `share_group_gode_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_1
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_2
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_2`;
CREATE TABLE `share_group_gode_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_2
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_3
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_3`;
CREATE TABLE `share_group_gode_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_3
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_4
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_4`;
CREATE TABLE `share_group_gode_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_4
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_5
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_5`;
CREATE TABLE `share_group_gode_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_5
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_6
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_6`;
CREATE TABLE `share_group_gode_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_6
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_7
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_7`;
CREATE TABLE `share_group_gode_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_7
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_8
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_8`;
CREATE TABLE `share_group_gode_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_8
-- ----------------------------

-- ----------------------------
-- Table structure for share_group_gode_9
-- ----------------------------
DROP TABLE IF EXISTS `share_group_gode_9`;
CREATE TABLE `share_group_gode_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_group_gode_9
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_0
-- ----------------------------
DROP TABLE IF EXISTS `share_location_0`;
CREATE TABLE `share_location_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_0
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_1
-- ----------------------------
DROP TABLE IF EXISTS `share_location_1`;
CREATE TABLE `share_location_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_1
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_2
-- ----------------------------
DROP TABLE IF EXISTS `share_location_2`;
CREATE TABLE `share_location_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_2
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_3
-- ----------------------------
DROP TABLE IF EXISTS `share_location_3`;
CREATE TABLE `share_location_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_3
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_4
-- ----------------------------
DROP TABLE IF EXISTS `share_location_4`;
CREATE TABLE `share_location_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_4
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_5
-- ----------------------------
DROP TABLE IF EXISTS `share_location_5`;
CREATE TABLE `share_location_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_5
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_6
-- ----------------------------
DROP TABLE IF EXISTS `share_location_6`;
CREATE TABLE `share_location_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_6
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_7
-- ----------------------------
DROP TABLE IF EXISTS `share_location_7`;
CREATE TABLE `share_location_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_7
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_8
-- ----------------------------
DROP TABLE IF EXISTS `share_location_8`;
CREATE TABLE `share_location_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_8
-- ----------------------------

-- ----------------------------
-- Table structure for share_location_9
-- ----------------------------
DROP TABLE IF EXISTS `share_location_9`;
CREATE TABLE `share_location_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of share_location_9
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_0
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_0`;
CREATE TABLE `start_talkback_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_0
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_1
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_1`;
CREATE TABLE `start_talkback_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_1
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_2
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_2`;
CREATE TABLE `start_talkback_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_2
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_3
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_3`;
CREATE TABLE `start_talkback_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_3
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_4
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_4`;
CREATE TABLE `start_talkback_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_4
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_5
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_5`;
CREATE TABLE `start_talkback_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_5
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_6
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_6`;
CREATE TABLE `start_talkback_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_6
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_7
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_7`;
CREATE TABLE `start_talkback_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_7
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_8
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_8`;
CREATE TABLE `start_talkback_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_8
-- ----------------------------

-- ----------------------------
-- Table structure for start_talkback_9
-- ----------------------------
DROP TABLE IF EXISTS `start_talkback_9`;
CREATE TABLE `start_talkback_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `isLocationShare` bit(1) DEFAULT b'0' COMMENT '是否位置共享',
  `groupID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组id',
  `groupName` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '群组名称',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of start_talkback_9
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_0
-- ----------------------------
DROP TABLE IF EXISTS `user_active_0`;
CREATE TABLE `user_active_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_0
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_1
-- ----------------------------
DROP TABLE IF EXISTS `user_active_1`;
CREATE TABLE `user_active_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_1
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_2
-- ----------------------------
DROP TABLE IF EXISTS `user_active_2`;
CREATE TABLE `user_active_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_2
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_3
-- ----------------------------
DROP TABLE IF EXISTS `user_active_3`;
CREATE TABLE `user_active_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_3
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_4
-- ----------------------------
DROP TABLE IF EXISTS `user_active_4`;
CREATE TABLE `user_active_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_4
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_5
-- ----------------------------
DROP TABLE IF EXISTS `user_active_5`;
CREATE TABLE `user_active_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_5
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_6
-- ----------------------------
DROP TABLE IF EXISTS `user_active_6`;
CREATE TABLE `user_active_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_6
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_7
-- ----------------------------
DROP TABLE IF EXISTS `user_active_7`;
CREATE TABLE `user_active_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_7
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_8
-- ----------------------------
DROP TABLE IF EXISTS `user_active_8`;
CREATE TABLE `user_active_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_8
-- ----------------------------

-- ----------------------------
-- Table structure for user_active_9
-- ----------------------------
DROP TABLE IF EXISTS `user_active_9`;
CREATE TABLE `user_active_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_active_9
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_0
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_0`;
CREATE TABLE `user_renew_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_0
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_1
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_1`;
CREATE TABLE `user_renew_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_1
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_2
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_2`;
CREATE TABLE `user_renew_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_2
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_3
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_3`;
CREATE TABLE `user_renew_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_3
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_4
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_4`;
CREATE TABLE `user_renew_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_4
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_5
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_5`;
CREATE TABLE `user_renew_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_5
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_6
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_6`;
CREATE TABLE `user_renew_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_6
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_7
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_7`;
CREATE TABLE `user_renew_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_7
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_8
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_8`;
CREATE TABLE `user_renew_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_8
-- ----------------------------

-- ----------------------------
-- Table structure for user_renew_9
-- ----------------------------
DROP TABLE IF EXISTS `user_renew_9`;
CREATE TABLE `user_renew_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_renew_9
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_0
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_0`;
CREATE TABLE `user_stop_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_0
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_1
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_1`;
CREATE TABLE `user_stop_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_1
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_2
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_2`;
CREATE TABLE `user_stop_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_2
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_3
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_3`;
CREATE TABLE `user_stop_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_3
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_4
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_4`;
CREATE TABLE `user_stop_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_4
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_5
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_5`;
CREATE TABLE `user_stop_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_5
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_6
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_6`;
CREATE TABLE `user_stop_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_6
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_7
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_7`;
CREATE TABLE `user_stop_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_7
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_8
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_8`;
CREATE TABLE `user_stop_8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_8
-- ----------------------------

-- ----------------------------
-- Table structure for user_stop_9
-- ----------------------------
DROP TABLE IF EXISTS `user_stop_9`;
CREATE TABLE `user_stop_9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备ID',
  `distinct_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID（未获取时传入设备ID）',
  `businessType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrcit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminalType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actionProvince` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  `app_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '应用的版本',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `lib` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK类型，例如python、iOS等',
  `lib_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'SDK版本',
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省份',
  `city` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '城市',
  `manufacturer` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备制造商，例如Apple',
  `model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备型号，例如iphone6',
  `os` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作系统，例如iOS',
  `os_version` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'os_version',
  `screen_height` int(10) DEFAULT NULL COMMENT '屏幕高度，例如1920',
  `screen_width` int(10) DEFAULT NULL COMMENT '屏幕宽度，例如1080',
  `wifi` bit(1) DEFAULT b'0' COMMENT '是否使用wifi，例如true',
  `network_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络类型，例如4G',
  `carrier` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '运营商名称，例如ChinaNet',
  `utm_matching_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '渠道追踪匹配模式',
  `is_first_day` bit(1) DEFAULT b'0' COMMENT '是否首日访问',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `orderSource` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单来源',
  `productCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品编码',
  `orderID` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编码',
  `db_value` int(10) DEFAULT NULL,
  `table_value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_stop_9
-- ----------------------------
