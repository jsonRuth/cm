package com.shanli.sys.controller;

import com.shanli.common.annotation.Log;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.domain.Tree;
import com.shanli.common.type.EnumErrorCode;
import com.shanli.common.utils.MD5Utils;
import com.shanli.common.utils.Result;
import com.shanli.operate.service.collect.RepoFeedbackService;
import com.shanli.operate.service.collect.RepoUserInfoService;
import com.shanli.operate.service.collect.RepoUserLockLogService;
import com.shanli.oss.domain.FileDO;
import com.shanli.oss.service.FileService;
import com.shanli.sys.domain.ChangePwdLogDO;
import com.shanli.sys.domain.MenuDO;
import com.shanli.sys.domain.UserDO;
import com.shanli.sys.service.ChangePwdLogService;
import com.shanli.sys.service.MenuService;
import com.shanli.sys.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <pre>
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
@Controller
public class LoginController extends AdminBaseController {

    @Autowired
    MenuService menuService;
    @Autowired
    FileService fileService;
    @Autowired
    RepoFeedbackService repoFeedbackService;
    @Autowired
    RepoUserInfoService userInfoService;
    @Autowired
    UserService userService;
    @Autowired
    ChangePwdLogService changePwdLogService;


    @GetMapping({"/", ""})
    String welcome(Model model) {
        return "redirect:/login";
    }


    @GetMapping({"/index"})
    String index(Model model,HttpServletRequest request) {
        String reqType = (String) request.getSession().getAttribute("reqType");
        Long leadTime = 0L;
        if ("1".equals(reqType)){
           UserDO userDO = getUser();
           leadTime = leadTime(userDO);
           if (leadTime-91<0){
               request.getSession().removeAttribute("reqType");
           }
        }
        List<Tree<MenuDO>> menus = menuService.listMenuTree(getUserId());
        model.addAttribute("leadTime",leadTime);
        model.addAttribute("menus", menus);
        model.addAttribute("menus", menus);
        model.addAttribute("name", getUser().getName());
        model.addAttribute("username", getUser().getUsername());
        FileDO fileDO = fileService.selectById(getUser().getPicId());
        model.addAttribute("picUrl", fileDO == null ? "/img/photo_s.jpg" : fileDO.getUrl());
        return "index_v1";
    }

    @GetMapping("/login")
    String login() {
        return "login";
    }

    @Log("登录")
    @PostMapping("/login")
    @ResponseBody
    Result<Long> ajaxLogin(String username, String password , HttpServletRequest request) {


        if(username.substring(0,1).equals("1") && username.length() == 11 && username.matches("^[0-9]*$")) {
            UserDO userDO = userService.selectByMobile(username);
            if (userDO == null) {
                return Result.build(EnumErrorCode.userLoginFail.getCode(), EnumErrorCode.userLoginFail.getMsg());
            } else {
                username = userDO.getUsername();
            }

        }
        password = MD5Utils.encrypt(username, password);
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        token.setRememberMe(true);//记住我是可选项，但只有会话缓存到redis等持久存储才能真正记住
        Subject subject = SecurityUtils.getSubject();
        UserDO userDO = userService.selectByUsername(username);
        try {
            subject.login(token);
            request.getSession().setAttribute("reqType", "1");
            return Result.ok();
        } catch (AuthenticationException e) {
            return Result.build(EnumErrorCode.userLoginFail.getCode(), EnumErrorCode.userLoginFail.getMsg());
        }


    }

    /**
     * 计算时间方法
     * @param userDO
     */
    private Long leadTime(UserDO userDO) {
        Date updateTime = changePwdLogService.selectByIdMax(userDO.getId());
        if (updateTime==null){
            updateTime = userDO.getGmtCreate();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf.format(updateTime);//密码更新时间
        Date date1 = new Date();//今天的时间
        String format1 = sdf.format(date1);
        Calendar cal = Calendar.getInstance();
        long time = 0;
        long timeInMillis = 0;
        try {
            cal.setTime(sdf.parse(format));
            time = cal.getTimeInMillis();
            cal.setTime(sdf.parse(format1));
            timeInMillis = cal.getTimeInMillis();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long between_days=((timeInMillis-time)/(1000*3600*24)+1);
        return between_days;
    }

    /**
     * 加载首页
     *
     * @param model
     * @return
     */
    @GetMapping("/main")
    String main(Model model) {
        //查询意见反馈累计数量
        Integer feedbackCount = repoFeedbackService.getLastTotalCount();
        model.addAttribute("feedbackCount", (feedbackCount == null) ? 0 : feedbackCount);
        //查询登录失败锁定次数
        Integer userlockCount = userInfoService.getLastTotalCount();
        model.addAttribute("userlockCount", (userlockCount == null) ? 0 : userlockCount);
        return "main";
    }

    @GetMapping("/403")
    String error403() {
        return "403";
    }

    @GetMapping("/css")
    String css(){
        return "404";
    }
    @GetMapping("/editor-app")
    String editor(){
        return "404";
    }
    @GetMapping("/fonts")
    String fonts(){
        return "404";
    }
    @GetMapping("/img")
    String img(){
        return "404";
    }
    @GetMapping("/js")
    String js(){
        return "404";
    }

}
