package com.shanli.sys.controller;

import com.shanli.common.annotation.Log;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.type.EnumErrorCode;
import com.shanli.common.utils.Result;
import com.shanli.sys.domain.RoleDO;
import com.shanli.sys.service.RoleService;
import com.shanli.sys.service.UserRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.*;

/**
 * <pre>
 * </pre>
 * 
 * <small> 2018年3月23日 | Aron</small>
 */
@RequestMapping("/sys/role")
@Controller
public class RoleController extends AdminBaseController {
    String prefix = "sys/role";
    @Autowired
    RoleService roleService;
    @Autowired
    UserRoleService userRoleService;
    
    @RequiresPermissions("sys:role:role")
    @GetMapping()
    String role() {
        return prefix + "/role";
    }
    
    @RequiresPermissions("sys:role:role")
    @GetMapping("/list")
    @ResponseBody()
    List<RoleDO> list() {
        List<RoleDO> roles = roleService.findAll();
        return roles;
    }

    @RequiresPermissions("sys:role:add")
    @GetMapping("/add")
    String add() {
        return prefix + "/add";
    }

    @RequiresPermissions("sys:role:edit")
    @GetMapping("/edit/{id}")
    String edit(@PathVariable("id") Long id, Model model) {
        RoleDO roleDO = roleService.selectById(id);
        model.addAttribute("role", roleDO);
        return prefix + "/edit";
    }

    @Log("保存角色")
    @RequiresPermissions("sys:role:add")
    @PostMapping("/save")
    @ResponseBody()
    Result<String> save(RoleDO role) {
        Date date = new Date();
        role.setGmtCreate(date);
        role.setGmtModified(date);
        roleService.insert(role);
        return Result.ok();
    }

    @Log("更新角色")
    @RequiresPermissions("sys:role:edit")
    @PostMapping("/update")
    @ResponseBody()
    Result<String> update(RoleDO role) {
        roleService.updateById(role);
        return Result.ok();
    }
    
    @Log("删除角色")
    @RequiresPermissions("sys:role:remove")
    @PostMapping("/remove")
    @ResponseBody()
    Result<String> save(Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("roleId", id);
        int size =userRoleService.selectByMap(map).size();
        if (size>0){
            return Result.build(EnumErrorCode.deptDeleteErrorExistUsers.getCode(),
                    EnumErrorCode.deptDeleteErrorExistUsers.getMsg());
        } else {
            roleService.deleteById(id);
            return Result.ok();
        }
    }
    
    @RequiresPermissions("sys:role:batchRemove")
    @Log("批量删除角色")
    @PostMapping("/batchRemove")
    @ResponseBody
    Result<String> batchRemove(@RequestParam("ids[]") Long[] ids) {
        Map<String, Object> map = new HashMap<String, Object>();
        for (Long id : ids) {
            map.put("roleId", id);
            int size =userRoleService.selectByMap(map).size();
            if (size>0){
                return Result.build(EnumErrorCode.deptDeleteErrorExistUsers.getCode(),
                        EnumErrorCode.deptDeleteErrorExistUsers.getMsg());
            }
        }
        roleService.deleteBatchIds(Arrays.asList(ids));
        return Result.ok();
    }
}
