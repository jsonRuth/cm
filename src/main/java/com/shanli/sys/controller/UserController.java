package com.shanli.sys.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.shanli.common.exception.IFastException;
import com.shanli.sys.domain.ChangePwdLogDO;
import com.shanli.sys.service.*;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.annotation.Log;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.domain.DictDO;
import com.shanli.common.domain.Tree;
import com.shanli.common.service.DictService;
import com.shanli.common.utils.MD5Utils;
import com.shanli.common.utils.Result;
import com.shanli.sys.domain.DeptDO;
import com.shanli.sys.domain.RoleDO;
import com.shanli.sys.domain.UserDO;
import com.shanli.sys.vo.UserVO;

/**
 * <pre>
 * </pre>
 * 
 * <small> 2018年3月23日 | Aron</small>
 */
@RequestMapping("/sys/user")
@Controller
public class UserController extends AdminBaseController {
    private String prefix = "sys/user";
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    DictService dictService;
    @Autowired
    ChangePwdLogService changePwdLogService;
    @Autowired
    UserRoleService userRoleService;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    MasService masService ;

    @RequiresPermissions("sys:user:user")
    @GetMapping("")
    String user(Model model) {
        return prefix + "/user";
    }
    
    @GetMapping("/list")
    @ResponseBody
    public Result<Page<UserDO>> list(UserDO userDTO) {
        Wrapper<UserDO> wrapper = new EntityWrapper<UserDO>();
        if (!StringUtils.isEmpty(userDTO.getName())) {
            wrapper.like("name", userDTO.getName());
        }
        if(userDTO.getDeptId() != null){
            wrapper.eq("deptId", userDTO.getDeptId());
        }
        //查询超管的用户id
        List<Integer> list = userRoleService.findListAdminUserId();
        wrapper.notIn("id",list);
        wrapper.orderBy("gmtCreate",false);
        // 查询列表数据
        Page<UserDO> page = userService.selectPage(getPage(UserDO.class),wrapper);
        return Result.ok(page);
    }

    
    @RequiresPermissions("sys:user:add")
    @GetMapping("/add")
    String add(Model model) {
        List<RoleDO> roles = roleService.selectList(null);
        model.addAttribute("roles", roles);
        return prefix + "/add";
    }

    @RequiresPermissions("sys:user:edit")
    @GetMapping("/edit/{id}")
    String edit(Model model, @PathVariable("id") Long id) {
        UserDO userDO = userService.selectById(id);
        model.addAttribute("user", userDO);
        List<RoleDO> roles = roleService.findListStatusByUserId(id);
        model.addAttribute("roles", roles);
        return prefix + "/edit";
    }

    @RequiresPermissions("sys:user:add")
    @Log("保存用户")
    @PostMapping("/save")
    @ResponseBody
    Result<String> save(UserDO user) {
        user.setPassword(MD5Utils.encrypt(user.getUsername(), user.getPassword()));
        Date date = new Date();
        user.setGmtCreate(date);
        user.setGmtModified(date);
        userService.insert(user);
        return Result.ok();
    }

    @RequiresPermissions("sys:user:edit")
    @Log("更新用户")
    @PostMapping("/update")
    @ResponseBody
    Result<String> update(UserDO user) {
        user.setGmtModified(new Date());
        userService.updateById(user);
        return Result.ok();
    }

    @RequiresPermissions("sys:user:edit")
    @Log("更新用户")
    @PostMapping("/updatePeronal")
    @ResponseBody
    Result<String> updatePeronal(UserDO user) {
        user.setGmtModified(new Date());
        userService.updatePersonal(user);
        return Result.ok();
    }

    @RequiresPermissions("sys:user:remove")
    @Log("删除用户")
    @PostMapping("/remove")
    @ResponseBody
    Result<String> remove(Long id) {
        userService.deleteById(id);
        return Result.ok();
    }

    @RequiresPermissions("sys:user:batchRemove")
    @Log("批量删除用户")
    @PostMapping("/batchRemove")
    @ResponseBody
    Result<String> batchRemove(@RequestParam("ids[]") Long[] userIds) {
        userService.deleteBatchIds(Arrays.asList(userIds));
        return Result.ok();
    }
    
    @PostMapping("/exist")
    @ResponseBody
    boolean exist(@RequestParam Map<String, Object> params) {
        // 存在，不通过，false
        return !userService.exist(params);
    }

    @RequiresPermissions("sys:user:resetPwd")
    @GetMapping("/resetPwd/{id}")
    String resetPwd(@PathVariable("id") Long userId, Model model) {
        UserDO userDO = new UserDO();
        userDO.setId(userId);
        model.addAttribute("user", userDO);
        return prefix + "/reset_pwd";
    }

    @Log("提交更改用户密码")
    @PostMapping("/resetPwd")
    @ResponseBody
    Result<String> resetPwd(UserVO userVO) {
        //判断五次修改的密码是否重复
        ChangePwdLogDO lastRecord = changePwdLogService.selectFiveChangePwdLog(MD5Utils.encrypt(getUser().getUsername(), userVO.getPwdNew()),getUser().getId());
        if(lastRecord != null){
            return Result.build(Result.EnumStatusCode.USER_PWD_CHANGE_FIVE_REPEAT);
        }
        UserDO userDO = userService.selectById(userVO.getUserDO().getId());
        //不得包含用户名的完整字符串
        if (userVO.getPwdNew().indexOf(userDO.getUsername())!=-1){
            return Result.build(Result.EnumStatusCode.USER_PWD_CHANGE_INCLUDE_REPEAT);
        }
        //保存密码修改记录
        ChangePwdLogDO changePwdLogDO = new ChangePwdLogDO();
        changePwdLogDO.setUserId(getUser().getId());
        changePwdLogDO.setOldPwd(getUser().getPassword());
        changePwdLogDO.setNewPwd(MD5Utils.encrypt(getUser().getUsername(), userVO.getPwdNew()));
        changePwdLogDO.setUpdateReason(userVO.getUpdateReason());
        changePwdLogService.saveChangePwdLog(changePwdLogDO);

        //修改密码
        try {
            userService.resetPwd(userVO, getUser());
        } catch (IFastException e) {
            return Result.build(Result.EnumStatusCode.OLD_PASSWORD_ERROR);
        }
        return Result.ok();
    }

    @RequiresPermissions("sys:user:resetPwd")
    @Log("admin提交更改用户密码")
    @PostMapping("/adminResetPwd")
    @ResponseBody
    Result<String> adminResetPwd(UserVO userVO) {
        userService.adminResetPwd(userVO);
        return Result.ok();

    }
    
    @GetMapping("/tree")
    @ResponseBody
    public Tree<DeptDO> tree() {
        Tree<DeptDO> tree = new Tree<DeptDO>();
        tree = userService.getTree();
        return tree;
    }
    
    @GetMapping("/treeView")
    String treeView() {
        return prefix + "/userTree";
    }
    
    @GetMapping("/personal")
    String personal(Model model) {
        UserDO userDO = userService.selectById(getUserId());
        model.addAttribute("user", userDO);
//        List<DictDO> hobbyList = dictService.getHobbyList(userDO);
        List<DictDO> hobbyList = new ArrayList<>();
        model.addAttribute("hobbyList", hobbyList);
//        List<DictDO> sexList = dictService.getSexList();
        List<DictDO> sexList = new ArrayList<>();
        model.addAttribute("sexList", sexList);
        return prefix + "/personal";
    }

    @GetMapping("/personalLogin")
    String personalLogin(Model model) {
        UserDO userDO = userService.selectById(getUserId());
        model.addAttribute("user", userDO);
//        List<DictDO> hobbyList = dictService.getHobbyList(userDO);
        List<DictDO> hobbyList = new ArrayList<>();
        model.addAttribute("hobbyList", hobbyList);
//        List<DictDO> sexList = dictService.getSexList();
        List<DictDO> sexList = new ArrayList<>();
        model.addAttribute("sexList", sexList);
        return prefix + "/personalLogin";
    }

    /**
     * 绑定手机页面
     * @param model
     * @return
     */
    @GetMapping("/phoneBinding")
    String phoneBinding(Model model) {
        UserDO userDO = userService.selectById(getUserId());
        if (StringUtils.isEmpty(userDO.getMobile())){
            model.addAttribute("user", userDO);
            Long expire = redisTemplate.getExpire(getUserId()+"tenLocking", TimeUnit.SECONDS);
            if (expire < 0){
                Long code = redisTemplate.getExpire(getUserId()+"code", TimeUnit.SECONDS);
                if (code > 0){
                    model.addAttribute("time", code);
                    return prefix + "/phoneBinding";
                }
            }
            model.addAttribute("time", expire);
            return prefix + "/phoneBinding";
        }
        else {
            model.addAttribute("user", userDO);
            return prefix + "/mobile";
        }

    }

    @Log("上传头像")
    @ResponseBody
    @PostMapping("/uploadImg")
    Result<?> uploadImg(@RequestParam("avatar_file") MultipartFile file, String avatar_data, HttpServletRequest request)
            throws Exception {
        Map<String, Object> result = new HashMap<>();
        result = userService.updatePersonalImg(file, avatar_data, getUserId());
        return Result.ok(result);
    }

    /**
     * 发送验证码
     * @param mobile
     * @return
     */
    @ResponseBody
    @PostMapping("/sendCode")
    public Result sendCode(String mobile){
        try {
            //验证手机号
            UserDO user = userService.selectByMobile(mobile);
            if (user!=null){
                return  Result.build(Result.EnumStatusCode.MOBILE_PHONE_TO_REPEAT);
            }

            //生成验证码
            String code = userService.createSmsCode(mobile,getUser().getId());
            String[] params = {code,"10"};
            masService.sendTemp(mobile,"03e1d73814d244c3aac2481c66cfd43c" , params);
            redisTemplate.opsForValue().set(getUserId()+"code",1,60,TimeUnit.SECONDS);
            return Result.ok() ;


        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.build(Result.EnumStatusCode.SEND_VERIFICATION_CODE);
    }


    @Log("绑定手机号")
    @PostMapping("/mobileBinding")
    @ResponseBody
    public Result sendCode(String code,String mobile,Long id){
        String key = id+"reg";
        //先验证验证码
        boolean flag = userService.checkSmsCode(mobile, code);
        String date = String.valueOf(redisTemplate.opsForList().index(key, 0));
        if (date != "null"){
            Long dateL= Long.valueOf(date);
            Date date2 = new Date(dateL);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            //测试第二天redis清空
            //cal.add(Calendar.DATE, 1);
            //String format1 = sdf.format(cal.getTime());
            Date date1 = new Date();
            String format1 = sdf.format(date1);
            String format2 = sdf.format(date2);
            long time = 0;
            long timeInMillis = 0;
            try {
                cal.setTime(sdf.parse(format1));
                time = cal.getTimeInMillis();
                cal.setTime(sdf.parse(format2));
                timeInMillis = cal.getTimeInMillis();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long between_days=(time-timeInMillis)/(1000*3600*24);
            //如果时间大于1天，将清空redis
            if (between_days >=1){
                Set set = redisTemplate.keys(key);
                redisTemplate.delete(set);

            }
        }
        if(flag){
                try {
                    userService.updateByMobile(id,mobile);
                } catch (Exception e) {
                    e.printStackTrace();
                    return Result.build(Result.EnumStatusCode.MOBILE_REPEAT_ERROR);
                }
                Set keys = redisTemplate.keys(key);
                redisTemplate.delete(keys);
                redisTemplate.delete(getUserId()+"code");
                return Result.ok();
            }
            redisTemplate.opsForList().rightPush(key,System.currentTimeMillis());
            Long size = redisTemplate.opsForList().size(key);
            if (size==5){
                Set keys = redisTemplate.keys(key);
                redisTemplate.delete(keys);
                redisTemplate.opsForValue().set( id+"tenLocking",2,600,TimeUnit.SECONDS);
                return Result.build(1,"验证码错误",1);
                }
            return Result.build(Result.EnumStatusCode.CODE_ERROR);
    }


    @Log("解绑手机号")
    @PostMapping("/removeBinding")
    @ResponseBody
    public Result<String> removeBinding(Integer id){
        try {
            userService.updateByIdMobile(id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok();
    }

}


