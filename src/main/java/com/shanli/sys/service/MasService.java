package com.shanli.sys.service;

public interface MasService {

    int sendSms(String phone, String content)throws Exception;

    int sendTemp(String phone, String tempId, String[] params)throws Exception;
}
