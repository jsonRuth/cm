package com.shanli.sys.service;

import com.shanli.common.base.CoreService;
import com.shanli.sys.domain.DeptDO;
import com.shanli.sys.domain.UserRoleDO;

import java.util.List;
import java.util.Map;

public interface UserRoleService extends CoreService<UserRoleDO> {

    /**
     * 查询所有超管的userid
     * @return
     */
    List<Integer> findListAdminUserId();
}
