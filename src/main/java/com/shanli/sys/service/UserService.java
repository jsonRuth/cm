package com.shanli.sys.service;

import com.shanli.common.base.CoreService;
import com.shanli.common.domain.Tree;
import com.shanli.sys.domain.DeptDO;
import com.shanli.sys.domain.UserDO;
import com.shanli.sys.vo.UserVO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.Set;

/**
 * <pre>
 * </pre>
 * 
 * <small> 2018年3月23日 | Aron</small>
 */
@Service
public interface UserService extends CoreService<UserDO> {

    boolean exist(Map<String, Object> params);

    Set<String> listRoles(Long userId);

    int resetPwd(UserVO userVO, UserDO userDO);

    int adminResetPwd(UserVO userVO);

    Tree<DeptDO> getTree();

    /**
     * 更新个人信息
     * 
     * @param userDO
     * @return
     */
    int updatePersonal(UserDO userDO);

    /**
     * 更新个人图片
     * 
     * @param file
     *            图片
     * @param avatar_data
     *            裁剪信息
     * @param userId
     *            用户ID
     * @throws Exception
     */
    Map<String, Object> updatePersonalImg(MultipartFile file, String avatar_data, Long userId) throws Exception;

    /**
     *
     * 验证手机号是否唯一
     * @param mobile
     * @return
     */
    UserDO selectByMobile(String mobile);

    /**
     * 生成验证码
     * @param mobile
     */
    String createSmsCode(String mobile,Long id);

    /**
     * 验证码是否存在
     * @param mobile
     * @param code
     * @return
     */
    boolean checkSmsCode(String mobile, String code);

    /**
     * 绑定手机号
     * @param id
     * @param mobile
     */
    void updateByMobile(Long id,String mobile);

    /**
     * 根据用户名查时间
     * @param username
     * @return
     */
    UserDO selectByUsername(String username);

    /**
     * 解绑手机号
     * @param id
     * @return
     */
    Boolean updateByIdMobile(Integer id);
}
