package com.shanli.sys.service;

import com.shanli.sys.domain.ChangePwdLogDO;
import com.shanli.common.base.CoreService;

import javax.xml.crypto.Data;
import java.util.Date;

/**
 * 
 * <pre>
 * 修改密码记录表
 * </pre>
 * <small> 2019-05-23 09:35:52 | niush</small>
 */
public interface ChangePwdLogService extends CoreService<ChangePwdLogDO> {

    /**
     * 保存密码修改记录
     * @param changePwdLogDO
     * @return
     */
    Integer saveChangePwdLog(ChangePwdLogDO changePwdLogDO);

    /**
     * 判断连续五次修改的密码是否重复
     * @param password
     * @return
     */
    ChangePwdLogDO selectFiveChangePwdLog(String password,long userId);

    /**
     * 根据userId查询最新的一条修改记录
     * @param userId
     * @return
     */
    Date selectByIdMax(Long userId);
}
