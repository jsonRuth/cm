package com.shanli.sys.service.impl;


import com.mascloud.sdkclient.Client;
import com.shanli.sys.service.MasService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


@Service("MasService")
public class MasServiceImpl implements MasService {

    @Value("${mas.url}")
    private String url;
    @Value("${mas.userAccount}")
    private String userAccount;
    @Value("${mas.password}")
    private String password;
    @Value("${mas.sign}")
    private String sign;
    @Value("${mas.msgGroup}")
    private String msgGroup;

    /**
     * 发短信
     * @param phone 手机号
     * @param content 短信内容
     * @return
     */
    public int sendSms(String phone, String content)throws Exception{
        if(!StringUtils.hasText(phone) || !StringUtils.hasText(content)){
            return 0;
        }
        Client client = Client.getInstance();
        boolean isLoggedin = client.login( url, userAccount, password, "中国移动和对讲业务" );//中国移动和对讲业务
        if( isLoggedin ) {
            String[] mobiles = {phone};
            return client.sendDSMS( mobiles, content, "123", 1, sign, msgGroup, true );
        } else {
            return 0;
        }
    }

    /**
     * 发送模板短信
     * @param phone 手机号
     * @param tempId 模板id(在云mas官网创建短信模板 mas.10086.cn)
     * @param params 模板参数
     * @return
     */
    public int sendTemp(String phone, String tempId, String[] params)throws Exception{
        /**
         * tempId: b0a87df095f647f6a07cf2711b3f4e28
         * 内容: 您好！您的验证码是{[验证码:数字, 总长度6]}
         */
        if(!StringUtils.hasText(phone) || !StringUtils.hasText(tempId) || params == null){
            return 0;
        }
        Client client = Client.getInstance();
        boolean isLoggedin = client.login( url, userAccount, password, "中国移动和对讲业务" );
        if( isLoggedin ) {
            String[] mobiles = {phone};
            return client.sendTSMS(mobiles, tempId, params, "123", 1, sign, msgGroup);
        } else {
            return 0;
        }
    }
}
