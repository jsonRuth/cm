package com.shanli.sys.service.impl;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.sys.dao.UserRoleDao;
import com.shanli.sys.domain.UserRoleDO;
import com.shanli.sys.service.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl extends CoreServiceImpl<UserRoleDao,UserRoleDO> implements UserRoleService {

    @Override
    public List<Integer> findListAdminUserId() {
        return this.baseMapper.findListAdminUserId();
    }
}
