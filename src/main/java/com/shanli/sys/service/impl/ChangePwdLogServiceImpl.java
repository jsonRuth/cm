package com.shanli.sys.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shanli.sys.dao.ChangePwdLogDao;
import com.shanli.sys.domain.ChangePwdLogDO;
import com.shanli.sys.service.ChangePwdLogService;
import com.shanli.common.base.CoreServiceImpl;

import java.util.Date;

/**
 * 
 * <pre>
 * 修改密码记录表
 * </pre>
 * <small> 2019-05-23 09:35:52 | niush</small>
 */
@Service
public class ChangePwdLogServiceImpl extends CoreServiceImpl<ChangePwdLogDao, ChangePwdLogDO> implements ChangePwdLogService {

    @Autowired
    private ChangePwdLogDao changePwdLogDao;

    @Override
    public Integer saveChangePwdLog(ChangePwdLogDO changePwdLogDO) {
        return baseMapper.insert(changePwdLogDO);
    }

    @Override
    public ChangePwdLogDO selectFiveChangePwdLog(String password,long userId) {
        return changePwdLogDao.selectFiveChangePwdLog(password,userId);
    }

    @Override
    public Date selectByIdMax(Long userId) {
        return changePwdLogDao.selectByIdMax(userId);
    }
}
