package com.shanli.sys.dao;

import com.shanli.common.base.BaseDao;
import com.shanli.sys.domain.RoleDO;

/**
 * <pre>
 * 角色
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
public interface RoleDao extends BaseDao<RoleDO> {

}
