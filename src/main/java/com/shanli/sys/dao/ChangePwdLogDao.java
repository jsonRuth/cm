package com.shanli.sys.dao;

import com.shanli.sys.domain.ChangePwdLogDO;
import com.shanli.common.base.BaseDao;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 
 * <pre>
 * 修改密码记录表
 * </pre>
 * <small> 2019-05-23 09:35:52 | niush</small>
 */

public interface ChangePwdLogDao extends BaseDao<ChangePwdLogDO> {

    ChangePwdLogDO selectFiveChangePwdLog(@Param("password") String password,@Param("userId") long userId);

    Date selectByIdMax(Long userId);
}
