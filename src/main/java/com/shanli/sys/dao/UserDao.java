package com.shanli.sys.dao;

import com.shanli.common.base.BaseDao;
import com.shanli.sys.domain.UserDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <pre>
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */

public interface UserDao extends BaseDao<UserDO> {
	
	Long[] listAllDept();


    UserDO selectByMobile(String mobile);

    void updateByMobile(@Param(value = "id") Long id,@Param(value = "mobile")String mobile);

    UserDO selectByUsername(String username);

    Boolean updateByIdMobile(Integer id);
}
