package com.shanli.sys.domain;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.shanli.common.base.BaseDO;

import com.shanli.common.base.BaseDO2;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 
 * <pre>
 * 修改密码记录表
 * </pre>
 * <small> 2019-05-23 09:35:52 | niush</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("sys_changepwdlog")
@EqualsAndHashCode(callSuper=true)
public class ChangePwdLogDO extends BaseDO2 {
   @TableId(type= IdType.AUTO)
   private Integer id;
   @TableField("old_pwd")
  /** 原密码 */
 private String oldPwd;
   @TableField("new_pwd")
  /** 新密码 */
 private String newPwd;
   @TableField("update_reason")
  /** 修改原因 */
 private String updateReason;
   @TableField("create_time")
  /** 创建时间 */
 private Date createTime;
   @TableField("update_time")
  /** 修改时间 */
 private Date updateTime;

   @TableField("user_id")
 private Long userId;
}
