package com.shanli.job.jobs;



import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.service.collect.RepoUserInfoService;
import com.shanli.operate.service.echat.AccountService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class UserInfoJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    AccountService accountService;
    @Autowired
    RepoUserInfoService userInfoService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("用户统计定时任务开始==============");
        List<RepoUserInfoDO> userInfoDOList = accountService.getUserInfoList();
        int total = userInfoDOList.size();
        int init = 4000;
        for(int i = 0;i<total;i+=init){
            if(i+4000>total){
                init=total-i;
            }
            List<RepoUserInfoDO> newList = userInfoDOList.subList(i,i+init);
            userInfoService.insertOrUpdateList(newList);
        }
        logger.info("用户统计定时任务结束==============");
    }
}


