package com.shanli.job.jobs;

import com.shanli.operate.domain.collect.RepoActiveGroupDetailsDO;
import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;
import com.shanli.operate.service.collect.MessageRecordService;
import com.shanli.operate.service.collect.RepoActiveGroupDetailsService;
import com.shanli.operate.service.collect.RepoGroupInfoService;
import com.shanli.operate.service.collect.RepoStatisGroupService;
import com.shanli.operate.service.echat.ChatGroupService;
import groovyjarjarantlr.collections.impl.LList;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 群组昨日关键数据统计
 */
@Component
public class GroupYtdStatisJob implements Job{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ChatGroupService chatGroupService;
    @Autowired
    private RepoStatisGroupService repoGroupStatisService;
    @Autowired
    private MessageRecordService messageRecordService;
    @Autowired
    private RepoGroupInfoService repoGroupInfoService;
    @Autowired
    private RepoActiveGroupDetailsService repoActiveGroupDetailsService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("群组昨日到达值定时任务开始==============");
        List<RepoStatisGroupDO> listReach = chatGroupService.getGroupReachStatis();
        repoGroupStatisService.addBatch(listReach);
        logger.info("群组昨日到达值定时任务结束==============数量：" + listReach.size());

        logger.info("群组昨日活跃值定时任务开始==============");
        //查询活跃数据
        List<RepoStatisGroupDO> listActive = chatGroupService.getGroupActiveStatis();
        repoGroupStatisService.addBatch(listActive);
        logger.info("群组昨日活跃值定时任务结束==============数量：" + listActive.size());

        logger.info("群组昨日活跃详情定时任务开始==============");
        List<RepoActiveGroupDetailsDO> listActiveGroup = repoGroupInfoService.getActiveGroup();

        int total = listActiveGroup.size();
        int init = 4000;
        for(int i = 0;i<total;i+=init){
            if(i+4000>total){
                init=total-i;
            }
            List<RepoActiveGroupDetailsDO> newList = listActiveGroup.subList(i,i+init);
            repoActiveGroupDetailsService.insertByUpdate(newList);
        }

        logger.info("群组昨日活跃详情定时任务结束==============");

    }
}
