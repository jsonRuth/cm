package com.shanli.job.jobs;

import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoStatisUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.service.cm.CustomerInfoService;
import com.shanli.operate.service.collect.MessageRecordService;
import com.shanli.operate.service.collect.RepoActiveUserService;
import com.shanli.operate.service.collect.RepoStatisUserService;
import com.shanli.operate.service.collect.RepoUserInfoService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * 用户昨日关键数据统计
 */
@Component
public class UserYtdStatisJob implements Job{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomerInfoService customerInfoService;
    @Autowired
    private RepoStatisUserService repoStatisUserService;
    @Autowired
    private MessageRecordService messageRecordService;
    @Autowired
    private RepoUserInfoService repoUserInfoService;
    @Autowired
    private RepoActiveUserService repoActiveUserService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("用户昨日到达值定时任务开始==============");
        //到达数
        List<RepoStatisUserDO> list = new ArrayList<>();
        RepoStatisUserDO reachTotalUser = customerInfoService.getYtdReachTotalUser();
        list.add(reachTotalUser);
        RepoStatisUserDO reachFreeUser = customerInfoService.getYtdReachFreeUser();
        list.add(reachFreeUser);
        RepoStatisUserDO reachMember = customerInfoService.getYtdReachMember();
        list.add(reachMember);

        //活跃数
        RepoStatisUserDO activeTotalUser = messageRecordService.getYtdActiveTotalUser();
        list.add(activeTotalUser);
        RepoStatisUserDO activeFreeUser = messageRecordService.getYtdActiveFreeUser();
        list.add(activeFreeUser);
        RepoStatisUserDO activeMember = messageRecordService.getYtdActiveMember();
        list.add(activeMember);



        //增加数
        RepoStatisUserDO increaseTotalUser = customerInfoService.getYtdIncreaseTotalUser();
        list.add(increaseTotalUser);
        RepoStatisUserDO increaseFreeUser = customerInfoService.getYtdIncreaseFreeUser();
        list.add(increaseFreeUser);
        RepoStatisUserDO increaseMember = customerInfoService.getYtdIncreaseMember();
        list.add(increaseMember);

        repoStatisUserService.addBatch(list);

        //获取活跃值数据
        List<RepoActiveUserDO> repoActiveUserDOS = repoUserInfoService.getActiveUser();

        int total = repoActiveUserDOS.size();
        int init = 4000;
        for(int i = 0;i<total;i+=init){
            if(i+4000>total){
                init=total-i;
            }
            List<RepoActiveUserDO> newList = repoActiveUserDOS.subList(i,i+init);
            Boolean aBoolean = repoActiveUserService.insertByUpdate(newList);
        }
        //添加到用户活跃表
//        repoActiveUserService.insertByUpdate(repoActiveUserDOS);

        logger.info("用户昨日活跃值定时任务结束==============数量：" + list.size());
    }
}
