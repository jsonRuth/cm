package com.shanli.job.jobs;

import com.shanli.operate.domain.collect.RepoCommonLoginDO;
import com.shanli.operate.service.echat.AccountService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CommonLoginJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private AccountService accountService;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("常用登录表统计定时任务开始==============");
        List<RepoCommonLoginDO> commonLoginDOList = accountService.getCommonLoginIP();

        logger.info("常用登录表统计定时任务结束==============");

    }
}
