package com.shanli.job.jobs;

import com.shanli.operate.domain.collect.RepoFeedbackDO;
import com.shanli.operate.service.cm.FeedbackService;
import com.shanli.operate.service.collect.RepoFeedbackService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时查询意见反馈数量
 */
@Component
public class FeedbackJob implements Job{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private RepoFeedbackService repoFeedbackService;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("意见反馈定时任务开始==============");
        Integer count = feedbackService.getFeedbackCount();
        RepoFeedbackDO repoFeedbackDO = new RepoFeedbackDO();
        repoFeedbackDO.setTotalCount((count == null)?0:count);
        repoFeedbackService.insert(repoFeedbackDO);
        logger.info("意见反馈定时任务结束==============数量："+count);
    }
}
