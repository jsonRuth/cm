package com.shanli.job.jobs;


import com.shanli.operate.domain.collect.RepoActiveGroupDO;
import com.shanli.operate.domain.collect.RepoChannelSourceDO;
import com.shanli.operate.service.cm.CustomerInfoService;
import com.shanli.operate.service.collect.RepoChannelSourceSerive;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChannelSourceJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RepoChannelSourceSerive channelSourceSerive;
    @Autowired
    private CustomerInfoService customerInfoService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("渠道来源表定时任务开始==============");

        List<RepoChannelSourceDO> channelSourceDOS = customerInfoService.selectChannelSource();

        boolean delete = channelSourceSerive.delete(null);

        channelSourceSerive.insertOrUpdateList(channelSourceDOS);

        logger.info("渠道来源表定时任务结束==============");
    }
}
