package com.shanli.job.jobs;

import com.shanli.operate.domain.collect.RepoGroupInfoDO;
import com.shanli.operate.domain.echat.AccountDO;
import com.shanli.operate.domain.echat.UserOfGroupDo;
import com.shanli.operate.service.collect.RepoGroupInfoService;
import com.shanli.operate.service.echat.AccountService;
import com.shanli.operate.service.echat.ChatGroupService;
import com.shanli.operate.service.echat.UserOfGroupService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.*;

public class GroupInfoJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ChatGroupService chatGroupService;
    @Autowired
    RepoGroupInfoService groupInfoService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("群组统计定时任务开始==============");
        List<RepoGroupInfoDO> repoGroupInfoDOSList = chatGroupService.getGroupInfo();
        int total = repoGroupInfoDOSList.size();
        int init = 10000;
        for(int i = 0;i<total;i+=init){
            if(i+10000>total){
                init=total-i;
            }
            List<RepoGroupInfoDO> newList = repoGroupInfoDOSList.subList(i,i+init);
            groupInfoService.insertOrUpdateList(newList);
        }

        logger.info("群组统计定时任务结束==============");
    }
}
