package com.shanli.job.jobs;

import com.shanli.operate.domain.collect.RepoUserLockLogDO;
import com.shanli.operate.service.cm.UserLockLogService;
import com.shanli.operate.service.collect.RepoUserLockLogService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时查询登录失败锁定次数
 */
@Component
public class LoginFailLockJob implements Job{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserLockLogService userLockLogService;
    @Autowired
    private RepoUserLockLogService repoUserLockLogService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("登录失败锁定定时任务开始==============");
        //查询最近七天登录失败人次（七天内每个用户最多算一次）
        Integer count = userLockLogService.getLoginFailLockCount();
        RepoUserLockLogDO repoUserLockLogDO = new RepoUserLockLogDO();
        repoUserLockLogDO.setTotalCount((count == null)?0:count);
        repoUserLockLogService.insert(repoUserLockLogDO);
        logger.info("登录失败锁定定时任务结束==============数量："+count);
    }
}
