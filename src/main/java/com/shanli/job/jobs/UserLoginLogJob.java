package com.shanli.job.jobs;


import com.shanli.operate.domain.collect.RepoUserLoginLogDO;
import com.shanli.operate.service.collect.RepoUserLoginLogService;
import com.shanli.operate.service.echat.AccountService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserLoginLogJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AccountService accountService;
    @Autowired
    private RepoUserLoginLogService userLoginLogService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("用户登录统计定时任务开始==============");

        List<RepoUserLoginLogDO> userLoginLogDOList = accountService.getUserLoginLogByYesterDay();
        int total = userLoginLogDOList.size();
        int init = 10000;
        for(int i = 0;i<total;i+=init){
            if(i+10000>total){
                init=total-i;
            }
            List<RepoUserLoginLogDO> newList = userLoginLogDOList.subList(i,i+init);
            userLoginLogService.insertOrUpdateList(newList);
        }
        logger.info("用户登录统计定时任务结束==============");
    }
}
