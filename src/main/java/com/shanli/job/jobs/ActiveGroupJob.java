package com.shanli.job.jobs;

import com.shanli.operate.domain.collect.RepoActiveGroupDO;
import com.shanli.operate.service.collect.MessageRecordService;
import com.shanli.operate.service.collect.RepoActiveGroupService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

/**
 * 活跃群组数
 */
@Component
public class ActiveGroupJob implements Job{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MessageRecordService messageRecordService;
    @Autowired
    private RepoActiveGroupService repoActiveGroupService;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("活跃群组定时任务开始==============");
        Integer count = messageRecordService.getCurActiveGroupCount();
        RepoActiveGroupDO repoActiveGroupDO = new RepoActiveGroupDO();
        repoActiveGroupDO.setTotalCount((count == null)?0:count);

        //查询当前的时间点（小时）
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        repoActiveGroupDO.setPointTime(hour);
        repoActiveGroupService.insert(repoActiveGroupDO);
        logger.info("活跃群组定时任务结束==============数量："+count);
    }
}
