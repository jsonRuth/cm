package com.shanli.job.jobs;

import com.shanli.operate.domain.collect.RepoOnlineUserDO;
import com.shanli.operate.service.collect.RepoOnlineUserService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Set;

/**
 * 在线用户数
 */
@Component
public class OnlineUserJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource(name = "secondRedisTemplate")
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RepoOnlineUserService repoOnlineUserService;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("在线用户数定时任务开始==============");
        Integer count = getCurUserOnlineCount().intValue();
        RepoOnlineUserDO repoOnlineUserDO = new RepoOnlineUserDO();

        //查询当前的时间点（小时）
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        repoOnlineUserDO.setPointTime(hour);
        repoOnlineUserDO.setTotalCount((count == null) ? 0 : count);
        repoOnlineUserService.insert(repoOnlineUserDO);
        logger.info("在线用户数定时任务结束==============数量：" + count);
    }

    //从redis查询在线用户数
    public Long getCurUserOnlineCount() {
        Long onLine = 0L;
        Set<String> allKeys = stringRedisTemplate.keys("ONLINE:UID:*");
        for (String key : allKeys) {
            Long temp = stringRedisTemplate.opsForHash().size(key);
            if (temp != null) {
                onLine += temp;
            }
        }
        return onLine;
    }
}
