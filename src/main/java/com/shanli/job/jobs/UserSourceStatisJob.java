package com.shanli.job.jobs;


import com.shanli.operate.domain.collect.RepoUserSourceDO;
import com.shanli.operate.service.cm.CustomerInfoService;
import com.shanli.operate.service.collect.RepoUserSourceService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * 渠道来源数据统计
 */
@Component
public class UserSourceStatisJob implements Job{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RepoUserSourceService repoUserSourceService;
    @Autowired
    private CustomerInfoService customerInfoService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("渠道来源数据统计定时任务开始==============");
        List<RepoUserSourceDO> list = customerInfoService.getUserSourceStatis();
        repoUserSourceService.addBatch(list);
        logger.info("渠道来源数据统计定时任务结束==============数量：" + list.size());
    }
}
