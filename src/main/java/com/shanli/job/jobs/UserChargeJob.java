package com.shanli.job.jobs;


import com.shanli.operate.domain.collect.RepoUserChargeDO;
import com.shanli.operate.service.collect.RepoUserChargeService;
import com.shanli.operate.service.echat.AccountService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserChargeJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RepoUserChargeService userChargeService;
    @Autowired
    private AccountService accountService;
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("用户缴费统计定时任务开始==============");

        List<RepoUserChargeDO> userChargeDOList = accountService.getUserChargeByYesterDay();;
        int total = userChargeDOList.size();
        int init = 10000;
        for(int i = 0;i<total;i+=init){
            if(i+10000>total){
                init=total-i;
            }
            List<RepoUserChargeDO> newList = userChargeDOList.subList(i,i+init);
           userChargeService.insertList(newList);
        }

        logger.info("用户缴费统计定时任务结束==============");
    }
}
