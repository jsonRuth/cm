package com.shanli.job.listenner;

import com.shanli.operate.domain.echat.AccountDO;
import com.shanli.operate.domain.echat.ChatGroupDO;
import com.shanli.operate.service.echat.AccountService;
import com.shanli.operate.service.echat.ChatGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {
    @Autowired
    private ChatGroupService chatGroupService;
    @Autowired
    private AccountService accountService;

    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 用户做自己的业务处理即可,注意message.toString()可以获取失效的key
        String expiredKey = message.toString();
        if (expiredKey.indexOf(":group") !=-1){
            String[] split = expiredKey.split(":");
            long Cg_ID = Long.parseLong(split[0]);
            ChatGroupDO chatGroupDO = chatGroupService.selectById(Cg_ID);
            if (chatGroupDO!=null){
                    chatGroupDO.setSpeechStatus(1);
                    chatGroupDO.setCgBannedTime(null);
                    chatGroupDO.setCgBannedLength(null);
                try {
                    chatGroupService.updateById(chatGroupDO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else if(expiredKey.indexOf(":account") !=-1){
            String[] split = expiredKey.split(":");
            Integer user_id = Integer.valueOf(split[0]);
            AccountDO accountDO = accountService.selectById(user_id);
            if (accountDO!=null){
                accountDO.setIsUseable((byte) 0);
                accountDO.setUserBannedTime(null);
                accountDO.setUserBannedLength(null);
                try {
                    accountService.updateById(accountDO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println(expiredKey);
    }
}
