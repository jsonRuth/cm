package com.shanli.job.dao;

import com.shanli.common.base.BaseDao;
import com.shanli.job.domain.TaskDO;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 15:45:42
 */
public interface TaskDao extends BaseDao<TaskDO> {
    
}
