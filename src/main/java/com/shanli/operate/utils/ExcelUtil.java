package com.shanli.operate.utils;


import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * excel导入导出
 */
public class ExcelUtil {

    /**
     * 导出excel
     *
     * @param list
     * @param response
     * @param title
     * @param columns
     * @param sheetName
     * @param fileName
     */
    public static void exportExcel(List<Map<String, Object>> list, HttpServletResponse response,
                                   String[] title, String[] columns, String sheetName, String fileName) {
        String[][] values = new String[list.size()][];
        //循环list集合
        for (int i = 0; i < list.size(); i++) {
            values[i] = new String[title.length];
            Map<String, Object> map = list.get(i);

            //按照导出的列排序
            List<String> tempList = new ArrayList<>();
            for (int j = 0; j < columns.length; j++) {
                Object val = map.get(columns[j]);
                tempList.add(val != null ? val.toString() : "");
            }

            //填充列数据
            for (int z = 0; z < tempList.size(); z++) {
                values[i][z] = tempList.get(z);
            }
        }
        XSSFWorkbook wb = ExcelUtil.getXSSFWorkbook(sheetName, title, values, null);
        // 将文件存到指定位置
        try {
            ExcelUtil.setResponseHeader(response, fileName);
            OutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void exportExcel(List<LinkedHashMap<String, Object>> list, HttpServletResponse response,
                                   String[] title, String sheetName, String fileName) {

        XSSFWorkbook wb = getXSSFWorkbook(sheetName,title,list,null);

        // 将文件存到指定位置
        try {
            setResponseHeader(response, fileName);
            OutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }





    /**
     * 设置请求头部
     *
     * @param response
     * @param fileName
     */
    private static void setResponseHeader(HttpServletResponse response, String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(), "ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            //response.setContentType("application/msexcel");
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static XSSFWorkbook getXSSFWorkbook(String sheetName, String[] title, String[][] values, XSSFWorkbook wb) {

        // 第一步，创建一个HSSFWorkbook，对应一个Excel文件
        if (wb == null) {
            wb = new XSSFWorkbook();
        }

        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = wb.createSheet(sheetName);

        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制
        XSSFRow row = sheet.createRow(0);

        // 第四步，创建单元格，并设置值表头 设置表头居中
        XSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

        //声明列对象
        XSSFCell cell = null;

        //创建标题
        for (int i = 0; i < title.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(title[i]);
            cell.setCellStyle(style);
        }

        //创建内容
        for (int i = 0; i < values.length; i++) {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < values[i].length; j++) {
                //将内容按顺序赋给对应的列对象
                row.createCell(j).setCellValue(values[i][j]);
            }
        }
        return wb;
    }




    public static XSSFWorkbook getXSSFWorkbook(String sheetName, String[] title, List<LinkedHashMap<String,Object>> list, XSSFWorkbook wb) {

        // 第一步，创建一个HSSFWorkbook，对应一个Excel文件
        if (wb == null) {
            wb = new XSSFWorkbook();
        }

        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = wb.createSheet(sheetName);

        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制
        XSSFRow row = sheet.createRow(0);

        // 第四步，创建单元格，并设置值表头 设置表头居中
        XSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

        //声明列对象
        XSSFCell cell = null;

        //创建标题
        for (int i = 0; i < title.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(title[i]);
            cell.setCellStyle(style);
        }

        //创建内容
        Map<String,Object> dataMap;
        for (int i = 0; i < list.size(); i++) {
            row = sheet.createRow(i + 1);
            dataMap = list.get(i);
            int index = 0;
            for(Object item: dataMap.values()){
                cell = row.createCell(index++);
                cell.setCellValue(item.toString());
            }
        }
      return wb;
    }






    /*读取xlsx文件*/
    public static List<Map<String, Object>> readExcelByFileForXlsx(InputStream file) {
        List<Map<String, Object>> varList = new ArrayList<>();
        try {
            //xslx
            XSSFWorkbook wb = new XSSFWorkbook(file);
            //sheet 从0开始
            XSSFSheet sheet = wb.getSheetAt(0);

            Row titleRow = sheet.getRow(0);
            //取得最后一行的行号
            int rowNum = sheet.getLastRowNum() + 1;
            //行循环开始
            for (int i = 1; i < rowNum; i++) {
                //行
                XSSFRow row = sheet.getRow(i);
                //每行的最后一个单元格位置
                int cellNum = row.getLastCellNum();
                Map<String, Object> varpd = new HashMap<>(cellNum);
                //列循环开始
                for (int j = 0; j < cellNum; j++) {

                    XSSFCell cell = row.getCell(Short.parseShort(j + ""));
                    String cellValue = null;
                    if (null != cell) {
                        // 判断excel单元格内容的格式，并对其进行转换，以便插入数据库
                        switch (cell.getCellType()) {
                            case 0:
                                cellValue = String.valueOf((long) cell.getNumericCellValue());
                                break;
                            case 1:
                                cellValue = cell.getStringCellValue();
                                break;
                            case 2:
                                cellValue = cell.getNumericCellValue() + "";
                                // cellValue = String.valueOf(cell.getDateCellValue());
                                break;
                            case 3:
                                cellValue = "";
                                break;
                            case 4:
                                cellValue = String.valueOf(cell.getBooleanCellValue());
                                break;
                            case 5:
                                cellValue = String.valueOf(cell.getErrorCellValue());
                                break;
                        }
                    } else {
                        cellValue = "";
                    }

                    varpd.put(titleRow.getCell(j).getStringCellValue(), cellValue);

                }
                varList.add(varpd);
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return varList;
    }

    /**
     * 下载excel模板文件
     * @param response
     * @param fileName
     */
    public static void downloadExcel(HttpServletResponse response, String fileName,InputStream in) {
        //设置要下载的文件的名称
        response.setHeader("Content-disposition", "attachment;fileName=" + fileName);
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        //通知客服文件的MIME类型
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        try {
            response.setHeader("Content-Disposition", "attachment;filename="
                    + new String(fileName.getBytes(),"iso-8859-1"));
//            FileInputStream in = new FileInputStream(resource.getFile());
            OutputStream out = response.getOutputStream();
            byte[] b = new byte[2048];
            int len;
            while ((len = in.read(b)) != -1) {
                out.write(b, 0, len);
            }

            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
