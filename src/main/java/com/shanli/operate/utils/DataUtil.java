package com.shanli.operate.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * 判断变量是否为空
 */
public class DataUtil {
    /**
     * 判断object类型字符串是否为空
     * @param object
     * @return
     */
    public static boolean ObjectIsNull(Object object){
        boolean flag = false;
        if(object == null){
            flag = true;
        }else if(StringUtils.isEmpty(object.toString().trim())){
            flag = true;
        }
        return flag;
    }
}
