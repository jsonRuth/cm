package com.shanli.operate.service.echat;


import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoGroupInfoDO;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;
import com.shanli.operate.domain.echat.ChatGroupDO;

import java.util.Date;
import java.util.List;

public interface ChatGroupService extends CoreService<ChatGroupDO> {

    /**
     * 查询群组到达值统计
     * @return
     */
    List<RepoStatisGroupDO> getGroupReachStatis();

    /**
     * 查询群组活跃值统计
     * @return
     */
    List<RepoStatisGroupDO> getGroupActiveStatis();

    /**
     * 根据用户id查询创建群组数
     * @param creator
     * @return
     */
    Integer selectCreatorCount(Integer creator);

    /**
     * 定时任务查询群组统计
     * @return
     */
    List<RepoGroupInfoDO> getGroupInfo();

    /**
     * 根据id删除tb_chatgroup(逻辑删除)
     * @param id
     * @return
     */
    Boolean updateByChatGroupIsActive(Long id);

    /**
     * 根据id禁言
     * @param id
     * @param cgBannedLength
     * @param cgBannedTime
     * @return
     */
    Boolean updateByIdChatGroup(Long id, Integer cgBannedLength, Date cgBannedTime);

    /**
     * 根据cg_id 恢复禁言
     * @param cg_id
     * @return
     */
    Boolean updateByChatGroup(Long cg_id);
}
