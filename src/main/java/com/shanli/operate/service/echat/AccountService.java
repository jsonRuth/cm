package com.shanli.operate.service.echat;

import com.shanli.operate.domain.collect.RepoCommonLoginDO;
import com.shanli.operate.domain.collect.RepoUserChargeDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.domain.collect.RepoUserLoginLogDO;
import com.shanli.operate.domain.echat.AccountDO;
import com.shanli.common.base.CoreService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2019-06-17 17:54:43 | niush</small>
 */
public interface AccountService extends CoreService<AccountDO> {
    /**
     * 通过id修改测试账号
     * @param accountDO
     * @return
     */
    int updateAccountSelective(AccountDO accountDO);

    /**
     * 批量添加
     * @param list
     * @return
     */
    Integer addBatch(List<AccountDO> list);

    /**
     * 批量重置测试账号密码
     * @param ids
     * @param password
     * @return
     */
    Integer resetTestAccountPwd(List<Integer> ids,String password);
    /**
     * 通过账号集合查询导入账号列表
     * @param accountList
     * @return
     */
    List<AccountDO> getUserAccountByNames(List<String> accountList);

    /**
     * 批量添加账号
     * @param params
     * @return
     */
    Integer addAccountBatch(Map<String,Map<String,AccountDO>> params);

    /**
     * 通过用户账号查询id
     * @param userAccount
     * @return
     */
    Integer selectByUserAccount(String userAccount);

    /**
     * 定时任务查询用户统计
     * @param isActive
     * @return
     */
    List<RepoUserInfoDO> getUserInfo(Integer isActive);

    /**
     * 通过账号集合查询全部账号列表（包含已删除用户）
     * @param accountList
     * @return
     */
    List<AccountDO> getUserAccountDeletedByNames(List<String> accountList);

    /**
     * 将已存在的用户（正式用户、已删除用户）转为测试用户
     * @param accountList
     * @return
     */
    Integer switchAccountTest(List<String> accountList,AccountDO accountDO);

    /**
     * 正式用户转测试用户时，保存初始的会员有效期
     * @param accountList
     * @return
     */
    Integer saveAccountService(List<String> accountList);

    /**
     * 批量添加用户，如果用户存在则更新
     * @param list
     * @return
     */
    Integer addBatchOnUpdate(List<AccountDO> list);

    /**
     * 查询用户统计报表
     * @return
     */
    List<RepoUserInfoDO> getUserInfoList();

    /**
     * 根据id禁用用户
     * @param id
     * @param userBannedLength
     * @param userBannedTime
     * @return
     */
    Boolean updateByIdUser(Integer id, Integer userBannedLength, Date userBannedTime);

    /**
     * 根据id恢复用户
     * @param user_id
     * @return
     */
    Boolean updateByUserRecover(Integer user_id);

    /**
     * 批量更新过期时间和会员过期时间
     * @param param
     * @return
     */
    int batchUpdateExpireTime(Map<String, Object> param);

    /**
     * 批量删除用户
     * @param param
     */
    int batchDeleteUserInfo(Map<String, Object> param);

    /**
     * 批量禁用
     * @param param
     * @return
     */
    int batchLockAccount(Map<String, Object> param);

    /**
     * 批量解禁
     * @param param
     * @return
     */
    int batchUnlockAccount(Map<String, Object> param);
    /**
     * 查询昨天用户登录记录
     * @return
     */
    List<RepoUserLoginLogDO> getUserLoginLogByYesterDay();

    /**
     * 查询昨天用户缴费记录
     * @return
     */
    List<RepoUserChargeDO> getUserChargeByYesterDay();

    /**
     * 根据ids 批量查询账号
     * @param userIds
     * @return
     */
    List<String> selectByUserIds(String[] userIds);

    /**
     * 查询登录对照表
     * @return
     */
    List<RepoCommonLoginDO> getCommonLoginIP();

    /**
     * 账号的禁用和解禁
     * @param param
     */
    void updateAccountForbidden(Map<String, Object> param);
}
