package com.shanli.operate.service.echat;

import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.echat.UserOfGroupDo;

import java.util.List;

public interface UserOfGroupService extends CoreService<UserOfGroupDo> {
    /**
     * 根据群组Id 查询用户Id
     * @param cgid
     * @return
     */
    List<Integer> selectByCgid(Long cgid);

    /**
     * 根据用户id查询群组id
     * @param userId
     * @return
     */
    List<Long> selectByUserId(Integer userId);

    /**
     * 根据用户id查询群组数
     * @param user_id
     * @return
     */
    Integer selectGroupCount(Integer user_id);

    /**
     * 根据Id删除repo_groupinfo(逻辑删除)
     * @param userId
     */
    Boolean updateByIsActive(Integer userId);

    /**
     * 根据cgid删除tb_userofgroup(逻辑删除)
     * @param cgid
     * @return
     */
    Boolean updateByCgidIsActive(Long cgid);
}
