package com.shanli.operate.service.cm;


import com.shanli.common.base.CoreService;
import com.shanli.operate.dao.cm.UserLockLogDao;
import com.shanli.operate.domain.cm.UserLockLogDO;

import java.util.List;

public interface UserLockLogService extends CoreService<UserLockLogDO> {

    /**
     * 查询用户登录失败锁定数量
     * @return
     */
    Integer getLoginFailLockCount();

    /**
     * 查询锁定用户数
     * @return
     */
    List<String> getLoginFailLockList();

}
