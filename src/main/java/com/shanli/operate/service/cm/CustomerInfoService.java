package com.shanli.operate.service.cm;


import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.cm.CustomerInfoDO;
import com.shanli.operate.domain.collect.RepoChannelSourceDO;
import com.shanli.operate.domain.collect.RepoStatisUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.domain.collect.RepoUserSourceDO;

import java.util.List;

public interface CustomerInfoService extends CoreService<CustomerInfoDO> {

    /**
     * 查询昨日到达总用户数
     * @return
     */
    RepoStatisUserDO getYtdReachTotalUser();

    /**
     * 查询昨日到达免费用户
     * @return
     */
    RepoStatisUserDO getYtdReachFreeUser();

    /**
     * 查询昨日到达会员数
     * @return
     */
    RepoStatisUserDO getYtdReachMember();

    /**
     * 查询昨日新增总用户数
     * @return
     */
    RepoStatisUserDO getYtdIncreaseTotalUser();

    /**
     * 查询昨日新增免费用户数
     * @return
     */
    RepoStatisUserDO getYtdIncreaseFreeUser();

    /**
     * 查询昨日新增会员数
     * @return
     */
    RepoStatisUserDO getYtdIncreaseMember();

    /**
     * 查询用户渠道来源统计
     * @return
     */
    List<RepoUserSourceDO> getUserSourceStatis();

    /**
     * 根据登录名查询渠道来源
     * @param customername
     * @return
     */
    CustomerInfoDO selectBySubType(String customername);

    /**
     * 查询渠道来源数据表
     * @return
     */
    List<RepoChannelSourceDO> selectChannelSource();
}
