package com.shanli.operate.service.cm;


import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.cm.FeedbackDO;

import java.util.Date;
import java.util.List;

public interface FeedbackService extends CoreService<FeedbackDO> {

    /**
     * 查询意见反馈数量
     * @return
     */
    Integer getFeedbackCount();

    /**
     * 保存回复信息
     * @param replyContent
     * @param i
     * @param id
     */
    Integer updateReplyContent(String replyContent, int i, Integer id, String replyAccount , Date recoveryTime);

    /**
     * 查询意见反馈数据
     * @return
     */
    List<String> getFeedbackTotalData();

}
