package com.shanli.operate.service.cm;

import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.cm.OrderinfoDO;
import org.springframework.stereotype.Service;

import java.util.List;

public interface OrderinfoService extends CoreService<OrderinfoDO> {

    /**
     * 通过手机集合查询客户列表
     * @param userPhone
     * @return
     */
    List<OrderinfoDO> getOrderinfoByPhone(List<String> userPhone);
}
