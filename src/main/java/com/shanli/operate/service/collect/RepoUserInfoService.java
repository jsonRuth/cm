package com.shanli.operate.service.collect;

import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoStatisUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;

import java.util.List;


public interface RepoUserInfoService extends CoreService<RepoUserInfoDO> {
    /**
     * 批量添加用户报表
     * @param userInfoDOList
     */
    Boolean insertOrUpdateList(List<RepoUserInfoDO> userInfoDOList);

    /**
     * 条件查询分页报表
     * @param pageNumber
     * @param pageSize
     * @param userInfo
     * @return
     */
    Page<RepoUserInfoDO> selectPageUser(Integer pageNumber, Integer pageSize, RepoUserInfoDO userInfo);

    /**
     * 根据群组cgid查询分页报表
     * @param pageNumber
     * @param pageSize
     * @param userId
     * @param userInfo
     * @return
     */
    Page<RepoUserInfoDO>  selectByCgidPageUser(Integer pageNumber, Integer pageSize, List<Integer> userId, RepoUserInfoDO userInfo);

    /**
     * 查询最近七天登录失败的用户
     * @param pageNumber
     * @param pageSize
     * @param userInfo
     * @return
     */
    Page<RepoUserInfoDO> selectPageUserLoginFail(Integer pageNumber, Integer pageSize, RepoUserInfoDO userInfo,String beginTime, String endTime);

    /**
     * 查询登录失败的用户
     * @return
     */
    Integer getLastTotalCount();

    /**
     * 查询昨日活跃值数据
     * @return
     */
    List<RepoActiveUserDO> getActiveUser();


//    /**
//     * 用户到达值
//     * @return
//     */
//    List<RepoStatisUserDO> getTotalUser();

}
