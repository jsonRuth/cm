package com.shanli.operate.service.collect;


import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoUserSourceDO;

import java.util.List;

public interface RepoUserSourceService extends CoreService<RepoUserSourceDO>{

    /**
     * 渠道来源数据统计
     * @return
     */
    List<RepoUserSourceDO> getUserSourceStatis();

    /**
     * 批量添加
     * @param list
     * @return
     */
    Integer addBatch(List<RepoUserSourceDO> list);

}
