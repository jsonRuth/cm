package com.shanli.operate.service.collect;

import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoUserChargeDO;

import java.util.List;

public interface RepoUserChargeService extends CoreService<RepoUserChargeDO> {

    /**
     * 批量添加用户缴费记录
     * @param userChargeDOList
     * @return
     */
    Boolean insertList(List<RepoUserChargeDO> userChargeDOList);
}
