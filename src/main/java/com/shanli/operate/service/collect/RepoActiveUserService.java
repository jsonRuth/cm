package com.shanli.operate.service.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;

import java.util.List;

public interface RepoActiveUserService extends CoreService<RepoActiveUserDO> {


    /**
     * 批量添加到用户活跃表
     * @param repoActiveUserDOS
     */
    Boolean insertByUpdate(List<RepoActiveUserDO> repoActiveUserDOS);

    /**
     * 查询用户活跃数据
     * @param pageNumber
     * @param pageSize
     * @param userActive
     * @return
     */
    Page<RepoActiveUserDO> selectPageUser(Integer pageNumber, Integer pageSize, RepoActiveUserDO userActive);
}
