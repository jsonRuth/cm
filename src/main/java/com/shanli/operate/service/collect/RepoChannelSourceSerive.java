package com.shanli.operate.service.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoChannelSourceDO;
import org.springframework.stereotype.Service;

import java.util.List;

public interface RepoChannelSourceSerive extends CoreService<RepoChannelSourceDO> {

    /**
     * 批量添加渠道来源数据
     * @param channelSourceDOS
     * @return
     */
    Boolean insertOrUpdateList(List<RepoChannelSourceDO> channelSourceDOS);

    /**
     * 查询全部用户的渠道来源数据
     * @param pageNumber
     * @param pageSize
     * @return
     */
    Page<RepoChannelSourceDO> selectByList(Integer pageNumber, Integer pageSize);

    /**
     * 查询免费用户渠道来源
     * @param pageNumber
     * @param pageSize
     * @return
     */
    Page<RepoChannelSourceDO> selectByListFree(Integer pageNumber, Integer pageSize);

    /**
     * 查询会员用户渠道来源
     * @param pageNumber
     * @param pageSize
     * @return
     */
    Page<RepoChannelSourceDO> selectBylistType(Integer pageNumber, Integer pageSize);

    /**
     * 查询总数据统计
     * @return
     */
    RepoChannelSourceDO getChannelSourceSratis();

}
