package com.shanli.operate.service.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoActiveGroupDetailsDO;

import java.util.List;


public interface RepoActiveGroupDetailsService extends CoreService<RepoActiveGroupDetailsDO> {


    /**
     * 批量添加群组活跃数据
     * @param listActiveGroup
     */
    void insertByUpdate(List<RepoActiveGroupDetailsDO> listActiveGroup);

    /**
     * 查询群组活跃数据
     * @param pageNumber
     * @param pageSize
     * @param activeGroup
     * @return
     */
    Page<RepoActiveGroupDetailsDO> selectActivePageGroup(Integer pageNumber, Integer pageSize, RepoActiveGroupDetailsDO activeGroup);
}
