package com.shanli.operate.service.collect;


import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoActiveGroupDO;

import java.util.List;

public interface RepoActiveGroupService extends CoreService<RepoActiveGroupDO>{

    /**
     * 查询活跃群组
     * @return
     */
    List<RepoActiveGroupDO> getActiveGroupStatis();
}
