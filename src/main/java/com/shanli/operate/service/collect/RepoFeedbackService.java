package com.shanli.operate.service.collect;


import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoFeedbackDO;

public interface RepoFeedbackService extends CoreService<RepoFeedbackDO>{

    /**
     * 查询意见反馈报表最后一条记录
     * @return
     */
    Integer getLastTotalCount();
}
