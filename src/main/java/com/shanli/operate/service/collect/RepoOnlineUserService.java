package com.shanli.operate.service.collect;


import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoOnlineUserDO;

import java.util.List;

public interface RepoOnlineUserService extends CoreService<RepoOnlineUserDO>{

    /**
     * 查询昨日用户在线数
     * @return
     */
    List<RepoOnlineUserDO> getOnlineUserStatis();
}
