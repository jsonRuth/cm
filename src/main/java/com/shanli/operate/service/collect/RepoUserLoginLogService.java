package com.shanli.operate.service.collect;

import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoUserLoginLogDO;

import java.util.List;

public interface RepoUserLoginLogService extends CoreService<RepoUserLoginLogDO> {
    /**
     * 批量添加用户登录信息报表
     * @param userLoginLogDOList
     * @return
     */
    Boolean insertOrUpdateList(List<RepoUserLoginLogDO> userLoginLogDOList);
}
