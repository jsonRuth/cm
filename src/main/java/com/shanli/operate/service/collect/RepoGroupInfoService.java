package com.shanli.operate.service.collect;

import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoActiveGroupDetailsDO;
import com.shanli.operate.domain.collect.RepoGroupInfoDO;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;

import java.util.List;


public interface RepoGroupInfoService extends CoreService<RepoGroupInfoDO> {

    /**
     * 批量添加repo_groupinfo
     * @param repoGroupInfoDOSList
     * @return
     */
    Boolean insertOrUpdateList(List<RepoGroupInfoDO> repoGroupInfoDOSList);

    /**
     * 通过id删除repo_groupinfo(逻辑删除)
     * @param id
     * @return
     */
    Boolean updateByIsActive(Long id);


    /**
     * 条件分页查询
     * @param pageNumber
     * @param pageSize
     * @param groupInfo
     * @return
     */
    Page<RepoGroupInfoDO> selectPageGroup(Integer pageNumber , Integer pageSize ,RepoGroupInfoDO groupInfo);

    /**
     * 根据群组id条件分页查询
     * @param pageNumber
     * @param pageSize
     * @param cgids
     * @param groupInfo
     * @return
     */
    Page<RepoGroupInfoDO> selectPageByCgids(Integer pageNumber, Integer pageSize, List<Long> cgids, RepoGroupInfoDO groupInfo);

    /**
     * 查询到达的群组数
     * @param pageNumber
     * @param pageSize
     * @param groupInfo
     * @return
     */
    Page<RepoGroupInfoDO> selectArrivePageGroup(Integer pageNumber, Integer pageSize, RepoGroupInfoDO groupInfo);

    /**
     * 查询活跃的群组数
     * @param pageNumber
     * @param pageSize
     * @param groupInfo
     * @return
     */
    Page<RepoGroupInfoDO> selectActivePageGroup(Integer pageNumber, Integer pageSize, RepoGroupInfoDO groupInfo);

    /**
     * 查询到达的群组数
     * @return
     */
    List<RepoStatisGroupDO> getGroupStatis();

    /**
     * 查询活跃的群组数
     * @return
     */
    List<RepoStatisGroupDO> getGroupStatisTwo();

    /**
     * 查询活跃详情数据
     * @return
     */
    List<RepoActiveGroupDetailsDO> getActiveGroup();

}
