package com.shanli.operate.service.collect;


import com.shanli.common.base.CoreService;
import com.shanli.operate.domain.collect.RepoStatisUserDO;

import java.util.List;

public interface RepoStatisUserService extends CoreService<RepoStatisUserDO>{

    /**
     * 用户昨日关键数据统计
     * @return
     */
    List<RepoStatisUserDO> getUserYtdStatis();

    /**
     * 批量添加
     * @param list
     * @return
     */
    Integer addBatch(List<RepoStatisUserDO> list);

}
