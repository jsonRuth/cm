package com.shanli.operate.service.impl.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoUserInfoDao;
import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoStatisUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.service.collect.RepoUserInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepoUserInfoServiceImpl extends CoreServiceImpl<RepoUserInfoDao,RepoUserInfoDO> implements RepoUserInfoService {


    /**
     * 批量添加用户报表
     * @param userInfoDOList
     */
    @Override
    public Boolean insertOrUpdateList(List<RepoUserInfoDO> userInfoDOList) {
        return this.baseMapper.insertOrUpdateList(userInfoDOList);
    }

    /**
     * 条件查询分页报表
     * @param pageNumber
     * @param pageSize
     * @param userInfo
     * @return
     */
    @Override
    public Page<RepoUserInfoDO> selectPageUser(Integer pageNumber, Integer pageSize, RepoUserInfoDO userInfo) {
        Page<RepoUserInfoDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectPageUser(page,userInfo));
        return page;
    }

    /**
     * 根据群组cgid查询分页报表
     * @param pageNumber
     * @param pageSize
     * @param userId
     * @param userInfo
     * @return
     */
    @Override
    public Page<RepoUserInfoDO> selectByCgidPageUser(Integer pageNumber, Integer pageSize, List<Integer> userId, RepoUserInfoDO userInfo) {
        Page<RepoUserInfoDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectByCgidPageUser(page,userId,userInfo));
        return page;
    }

    /**
     * 查询最近七天登录失败的用户
     * @param pageNumber
     * @param pageSize
     * @param userInfo
     * @return
     */
    @Override
    public Page<RepoUserInfoDO> selectPageUserLoginFail(Integer pageNumber, Integer pageSize, RepoUserInfoDO userInfo,String beginTime, String endTime) {
        Page<RepoUserInfoDO> page = new Page<>(pageNumber,pageSize);
        endTime=endTime + " 23:59:59";
        page.setRecords(this.baseMapper.selectPageUserLoginFail(page,userInfo,beginTime,endTime));
        return page;
    }

    /**
     * 查询最近7天登录失败次数
     * @return
     */
    @Override
    public Integer getLastTotalCount() {
        return this.baseMapper.getLastTotalCount();
    }

    /**
     * 查询昨日活跃用户数据
     * @return
     */
    @Override
    public List<RepoActiveUserDO> getActiveUser() {
        return this.baseMapper.getActiveUser();
    }

//    /**
//     * 用户到达值
//     * @return
//     */
//    @Override
//    public List<RepoStatisUserDO> getTotalUser() {
//        return this.baseMapper.getTotalUser();
//    }
}
