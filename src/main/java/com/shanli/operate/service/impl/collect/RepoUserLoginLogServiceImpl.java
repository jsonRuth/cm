package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoUserLoginLogDao;
import com.shanli.operate.domain.collect.RepoUserLoginLogDO;
import com.shanli.operate.service.collect.RepoUserLoginLogService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepoUserLoginLogServiceImpl extends CoreServiceImpl<RepoUserLoginLogDao,RepoUserLoginLogDO> implements RepoUserLoginLogService {

    /**
     * 批量添加用户登录记录报表
     * @param userLoginLogDOList
     * @return
     */
    @Override
    public Boolean insertOrUpdateList(List<RepoUserLoginLogDO> userLoginLogDOList) {
        return this.baseMapper.insertOrUpdateList(userLoginLogDOList);
    }
}
