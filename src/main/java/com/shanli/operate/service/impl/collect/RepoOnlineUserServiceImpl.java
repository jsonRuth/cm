package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoOnlineUserDao;
import com.shanli.operate.domain.collect.RepoOnlineUserDO;
import com.shanli.operate.service.collect.RepoOnlineUserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class RepoOnlineUserServiceImpl extends CoreServiceImpl<RepoOnlineUserDao,RepoOnlineUserDO> implements RepoOnlineUserService {


    @Override
    public List<RepoOnlineUserDO> getOnlineUserStatis() {
        return this.baseMapper.getOnlineUserStatis();
    }
}
