package com.shanli.operate.service.impl.cm;

import com.shanli.common.base.CoreServiceImpl;

import com.shanli.operate.dao.cm.UserLockLogDao;

import com.shanli.operate.domain.cm.UserLockLogDO;
import com.shanli.operate.service.cm.UserLockLogService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class UserLockLogServiceImpl extends CoreServiceImpl<UserLockLogDao,UserLockLogDO> implements UserLockLogService {


    @Override
    public Integer getLoginFailLockCount() {
        return this.baseMapper.getLoginFailLockCount();
    }

    @Override
    public List<String> getLoginFailLockList() {
        return this.baseMapper.getLoginFailLockList();
    }
}
