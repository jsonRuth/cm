package com.shanli.operate.service.impl.cm;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.cm.FeedbackDao;
import com.shanli.operate.domain.cm.FeedbackDO;
import com.shanli.operate.service.cm.FeedbackService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service()
public class FeedbackServiceImpl extends CoreServiceImpl<FeedbackDao,FeedbackDO> implements FeedbackService {


    @Override
    public Integer getFeedbackCount() {
        return this.baseMapper.getFeedbackCount();
    }

    @Override
    public Integer updateReplyContent(String replyContent, int readStatus, Integer id, String replyAccount , Date recoveryTime) {
        return this.baseMapper.updateReplyContent(replyContent,readStatus,id,replyAccount ,recoveryTime);
    }

    @Override
    public List<String> getFeedbackTotalData() {
        return this.baseMapper.getFeedbackTotalData();
    }


}
