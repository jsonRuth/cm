package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoStatisGroupDao;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;
import com.shanli.operate.service.collect.RepoStatisGroupService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class RepoStatisGroupServiceImpl extends CoreServiceImpl<RepoStatisGroupDao,RepoStatisGroupDO> implements RepoStatisGroupService {


    @Override
    public List<RepoStatisGroupDO> getGroupYtdStatis() {
        return this.baseMapper.getGroupYtdStatis();
    }

    @Override
    public Integer addBatch(List<RepoStatisGroupDO> list){
        return this.baseMapper.addBatch(list);
    }

}
