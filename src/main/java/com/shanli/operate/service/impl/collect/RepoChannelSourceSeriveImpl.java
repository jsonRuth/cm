package com.shanli.operate.service.impl.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoChannelSourceDao;
import com.shanli.operate.domain.collect.RepoChannelSourceDO;
import com.shanli.operate.service.collect.RepoChannelSourceSerive;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepoChannelSourceSeriveImpl extends CoreServiceImpl<RepoChannelSourceDao,RepoChannelSourceDO> implements RepoChannelSourceSerive {

    /**
     * 批量添加渠道来源数据
     * @param channelSourceDOS
     * @return
     */
    @Override
    public Boolean insertOrUpdateList(List<RepoChannelSourceDO> channelSourceDOS) {
        return this.baseMapper.insertOrUpdateList(channelSourceDOS);
    }

    /**
     * 查询全部用户的渠道来源数据
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @Override
    public Page<RepoChannelSourceDO> selectByList(Integer pageNumber, Integer pageSize) {
        Page<RepoChannelSourceDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectByList(page));
        return page;
    }

    /**
     * 查询免费用户渠道来源
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @Override
    public Page<RepoChannelSourceDO> selectByListFree(Integer pageNumber, Integer pageSize) {
        Page<RepoChannelSourceDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectByListFree(page));
        return page;
    }

    /**
     * 查询会员用户渠道来源
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @Override
    public Page<RepoChannelSourceDO> selectBylistType(Integer pageNumber, Integer pageSize) {
        Page<RepoChannelSourceDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectBylistType(page));
        return page;
    }

    @Override
    public RepoChannelSourceDO getChannelSourceSratis() {
        return this.baseMapper.getChannelSourceSratis();
    }
}
