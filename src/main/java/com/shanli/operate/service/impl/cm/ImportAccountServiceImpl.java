package com.shanli.operate.service.impl.cm;

import com.shanli.operate.dao.cm.ImportAccountDao;
import com.shanli.operate.domain.cm.ImportAccountDO;
import com.shanli.operate.service.cm.ImportAccountService;
import com.shanli.common.base.CoreServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportAccountServiceImpl extends CoreServiceImpl<ImportAccountDao, ImportAccountDO> implements ImportAccountService {
    @Override
    public int realDeleteIAById(Integer id) {
        return this.baseMapper.realDeleteIAById(id);
    }

    @Override
    public List<ImportAccountDO> getImportAccountByNames(List<String> accountList) {
        return this.baseMapper.getImportAccountByNames(accountList);
    }

    @Override
    public Integer addBatch(List<ImportAccountDO> list) {
        return this.baseMapper.addBatch(list);
    }
}
