package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoFeedbackDao;
import com.shanli.operate.domain.collect.RepoFeedbackDO;
import com.shanli.operate.service.collect.RepoFeedbackService;
import org.springframework.stereotype.Service;

@Service()
public class RepoFeedbackServiceImpl extends CoreServiceImpl<RepoFeedbackDao,RepoFeedbackDO> implements RepoFeedbackService {

    @Override
    public Integer getLastTotalCount() {
        return this.baseMapper.getLastTotalCount();
    }
}
