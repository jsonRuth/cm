package com.shanli.operate.service.impl.echat;

import com.shanli.operate.domain.collect.RepoCommonLoginDO;
import com.shanli.operate.domain.collect.RepoUserChargeDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.domain.collect.RepoUserLoginLogDO;
import org.springframework.stereotype.Service;

import com.shanli.operate.dao.echat.AccountDao;
import com.shanli.operate.domain.echat.AccountDO;
import com.shanli.operate.service.echat.AccountService;
import com.shanli.common.base.CoreServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2019-06-17 17:54:43 | niush</small>
 */
@Service
public class AccountServiceImpl extends CoreServiceImpl<AccountDao, AccountDO> implements AccountService {

    /**
     * 通过id修改测试账号
     *
     * @param accountDO
     * @return
     */
    @Override
    public int updateAccountSelective(AccountDO accountDO) {
        return this.baseMapper.updateAccountSelective(accountDO);
    }

    @Override
    public Integer addBatch(List<AccountDO> list) {
        return this.baseMapper.addBatch(list);
    }

    @Override
    public Integer resetTestAccountPwd(List<Integer> ids, String password) {
        return this.baseMapper.resetTestAccountPwd(ids, password);
    }

    /**
     * 通过账号集合查询导入账号列表
     *
     * @param accountList
     * @return
     */
    @Override
    public List<AccountDO> getUserAccountByNames(List<String> accountList) {
        return this.baseMapper.getUserAccountByNames(accountList);
    }

    /**
     * 批量添加账号
     *
     * @param params
     * @return
     */
    @Override
    public Integer addAccountBatch(Map<String, Map<String, AccountDO>> params) {
        return this.baseMapper.addAccountBatch(params);
    }

    /**
     * 通过用户账号查询id
     *
     * @param userAccount
     * @return
     */
    @Override
    public Integer selectByUserAccount(String userAccount) {
        return this.baseMapper.selectByUserAccount(userAccount);
    }

    /**
     * 定时任务查询用户统计
     *
     * @param isActive
     * @return
     */
    @Override
    public List<RepoUserInfoDO> getUserInfo(Integer isActive) {
        return this.baseMapper.getUserInfo(isActive);
    }

    /**
     * 通过账号集合查询全部账号列表（包含已删除用户）
     *
     * @param accountList
     * @return
     */
    @Override
    public List<AccountDO> getUserAccountDeletedByNames(List<String> accountList) {
        return this.baseMapper.getUserAccountDeletedByNames(accountList);
    }

    /**
     * 将已存在的用户（正式用户、已删除用户）转为测试用户
     *
     * @param accountList
     * @return
     */
    @Override
    public Integer switchAccountTest(List<String> accountList, AccountDO accountDO) {
        return this.baseMapper.switchAccountTest(accountList, accountDO);
    }

    /**
     * 正式用户转测试用户时，保存初始的会员有效期
     *
     * @param accountList
     * @return
     */
    @Override
    public Integer saveAccountService(List<String> accountList) {
        return this.baseMapper.saveAccountService(accountList);
    }

    /**
     * 批量添加用户，如果用户存在则更新
     *
     * @param list
     * @return
     */
    @Override
    public Integer addBatchOnUpdate(List<AccountDO> list) {
        return this.baseMapper.addBatchOnUpdate(list);
    }

    /**
     * 查询用户统计报表
     *
     * @return
     */
    @Override
    public List<RepoUserInfoDO> getUserInfoList() {
        return this.baseMapper.getUserInfoList();
    }

    /**
     * 根据id禁用用户
     *
     * @param id
     * @param userBannedLength
     * @param userBannedTime
     * @return
     */
    @Override
    public Boolean updateByIdUser(Integer id, Integer userBannedLength, Date userBannedTime) {
        return this.baseMapper.updateByIdUser(id, userBannedLength, userBannedTime);
    }

    /**
     * 根据id恢复用户
     *
     * @param user_id
     * @return
     */
    @Override
    public Boolean updateByUserRecover(Integer user_id) {
        return this.baseMapper.updateByUserRecover(user_id);
    }


    @Override
    public int batchUpdateExpireTime(Map<String, Object> param) {
        return baseMapper.batchUpdateExpireTime(param);

    }

    @Override
    public int batchDeleteUserInfo(Map<String, Object> param) {
        return baseMapper.batchUpdateUserInfo(param);
    }

    @Override
    public int batchLockAccount(Map<String, Object> param) {
        param.put("type", 1);
        return baseMapper.batchUpdateAccountLock(param);
    }

    @Override
    public int batchUnlockAccount(Map<String, Object> param) {
        //0表示解禁
        param.put("type", 0);
        return baseMapper.batchUpdateAccountLock(param);
    }

    /**
     * 查询昨天用户登录记录
     * @return
     */
    @Override
    public List<RepoUserLoginLogDO> getUserLoginLogByYesterDay () {
        return this.baseMapper.getUserLoginLogByYesterDay();
    }

    /**
     * 查询昨天用户缴费记录
     * @return
     */
    @Override
    public List<RepoUserChargeDO> getUserChargeByYesterDay () {
        return this.baseMapper.getUserChargeByYesterDay();
    }

    /**
     * 根据ids 批量查询账号
     * @param userIdList
     * @return
     */
    @Override
    public List<String> selectByUserIds(String[]  userIdList) {
        return this.baseMapper.selectUserIds(userIdList);
    }

    /**
     * 查询登录对照表
     * @return
     */
    @Override
    public List<RepoCommonLoginDO> getCommonLoginIP() {
        return this.baseMapper.getCommonLoginIP();
    }

    @Override
    public void updateAccountForbidden(Map<String, Object> param) {
        baseMapper.updateAccountForbidden( param);
    }
}

