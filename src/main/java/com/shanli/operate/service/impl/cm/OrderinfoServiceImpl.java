package com.shanli.operate.service.impl.cm;


import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.cm.OrderinfoDao;
import com.shanli.operate.domain.cm.OrderinfoDO;
import com.shanli.operate.service.cm.OrderinfoService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderinfoServiceImpl extends CoreServiceImpl<OrderinfoDao,OrderinfoDO> implements OrderinfoService {
    @Override
    public List<OrderinfoDO> getOrderinfoByPhone(List<String> userPhone) {
        return this.baseMapper.getOrderinfoByPhone(userPhone);
    }
}
