package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoUserLockLogDao;
import com.shanli.operate.domain.collect.RepoUserLockLogDO;
import com.shanli.operate.service.collect.RepoUserLockLogService;
import org.springframework.stereotype.Service;

@Service
public class RepoUserLockLogServiceImpl extends CoreServiceImpl<RepoUserLockLogDao,RepoUserLockLogDO> implements RepoUserLockLogService {


}
