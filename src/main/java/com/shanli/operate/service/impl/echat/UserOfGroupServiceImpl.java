package com.shanli.operate.service.impl.echat;


import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.echat.UserOfGroupDao;
import com.shanli.operate.domain.echat.UserOfGroupDo;
import com.shanli.operate.service.echat.UserOfGroupService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserOfGroupServiceImpl extends CoreServiceImpl<UserOfGroupDao,UserOfGroupDo> implements UserOfGroupService {
    @Override
    public List<Integer> selectByCgid(Long cgid) {
        return this.baseMapper.selectByCgid(cgid);
    }

    @Override
    public List<Long> selectByUserId(Integer userId) {
        return this.baseMapper.selectByUserId(userId);
    }

    @Override
    public Integer selectGroupCount(Integer user_id) {
        return this.baseMapper.selectGroupCount(user_id);
    }

    @Override
    public Boolean updateByIsActive(Integer userId) {
        return this.baseMapper.updateByIsActive(userId);
    }

    @Override
    public Boolean updateByCgidIsActive(Long cgid) {
        return this.baseMapper.updateByCgidIsActive(cgid);
    }

}
