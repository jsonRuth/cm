package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoUserSourceDao;
import com.shanli.operate.domain.collect.RepoUserSourceDO;
import com.shanli.operate.service.collect.RepoUserSourceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class RepoUserSourceServiceImpl extends CoreServiceImpl<RepoUserSourceDao,RepoUserSourceDO> implements RepoUserSourceService {


    @Override
    public List<RepoUserSourceDO> getUserSourceStatis() {
        return this.baseMapper.getUserSourceStatis();
    }

    @Override
    public Integer addBatch(List<RepoUserSourceDO> list) {
        return this.baseMapper.addBatch(list);
    }
}
