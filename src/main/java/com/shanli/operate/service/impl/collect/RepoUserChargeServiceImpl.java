package com.shanli.operate.service.impl.collect;


import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoUserChargeDao;
import com.shanli.operate.domain.collect.RepoUserChargeDO;
import com.shanli.operate.service.collect.RepoUserChargeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepoUserChargeServiceImpl extends CoreServiceImpl<RepoUserChargeDao,RepoUserChargeDO> implements RepoUserChargeService {


    /**
     * 批量添加用户缴费记录
     * @param userChargeDOList
     * @return
     */
    @Override
    public Boolean insertList(List<RepoUserChargeDO> userChargeDOList) {
        return this.baseMapper.insertList(userChargeDOList);
    }
}
