package com.shanli.operate.service.impl.collect;



import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoActiveUserDao;
import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.service.collect.RepoActiveUserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepoActiveUserServiceImpl extends CoreServiceImpl<RepoActiveUserDao,RepoActiveUserDO> implements RepoActiveUserService {


    @Override
    public Boolean insertByUpdate(List<RepoActiveUserDO> repoActiveUserDOS) {
        return this.baseMapper.insertByUpdate(repoActiveUserDOS);
    }

    @Override
    public Page<RepoActiveUserDO> selectPageUser(Integer pageNumber, Integer pageSize, RepoActiveUserDO userActive) {
        Page<RepoActiveUserDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectPageUser(page,userActive));
        return page;
    }
}
