package com.shanli.operate.service.impl.echat;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.echat.ChatGroupDao;
import com.shanli.operate.domain.collect.RepoGroupInfoDO;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;
import com.shanli.operate.domain.echat.ChatGroupDO;
import com.shanli.operate.service.echat.ChatGroupService;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

@Service()
public class ChatGroupServiceImpl extends CoreServiceImpl<ChatGroupDao,ChatGroupDO> implements ChatGroupService {


    @Override
    public List<RepoStatisGroupDO> getGroupReachStatis() {
        return this.baseMapper.getGroupReachStatis();
    }

    @Override
    public List<RepoStatisGroupDO> getGroupActiveStatis() {
        return this.baseMapper.getGroupActiveStatis();
    }

    @Override
    public Integer selectCreatorCount(Integer creator) {
        return this.baseMapper.selectCreatorCount(creator);
    }

    @Override
    public List<RepoGroupInfoDO> getGroupInfo() {
        return this.baseMapper.getGroupInfo();
    }

    @Override
    public Boolean updateByChatGroupIsActive(Long id) {
        return this.baseMapper.updateByChatGroupIsActive(id);
    }

    @Override
    public Boolean updateByIdChatGroup(Long id, Integer cgBannedLength,Date cgBannedTime) {
        return this.baseMapper.updateByIdChatGroup(id,cgBannedLength,cgBannedTime);
    }

    @Override
    public Boolean updateByChatGroup(Long cg_id) {
        return this.baseMapper.updateByChatGroup(cg_id);
    }
}
