package com.shanli.operate.service.impl.cm;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.cm.CustomerInfoDao;
import com.shanli.operate.domain.cm.CustomerInfoDO;
import com.shanli.operate.domain.collect.RepoChannelSourceDO;
import com.shanli.operate.domain.collect.RepoStatisUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.domain.collect.RepoUserSourceDO;
import com.shanli.operate.service.cm.CustomerInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class CustomerInfoServiceImpl extends CoreServiceImpl<CustomerInfoDao,CustomerInfoDO> implements CustomerInfoService {


    @Override
    public RepoStatisUserDO getYtdReachTotalUser() {
        return this.baseMapper.getYtdReachTotalUser();
    }

    @Override
    public RepoStatisUserDO getYtdReachFreeUser() {
        return this.baseMapper.getYtdReachFreeUser();
    }

    @Override
    public RepoStatisUserDO getYtdReachMember() {
        return this.baseMapper.getYtdReachMember();
    }

    @Override
    public RepoStatisUserDO getYtdIncreaseTotalUser() {
        return this.baseMapper.getYtdIncreaseTotalUser();
    }

    @Override
    public RepoStatisUserDO getYtdIncreaseFreeUser() {
        return this.baseMapper.getYtdIncreaseFreeUser();
    }

    @Override
    public RepoStatisUserDO getYtdIncreaseMember() {
        return this.baseMapper.getYtdIncreaseMember();
    }

    @Override
    public List<RepoUserSourceDO> getUserSourceStatis() {
        return this.baseMapper.getUserSourceStatis();
    }

    @Override
    public CustomerInfoDO selectBySubType(String customername) {
        return this.baseMapper.selectBySubType(customername);
    }

    /**
     * 查询渠道来源数据表
     * @return
     */
    @Override
    public List<RepoChannelSourceDO> selectChannelSource() {
        return this.baseMapper.selectChannelSource();
    }

}
