package com.shanli.operate.service.impl.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoActiveGroupDetailsDao;
import com.shanli.operate.domain.collect.RepoActiveGroupDetailsDO;
import com.shanli.operate.service.collect.RepoActiveGroupDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepoActiveGroupDetailsServiceImpl extends CoreServiceImpl<RepoActiveGroupDetailsDao,RepoActiveGroupDetailsDO> implements RepoActiveGroupDetailsService {


    @Override
    public void insertByUpdate(List<RepoActiveGroupDetailsDO> listActiveGroup) {
       this.baseMapper.insertByUpdate(listActiveGroup);
    }

    @Override
    public Page<RepoActiveGroupDetailsDO> selectActivePageGroup(Integer pageNumber, Integer pageSize, RepoActiveGroupDetailsDO activeGroup) {
        Page<RepoActiveGroupDetailsDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectActivePageGroup(page, activeGroup));
        return page;
    }
}
