package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoActiveGroupDao;
import com.shanli.operate.domain.collect.RepoActiveGroupDO;
import com.shanli.operate.service.collect.RepoActiveGroupService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class RepoAcriveGroupServiceImpl extends CoreServiceImpl<RepoActiveGroupDao,RepoActiveGroupDO> implements RepoActiveGroupService {


    @Override
    public List<RepoActiveGroupDO> getActiveGroupStatis() {
        return this.baseMapper.getActiveGroupStatis();
    }
}
