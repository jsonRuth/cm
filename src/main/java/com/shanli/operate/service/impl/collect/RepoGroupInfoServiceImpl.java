package com.shanli.operate.service.impl.collect;

import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoGroupInfoDao;
import com.shanli.operate.domain.collect.RepoActiveGroupDetailsDO;
import com.shanli.operate.domain.collect.RepoGroupInfoDO;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;
import com.shanli.operate.service.collect.RepoGroupInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepoGroupInfoServiceImpl extends CoreServiceImpl<RepoGroupInfoDao,RepoGroupInfoDO> implements RepoGroupInfoService {
    @Autowired
    private  RepoGroupInfoDao repoGroupInfoDao;

    @Override
    public Boolean insertOrUpdateList(List<RepoGroupInfoDO> repoGroupInfoDOSList) {
        return this.baseMapper.insertOrUpdateList(repoGroupInfoDOSList);
    }

    @Override
    public Boolean updateByIsActive(Long id) {
        return this.baseMapper.updateByIsActive(id);
    }

    @Override
    public Page<RepoGroupInfoDO> selectPageGroup(Integer pageNumber , Integer pageSize ,RepoGroupInfoDO groupInfo) {
        Page<RepoGroupInfoDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectPageGroup(page, groupInfo));
        return page;
    }

    @Override
    public Page<RepoGroupInfoDO> selectPageByCgids(Integer pageNumber, Integer pageSize, List<Long> cgids, RepoGroupInfoDO groupInfo) {
        Page<RepoGroupInfoDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectPageByCgids(page,cgids, groupInfo));
        return page;
    }

    @Override
    public Page<RepoGroupInfoDO> selectArrivePageGroup(Integer pageNumber, Integer pageSize, RepoGroupInfoDO groupInfo) {
        Page<RepoGroupInfoDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectArrivePageGroup(page, groupInfo));
        return page;
    }

    @Override
    public Page<RepoGroupInfoDO> selectActivePageGroup(Integer pageNumber, Integer pageSize, RepoGroupInfoDO groupInfo) {
        Page<RepoGroupInfoDO> page = new Page<>(pageNumber,pageSize);
        page.setRecords(this.baseMapper.selectActivePageGroup(page, groupInfo));
        return page;
    }

    @Override
    public List<RepoStatisGroupDO> getGroupStatis() {
        return this.baseMapper.getGroupStatis();
    }

    @Override
    public List<RepoStatisGroupDO> getGroupStatisTwo() {
        return this.baseMapper.getGroupStatisTwo();
    }

    @Override
    public List<RepoActiveGroupDetailsDO> getActiveGroup() {
        return this.baseMapper.getActiveGroup();
    }

}
