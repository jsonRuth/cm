package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.RepoStatisUserDao;
import com.shanli.operate.domain.collect.RepoStatisUserDO;
import com.shanli.operate.service.collect.RepoStatisUserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class RepoStatisUserServiceImpl extends CoreServiceImpl<RepoStatisUserDao,RepoStatisUserDO> implements RepoStatisUserService {


    @Override
    public List<RepoStatisUserDO> getUserYtdStatis() {
        return this.baseMapper.getUserYtdStatis();
    }

    @Override
    public Integer addBatch(List<RepoStatisUserDO> list){
        return this.baseMapper.addBatch(list);
    }

}
