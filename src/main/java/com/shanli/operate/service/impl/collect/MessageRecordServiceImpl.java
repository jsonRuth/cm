package com.shanli.operate.service.impl.collect;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.operate.dao.collect.MessageRecordDao;
import com.shanli.operate.domain.collect.MessageRecordDO;
import com.shanli.operate.domain.collect.RepoStatisUserDO;
import com.shanli.operate.service.collect.MessageRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
public class MessageRecordServiceImpl extends CoreServiceImpl<MessageRecordDao,MessageRecordDO> implements MessageRecordService {


    @Override
    public Integer getCurActiveGroupCount() {
        return this.baseMapper.getCurActiveGroupCount();
    }

    @Override
    public RepoStatisUserDO getYtdActiveTotalUser() {
        return this.baseMapper.getYtdActiveTotalUser();
    }

    @Override
    public RepoStatisUserDO getYtdActiveFreeUser() {
        return this.baseMapper.getYtdActiveFreeUser();
    }

    @Override
    public RepoStatisUserDO getYtdActiveMember() {
        return this.baseMapper.getYtdActiveMember();
    }

}
