package com.shanli.operate.dao.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoStatisUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoUserInfoDao extends BaseDao<RepoUserInfoDO> {

    Boolean insertOrUpdateList(@Param("userInfoDOList") List<RepoUserInfoDO> userInfoDOList);

    List<RepoUserInfoDO> selectPageUser(Page<RepoUserInfoDO> page, @Param("userInfo") RepoUserInfoDO userInfo);

    List<RepoUserInfoDO> selectByCgidPageUser(Page<RepoUserInfoDO> page,  @Param("userId")List<Integer> userId,  @Param("userInfo")RepoUserInfoDO userInfo);

    List<RepoUserInfoDO> selectPageUserLoginFail(Page<RepoUserInfoDO> page, @Param("userInfo") RepoUserInfoDO userInfo,@Param("beginTime")String beginTime, @Param("endTime")String endTime);

    Integer getLastTotalCount();

    List<RepoActiveUserDO> getActiveUser();


//    List<RepoStatisUserDO> getTotalUser();
}
