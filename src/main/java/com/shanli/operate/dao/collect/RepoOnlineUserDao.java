package com.shanli.operate.dao.collect;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoOnlineUserDO;

import java.util.List;


public interface RepoOnlineUserDao extends BaseDao<RepoOnlineUserDO>{

    /**
     * 查询昨日用户在线数
     * @return
     */
    List<RepoOnlineUserDO> getOnlineUserStatis();
}