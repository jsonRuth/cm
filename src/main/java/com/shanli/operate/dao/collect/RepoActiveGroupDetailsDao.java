package com.shanli.operate.dao.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoActiveGroupDetailsDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RepoActiveGroupDetailsDao extends BaseDao<RepoActiveGroupDetailsDO> {


    void insertByUpdate(@Param("listActiveGroup") List<RepoActiveGroupDetailsDO> listActiveGroup);


    List<RepoActiveGroupDetailsDO> selectActivePageGroup(Pagination page,@Param("activeGroup") RepoActiveGroupDetailsDO activeGroup);
}
