package com.shanli.operate.dao.collect;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoFeedbackDO;


public interface RepoFeedbackDao extends BaseDao<RepoFeedbackDO>{

    /**
     * 查询意见反馈报表最后一条记录
     * @return
     */
    Integer getLastTotalCount();
}