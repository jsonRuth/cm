package com.shanli.operate.dao.collect;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoActiveUserDao extends BaseDao<RepoActiveUserDO> {

    /**
     * 批量添加到活跃用户表
     * @param repoActiveUserDOS
     */
    Boolean insertByUpdate(@Param("repoActiveUserDOS") List<RepoActiveUserDO> repoActiveUserDOS);

    /**
     * 查询活跃用户数据
     * @param page
     * @param userActive
     * @return
     */
    List<RepoActiveUserDO> selectPageUser(Pagination page, @Param("userActive") RepoActiveUserDO userActive);
}
