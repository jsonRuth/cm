package com.shanli.operate.dao.collect;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoUserChargeDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoUserChargeDao extends BaseDao<RepoUserChargeDO> {


    /**
     * 批量添加用户缴费记录
     * @param userChargeDOList
     * @return
     */
    Boolean insertList(@Param("userChargeDOList") List<RepoUserChargeDO> userChargeDOList);
}
