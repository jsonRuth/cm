package com.shanli.operate.dao.collect;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoUserLoginLogDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoUserLoginLogDao extends BaseDao<RepoUserLoginLogDO> {

    /**
     * 批量添加用户登录记录报表
     * @param userLoginLogDOList
     * @return
     */
    Boolean insertOrUpdateList(@Param("userLoginLogDOList") List<RepoUserLoginLogDO> userLoginLogDOList);
}
