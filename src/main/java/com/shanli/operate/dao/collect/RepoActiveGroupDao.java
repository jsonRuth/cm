package com.shanli.operate.dao.collect;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoActiveGroupDO;

import java.util.List;


public interface RepoActiveGroupDao extends BaseDao<RepoActiveGroupDO>{

    /**
     * 查询活跃群组
     * @return
     */
    List<RepoActiveGroupDO> getActiveGroupStatis();
}