package com.shanli.operate.dao.collect;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoActiveGroupDetailsDO;
import com.shanli.operate.domain.collect.RepoGroupInfoDO;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoGroupInfoDao extends BaseDao<RepoGroupInfoDO> {

    Boolean insertOrUpdateList(@Param("repoGroupInfoDOSList")List<RepoGroupInfoDO> repoGroupInfoDOSList);

    Boolean updateByIsActive(Long id);

    List<RepoGroupInfoDO> selectPageGroup(Pagination page, @Param("groupInfo") RepoGroupInfoDO groupInfo);

    List<RepoGroupInfoDO> selectPageByCgids(Pagination page,@Param("cgids") List<Long> cgids, @Param("groupInfo")RepoGroupInfoDO groupInfo);


    /**
     * 查询到达值
     * @param page
     * @param groupInfo
     * @return
     */
    List<RepoGroupInfoDO> selectArrivePageGroup(Pagination page, @Param("groupInfo") RepoGroupInfoDO groupInfo);

    /**
     * 查询群组活跃值
     * @param page
     * @param groupInfo
     * @return
     */
    List<RepoGroupInfoDO> selectActivePageGroup(Pagination page, @Param("groupInfo") RepoGroupInfoDO groupInfo);

    /**
     * 查询群组的到达值
     * @return
     */
    List<RepoStatisGroupDO> getGroupStatis();

    /**
     * 查询群组的活跃值
     * @return
     */
    List<RepoStatisGroupDO> getGroupStatisTwo();

    /**
     * 查询群组活跃详情数据
     * @return
     */
    List<RepoActiveGroupDetailsDO> getActiveGroup();

}
