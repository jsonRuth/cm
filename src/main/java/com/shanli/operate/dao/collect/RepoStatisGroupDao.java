package com.shanli.operate.dao.collect;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;

import java.util.List;


public interface RepoStatisGroupDao extends BaseDao<RepoStatisGroupDO>{

    /**
     * 群组昨日关键数据统计
     * @return
     */
    List<RepoStatisGroupDO> getGroupYtdStatis();

    /**
     * 批量添加
     * @param list
     * @return
     */
    Integer addBatch(List<RepoStatisGroupDO> list);
}