package com.shanli.operate.dao.collect;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.MessageRecordDO;
import com.shanli.operate.domain.collect.RepoStatisUserDO;

import java.util.List;


public interface MessageRecordDao extends BaseDao<MessageRecordDO>{

    /**
     * 查询最近一小时活跃群组数
     * @return
     */
    Integer getCurActiveGroupCount();

    /**
     * 查询昨日活跃总用户数
     * @return
     */
    RepoStatisUserDO getYtdActiveTotalUser();

    /**
     * 查询昨日活跃免费用户数
     * @return
     */
    RepoStatisUserDO getYtdActiveFreeUser();

    /**
     * 查询昨日活跃会员数
     * @return
     */
    RepoStatisUserDO getYtdActiveMember();
}