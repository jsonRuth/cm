package com.shanli.operate.dao.collect;

import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoActiveGroupDO;
import com.shanli.operate.domain.collect.RepoChannelSourceDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RepoChannelSourceDao extends BaseDao<RepoChannelSourceDO> {


    /**
     * 批量添加渠道来源数据
     * @param channelSourceDOS
     * @return
     */
    Boolean insertOrUpdateList(@Param("channelSourceDOS") List<RepoChannelSourceDO> channelSourceDOS);


    /**
     * 查询全部用户的渠道来源数据
     * @param page
     * @return
     */
    List<RepoChannelSourceDO> selectByList(Page<RepoChannelSourceDO> page);

    /**
     * 查询免费用户渠道来源
     * @param page
     * @return
     */
    List<RepoChannelSourceDO> selectByListFree(Page<RepoChannelSourceDO> page);

    /**
     * 查询会员用户渠道来源
     * @param page
     * @return
     */
    List<RepoChannelSourceDO> selectBylistType(Page<RepoChannelSourceDO> page);

    /**
     * 查询总数据渠道来源总数
     * @return
     */
    RepoChannelSourceDO getChannelSourceSratis();

}
