package com.shanli.operate.dao.echat;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.collect.RepoGroupInfoDO;
import com.shanli.operate.domain.collect.RepoStatisGroupDO;
import com.shanli.operate.domain.echat.ChatGroupDO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;


public interface ChatGroupDao extends BaseDao<ChatGroupDO>{

    /**
     * 查询群组到达值统计
     * @return
     */
    List<RepoStatisGroupDO> getGroupReachStatis();

    /**
     * 查询群组活跃值统计
     * @return
     */
    List<RepoStatisGroupDO> getGroupActiveStatis();

    /**
     * 根据用户id查询创建群组数
     * @param creator
     * @return
     */
    Integer selectCreatorCount(Integer creator);

    /**
     * 定时任务查询群组统计
     * @return
     */
    List<RepoGroupInfoDO> getGroupInfo();

    /**
     * 根据id删除tb_chatgroup(逻辑删除)
     * @param id
     * @return
     */
    Boolean updateByChatGroupIsActive(Long id);

    Boolean updateByIdChatGroup(@Param("id") Long id, @Param("cgBannedLength") Integer cgBannedLength,@Param("cgBannedTime")Date cgBannedTime);

    Boolean updateByChatGroup(@Param("cg_id")Long cg_id);
}