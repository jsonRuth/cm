package com.shanli.operate.dao.echat;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.echat.UserOfGroupDo;

import java.util.List;

public interface UserOfGroupDao extends BaseDao<UserOfGroupDo>{
    List<Integer> selectByCgid(Long cgid);

    List<Long> selectByUserId(Integer userId);

    Integer selectGroupCount(Integer user_id);

    Boolean updateByIsActive(Integer userId);

    Boolean updateByCgidIsActive(Long cgid);
}
