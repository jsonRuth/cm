package com.shanli.operate.dao.cm;

import com.shanli.operate.domain.cm.ImportAccountDO;
import com.shanli.common.base.BaseDao;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ImportAccountDao  extends BaseDao<ImportAccountDO> {

    /**
     * 删除导入账号（物理删除）
     * @param id
     * @return
     */
    int realDeleteIAById(Integer id);

    /**
     * 通过账号集合查询导入账号列表
     * @param accountList
     * @return
     */
    List<ImportAccountDO> getImportAccountByNames(@Param("accountList") List<String> accountList);

    /**
     * 批量添加
     * @param list
     * @return
     */
    Integer addBatch(List<ImportAccountDO> list);
}
