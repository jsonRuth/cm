package com.shanli.operate.dao.cm;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.cm.CustomerInfoDO;
import com.shanli.operate.domain.collect.*;
import com.shanli.operate.domain.echat.ChatGroupDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface CustomerInfoDao extends BaseDao<CustomerInfoDO>{

    /**
     * 查询昨日到达总用户数
     * @return
     */
    RepoStatisUserDO getYtdReachTotalUser();

    /**
     * 查询昨日到达免费用户
     * @return
     */
    RepoStatisUserDO getYtdReachFreeUser();

    /**
     * 查询昨日到达会员数
     * @return
     */
    RepoStatisUserDO getYtdReachMember();

    /**
     * 查询昨日新增总用户数
     * @return
     */
    RepoStatisUserDO getYtdIncreaseTotalUser();

    /**
     * 查询昨日新增免费用户数
     * @return
     */
    RepoStatisUserDO getYtdIncreaseFreeUser();

    /**
     * 查询昨日新增会员数
     * @return
     */
    RepoStatisUserDO getYtdIncreaseMember();

    /**
     * 查询用户渠道来源统计
     * @return
     */
    List<RepoUserSourceDO> getUserSourceStatis();

    /**
     * 根据登录名查询渠道来源
     * @param customername
     * @return
     */
    CustomerInfoDO selectBySubType(String customername);

    /**
     * 查询渠道来源数据表
     * @return
     */
    List<RepoChannelSourceDO> selectChannelSource();

}