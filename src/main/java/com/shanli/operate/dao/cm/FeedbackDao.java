package com.shanli.operate.dao.cm;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.cm.FeedbackDO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;


public interface FeedbackDao extends BaseDao<FeedbackDO>{

    /**
     * 查询意见反馈数量
     * @return
     */
    Integer getFeedbackCount();

    /**
     * 保存回复数据
     * @param replyContent
     * @param readStatus
     * @param id
     */
    Integer updateReplyContent(@Param("replyContent")String replyContent, @Param("readStatus")int readStatus,@Param("id")Integer id
            , @Param("replyAccount")String replyAccount ,@Param("recoveryTime") Date recoveryTime);

    /**
     * 查询意见反馈数据
     * @return
     */
    List<String> getFeedbackTotalData();

}