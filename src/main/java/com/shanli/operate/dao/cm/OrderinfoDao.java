package com.shanli.operate.dao.cm;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.shanli.operate.domain.cm.OrderinfoDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderinfoDao extends BaseMapper<OrderinfoDO> {
    /**
     * 通过手机集合查询客户列表
     * @param Phone
     * @return
     */
    List<OrderinfoDO> getOrderinfoByPhone(@Param("Phone") List<String> Phone);
}

