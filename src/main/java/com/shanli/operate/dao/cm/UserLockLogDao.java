package com.shanli.operate.dao.cm;


import com.shanli.common.base.BaseDao;
import com.shanli.operate.domain.cm.UserLockLogDO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserLockLogDao extends BaseDao<UserLockLogDO>{

    /**
     * 查询用户登录失败锁定数量
     * @return
     */
    Integer getLoginFailLockCount();

    /**
     * 查询锁定用户数
     * @return
     */
    List<String> getLoginFailLockList();

}