package com.shanli.operate.controller;

import com.shanli.operate.domain.cm.ImportAccountDO;
import com.shanli.operate.domain.echat.AccountDO;
import com.shanli.operate.service.cm.ImportAccountService;
import com.shanli.operate.service.echat.AccountService;
import com.shanli.operate.utils.DataUtil;
import com.shanli.operate.utils.ExcelUtil;
import com.shanli.common.annotation.Log;
import com.shanli.common.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.AdminBaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/operate/importAccount")
public class ImportAccountController extends AdminBaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ImportAccountService importAccountService;
    @Autowired
    private AccountService userService;

    @GetMapping()
    @RequiresPermissions("operate:importAccount:importAccount")
    String Importaccount(){
        return "operate/importAccount/importAccount";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("operate:importAccount:importAccount")
    public Result<Page<ImportAccountDO>> list(ImportAccountDO importAccountDTO,String beginTime,String endTime){
        Wrapper<ImportAccountDO> wrapper = new EntityWrapper<ImportAccountDO>().orderBy("id", false);
        if (!StringUtils.isEmpty(importAccountDTO.getAccount())) {
            wrapper = wrapper.like("account", importAccountDTO.getAccount());
        }
        if(null != importAccountDTO.getAccountStatus() &&
                (importAccountDTO.getAccountStatus() == 0 || importAccountDTO.getAccountStatus() == 1)){
            wrapper = wrapper.eq("account_status",importAccountDTO.getAccountStatus());
        }
        if (!StringUtils.isEmpty(beginTime)) {
            wrapper = wrapper.ge("create_time", beginTime);
        }
        if (!StringUtils.isEmpty(endTime)) {
            wrapper = wrapper.le("create_time", endTime + " 23:59:59");
        }
        Page<ImportAccountDO> page = importAccountService.selectPage(getPage(ImportAccountDO.class), wrapper);
        return Result.ok(page);
    }

    @Log("删除网上商城账号")
    @PostMapping( "/remove")
    @ResponseBody
    @RequiresPermissions("operate:importAccount:remove")
    public Result<String>  remove( Integer id){
        Map<String,Object> map = new HashMap<>(2);
        map.put("id",id);
        map.put("account_status",1);
        List<ImportAccountDO> list = importAccountService.selectByMap(map);
        if(list == null || list.size() == 0){
            int num = importAccountService.realDeleteIAById(id);
            if(num > 0){
                return Result.ok();
            }
            return Result.fail();
        }else{
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_DELETE_ERROR);
        }

    }

    /**
     * 批量导入页面
     * @return
     */
    @RequiresPermissions("operate:importAccount:batchImport")
    @GetMapping("/batchImport")
    String batchImport(){
        return "operate/importAccount/batchImport";
    }

    @Log("批量导入账号")
    @RequiresPermissions("operate:importAccount:batchImport")
    @PostMapping("/saveImport")
    @ResponseBody
    public Result<String> saveImport(MultipartFile file){
        List<Map<String, Object>> list;
        try {
            list = ExcelUtil.readExcelByFileForXlsx(file.getInputStream());
        } catch (Exception ex) {
            ex.printStackTrace();
            return Result.build(Result.EnumStatusCode.EXCEL_ERROR);
        }
        //读取不到数据
        if(list.size()==0){
            return Result.build(Result.EnumStatusCode.FORMAT_IS_WRONG);
        }
        Map<String,Object> mapTitle = list.get(0);
        if(!mapTitle.containsKey("账号") || !mapTitle.containsKey("账号来源") || !mapTitle.containsKey("功能费类型") ||!mapTitle.containsKey("购买数量")){
            return Result.build(Result.EnumStatusCode.FORMAT_IS_WRONG);
        }
        //导入数量限制1000
        if(list.size() > 1000){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_OUT_RANGE);
        }

        List<String> accountList = new ArrayList<>();
        List<ImportAccountDO> addList = new ArrayList<>(list.size());

        //将导入的account放入集合
        Iterator<Map<String,Object>> it = list.iterator();
        while(it.hasNext()){
            Map<String, Object> item = it.next();
            if(DataUtil.ObjectIsNull(item.get("账号"))
                    && DataUtil.ObjectIsNull(item.get("账号来源"))
                    && DataUtil.ObjectIsNull(item.get("功能费类型"))
                    && DataUtil.ObjectIsNull(item.get("购买数量"))){
                it.remove();
            }else {
                accountList.add(item.get("账号").toString());
            }
        }

        //判断导入的中号中是否有重复账号
        Set<String> set = new HashSet<String>(accountList);
        if(set.size() != accountList.size()){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_DUPLICATE);
        }

        //查询导入的账号是否已存在
        List<ImportAccountDO> duplicateList = importAccountService.getImportAccountByNames(accountList);
        if(duplicateList != null && duplicateList.size() > 0){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
        }

        //判断录入的账号中是否注册过
        List<AccountDO> checkUserList = userService.getUserAccountByNames(accountList);
        if(checkUserList != null && checkUserList.size() > 0){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
        }

        //循环导入的数据，放入集合
        for(Map<String,Object> map:list){
            ImportAccountDO importAccountDO = new ImportAccountDO();
            //账号不为空判断
            if(map.get("账号") == null || map.get("账号") ==""){
                return Result.build(Result.EnumStatusCode.FORMAT_IS_WRONG);
            }
            //账号是否首位为1，长度是否为13位号码
            String account = map.get("账号").toString();
            if(!account.substring(0,1).equals("1") || account.length() != 13 || !account.matches("^[0-9]*$")){
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_INVALID);
            }
            importAccountDO.setAccount(map.get("账号").toString());
            if(map.get("账号来源") != null&& map.get("账号来源").toString().equals("网上商城")){
                importAccountDO.setAccountSource(1);
            }else {
                return Result.build(Result.EnumStatusCode.ACCOUNT_SOURCE_ERROR);
            }
            if(map.get("功能费类型") != null && map.get("功能费类型").toString().equals("年卡会员-网上商城版")){
                importAccountDO.setFeeType(1);
            }else {
                return Result.build(Result.EnumStatusCode.FUNCTION_FEE_TYPE_ERROR);
            }
            Pattern pattern = Pattern.compile("[0-9]*");
            String buyNumber = String.valueOf(map.get("购买数量"));
            if(map.get("购买数量") != null && pattern.matcher(buyNumber).matches() && Integer.parseInt(map.get("购买数量").toString())<=100 && Integer.parseInt(map.get("购买数量").toString())>=1){
                importAccountDO.setBuyCount(Integer.parseInt(map.get("购买数量").toString()));
            }else {
                return Result.build(Result.EnumStatusCode.PURCHASE_QUANTITY_ERROR);
            }
            importAccountDO.setAccountStatus(0);
            addList.add(importAccountDO);
        }

        //添加
        if(addList.size() > 0){
            try {
                importAccountService.addBatch(addList);
            } catch (DuplicateKeyException ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
            } catch (Exception ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_IMPORT_ERROR);
            }
        }
        return Result.ok();
    }

    /**
     * 下载excel模板文件
     * @param fileName
     * @param response
     */
    @RequestMapping("downloadTemplate")
    @ResponseBody
    public void downloadTemplate(String fileName, HttpServletResponse response){
        String filePath = "templates"+ File.separatorChar+"operate" + File.separatorChar + "excelTemplate" +
                File.separatorChar + fileName;
        logger.info(filePath);
        //从jar包中读取文件，需要用getResourceAsStream
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(filePath);


        if (fileName.equals("MemberImportTemplate.xlsx")){
            ExcelUtil.downloadExcel(response,"对讲账号导入模板.xlsx" ,in);
        }else {
            ExcelUtil.downloadExcel(response,"测试账号导入模板.xlsx" ,in);
        }

    }

    /**
     * 手动录入页面
     * @return
     */
    @RequiresPermissions("operate:importAccount:handInput")
    @GetMapping("/handInput")
    String handInput(){
        return "operate/importAccount/handInput";
    }

    @Log("手动录入账号")
    @RequiresPermissions("operate:importAccount:handInput")
    @PostMapping("/saveAccount")
    @ResponseBody
    public Result<String> saveAccount(String accounts,ImportAccountDO importAccountDTO){
        if(StringUtils.isEmpty(accounts)){
            return Result.build(Result.EnumStatusCode.ACCOUNT_IS_EMPTY);
        }
        String[] account = accounts.split("\r\n");
        if(account.length > 10){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_OUT_RANGE);
        }

        //账号数组转为list
        List<String> accountList = Arrays.asList(account);
        for (String emailAddr : accountList) {
            if (emailAddr == null || emailAddr.length()==0 || emailAddr=="" || emailAddr.trim().isEmpty()){
                return Result.build(Result.EnumStatusCode.WRONG_ACCOUNT_FORMAT);
            }
        }
        //判断录入的中号中是否有重复账号
        Set<String> set = new HashSet<String>(accountList);
        if(set.size() != accountList.size()){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_DUPLICATE);
        }

        //查询录入的账号是否已存在
        List<ImportAccountDO> duplicateList = importAccountService.getImportAccountByNames(accountList);
        if(duplicateList != null && duplicateList.size() > 0){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
        }

        //判断录入的账号中是否注册过
        List<AccountDO> checkUserList = userService.getUserAccountByNames(accountList);
        if(checkUserList != null && checkUserList.size() > 0){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
        }

        List<ImportAccountDO> addList = new ArrayList<>(accountList.size());
        //循环导入的数据，放入集合
        for(String accountTmp:accountList){
            ImportAccountDO importAccountDO = new ImportAccountDO();
            //账号不为空判断
            if(StringUtils.isEmpty(accountTmp)){
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_INVALID);
            }
            //账号是否首位为1，长度是否为13位号码
            if(!accountTmp.substring(0,1).equals("1") || accountTmp.length() != 13 || !accountTmp.matches("^[0-9]*$")){
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_INVALID);
            }
            importAccountDO.setAccount(accountTmp);
            importAccountDO.setAccountSource(importAccountDTO.getAccountSource());
            importAccountDO.setFeeType(importAccountDTO.getFeeType());
            importAccountDO.setBuyCount(importAccountDTO.getBuyCount());
            importAccountDO.setAccountStatus(0);
            addList.add(importAccountDO);
        }

        //添加
        if(addList.size() > 0){
            try {
                importAccountService.addBatch(addList);
            } catch (DuplicateKeyException ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
            } catch (Exception ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_IMPORT_ERROR);
            }
        }
        return Result.ok();
    }
}
