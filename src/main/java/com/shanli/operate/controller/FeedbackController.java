package com.shanli.operate.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.annotation.Log;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.utils.Result;
import com.shanli.operate.domain.cm.FeedbackDO;
import com.shanli.operate.service.cm.FeedbackService;
import com.shanli.operate.utils.ExcelUtil;
import com.shanli.sys.domain.UserDO;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/operate/feedAccount")
public class FeedbackController extends AdminBaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    FeedbackService feedbackService;

    @GetMapping()
    public String feedAccount() {
        return "operate/feedAccount/feedAccount";
    }

    @ResponseBody
    @GetMapping("/list")
    public Result<Page<FeedbackDO>> list(FeedbackDO feedbackDO ,String beginTime, String endTime){
        List<String> feedbackAccount = feedbackService.getFeedbackTotalData();
        Wrapper<FeedbackDO> wrapper = new EntityWrapper<FeedbackDO>().orderBy("create_time", false);
        wrapper.in("user_account", feedbackAccount);
            if (!StringUtils.isEmpty(beginTime)) {
            wrapper.ge("create_time", beginTime);
        }
        if (!StringUtils.isEmpty(endTime)) {
            wrapper.le("create_time", endTime + " 23:59:59");
        }
        //账号
        if(!StringUtils.isEmpty(feedbackDO.getUserAccount())){
            wrapper.like("User_Account",feedbackDO.getUserAccount());
        }
        //状态
        if(feedbackDO.getReadStatus()== 0 || feedbackDO.getReadStatus() == 1
            || feedbackDO.getReadStatus() == 2 || feedbackDO.getReadStatus() == 3){
            wrapper = wrapper.eq("read_status",feedbackDO.getReadStatus());
        }

        //软件版本
        if(!StringUtils.isEmpty(feedbackDO.getAppVersion())){
            wrapper.like("app_version",feedbackDO.getAppVersion());
        }
        //设备ID
        if(!StringUtils.isEmpty(feedbackDO.getDeviveId())){
            wrapper.like("device_id",feedbackDO.getDeviveId());
        }
        //设备品牌
        if(!StringUtils.isEmpty(feedbackDO.getManufacturer())){
            wrapper.like("manufacturer",feedbackDO.getManufacturer());
        }
        //设备品牌型号
        if(!StringUtils.isEmpty(feedbackDO.getModel())){
            wrapper.like("model",feedbackDO.getModel());
        }
        //设备系统版本
        if(!StringUtils.isEmpty(feedbackDO.getOsVersion())){
            wrapper.like("os_version",feedbackDO.getOsVersion());
        }

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String format = sf.format(date);
        wrapper.le("create_time", format);
        Page<FeedbackDO> page = feedbackService.selectPage(getPage(FeedbackDO.class),wrapper);
        return Result.ok(page);
    }

    @GetMapping("/edit/{id}")
    String edit(@PathVariable("id") Integer id, Model model) {
        FeedbackDO feedback = feedbackService.selectById(id);
        model.addAttribute("feedbackDO",feedback);
        if (feedback.getReadStatus()==0){
            return "operate/feedAccount/edit";
        }
        return "operate/feedAccount/editLock";

    }

    @Log("添加回复信息")
    @ResponseBody
    @PostMapping("/updateReplyContent")
    public Result<String> updateReplyContent(FeedbackDO feedbackDO){
        UserDO user = getUser();
        try {
            feedbackDO.setReplyAccount(user.getUsername());
            feedbackDO.setRecoveryTime(new Date());
            feedbackService.updateReplyContent(feedbackDO.getReplyContent(),feedbackDO.getReadStatus(),feedbackDO.getId(),feedbackDO.getReplyAccount(),feedbackDO.getRecoveryTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok();
    }

    @Log("导出勾选意见反馈")
    @GetMapping("/exportSelected")
    @ResponseBody
    public Result<String> exportSelected(HttpServletResponse response, Integer[] ids) {

        Wrapper<FeedbackDO> wrapper = new EntityWrapper<FeedbackDO>().orderBy("id", false).in("id", ids);

        List<Map<String, Object>> list = feedbackService.selectMaps(wrapper);

        String fileName = "意见反馈(勾选)" + System.currentTimeMillis() + ".xlsx";
        exportCompany(fileName, list, response);

        return Result.ok();
    }

    @Log("导出所有意见反馈")
    @GetMapping("/exportAll")
    @ResponseBody
    public Result<String> exportAll(HttpServletResponse response, FeedbackDO feedbackDO, String beginTime, String endTime) {
        Wrapper<FeedbackDO> wrapper = new EntityWrapper<FeedbackDO>().orderBy("id", false);
        if (!StringUtils.isEmpty(beginTime)) {
            wrapper.ge("create_time", beginTime);
        }
        if (!StringUtils.isEmpty(endTime)) {
            wrapper.le("create_time", endTime + " 23:59:59");
        }
        //账号
        if(!StringUtils.isEmpty(feedbackDO.getUserAccount())){
            wrapper.like("User_Account",feedbackDO.getUserAccount());
        }
        //状态
        if(feedbackDO.getReadStatus()== 0 || feedbackDO.getReadStatus() == 1){
            wrapper = wrapper.eq("read_status",feedbackDO.getReadStatus());
        }
        //软件版本
        if(!StringUtils.isEmpty(feedbackDO.getAppVersion())){
            wrapper.like("app_version",feedbackDO.getAppVersion());
        }
        //设备ID
        if(!StringUtils.isEmpty(feedbackDO.getDeviveId())){
            wrapper.like("device_id",feedbackDO.getDeviveId());
        }
        //设备品牌
        if(!StringUtils.isEmpty(feedbackDO.getManufacturer())){
            wrapper.like("manufacturer",feedbackDO.getManufacturer());
        }
        //设备品牌型号
        if(!StringUtils.isEmpty(feedbackDO.getModel())){
            wrapper.like("model",feedbackDO.getModel());
        }
        //设备系统版本
        if(!StringUtils.isEmpty(feedbackDO.getOsVersion())){
            wrapper.like("os_version",feedbackDO.getOsVersion());
        }

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String format = sf.format(date);
        wrapper.le("create_time", format);

        List<Map<String, Object>> list = feedbackService.selectMaps(wrapper);
        String fileName = "意见反馈(全部)" + System.currentTimeMillis() + ".xlsx";
        exportCompany(fileName, list, response);
        return Result.ok();
    }

    private void exportCompany(String fileName, List<Map<String, Object>> list, HttpServletResponse response) {
        String[] title = {"用户账号", "反馈内容", "反馈时间", "设备ID","设备品牌","设备品牌型号","设备系统","设备系统版本","软件版本"};
        String[] columns = {"userAccount", "content", "createTime", "deviveId", "manufacturer", "model", "os", "osVersion", "appVersion"};
        ExcelUtil.exportExcel(list, response, title, columns, "sheet1", fileName);
    }

}
