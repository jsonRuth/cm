package com.shanli.operate.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.utils.Result;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.service.collect.RepoUserInfoService;
import com.shanli.sys.domain.UserDO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/operate/loginFailAccount")
public class LoginFailController  extends AdminBaseController {
    @Autowired
    private RepoUserInfoService userInfoService;
    @Resource(name = "firstRedisTemplate")
    StringRedisTemplate stringRedisTemplate;

    @GetMapping()
    String loginFail(Model model){
        Date date = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String beginTime = sf.format(date);
        String endTime = null;
        try {
            Date date1 = sf.parse(beginTime);
            //昨天的日期
            endTime = sf.format((new Date(date1.getTime() - 1 * 24 * 60 * 60 * 1000)));

            //7天前的日期
            beginTime= sf.format((new Date(date1.getTime() - 7 * 24 * 60 * 60 * 1000)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        model.addAttribute("beginTime", beginTime);
        model.addAttribute("endTime", endTime);

        return "operate/loginFailAccount/loginFailAccount";
    }

    @ResponseBody
    @GetMapping("/list")
    public Result<Page<RepoUserInfoDO>> list(Integer pageNumber , Integer pageSize , RepoUserInfoDO userInfo, String beginTime, String endTime) {
        Page<RepoUserInfoDO> page = userInfoService.selectPageUserLoginFail(pageNumber,pageSize,userInfo,beginTime,endTime);
        return Result.ok(page);
    }


    @GetMapping("/loginFailColumn")
    @ResponseBody
    public Result<String> saveGroupColumn(@RequestParam String json){
        UserDO user = getUser();
        stringRedisTemplate.opsForValue().set(user.getUsername()+":loginFailColumn",json);
        return Result.ok();
    }

    @ResponseBody
    @GetMapping("/getloginFailColumn")
    public Result<String> getGroupColumnInfo(){
        UserDO user = getUser();
        String json = stringRedisTemplate.opsForValue().get(user.getUsername()+":loginFailColumn");
        return Result.ok(json);
    }

}
