package com.shanli.operate.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.utils.Result;
import com.shanli.operate.domain.collect.RepoChannelSourceDO;
import com.shanli.operate.service.collect.RepoChannelSourceSerive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/operate/channelSource")
public class ChannelSourceController extends AdminBaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private RepoChannelSourceSerive channelSourceSerive;

    @GetMapping()
    public String channelSource(){
        return "operate/channelSource/channelSource";

    }

    /**
     * 查询总用户渠道来源
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @GetMapping("/list")
    @ResponseBody
    public Result<Page<RepoChannelSourceDO>> list(Integer pageNumber , Integer pageSize){
        Page<RepoChannelSourceDO> page = channelSourceSerive.selectByList(pageNumber,pageSize);
        return Result.ok(page);
    }

    /**
     * 查询免费用户渠道来源
     * @return
     */
    @GetMapping("/listFree")
    @ResponseBody
    public Result<Page<RepoChannelSourceDO>> listFree(Integer pageNumber , Integer pageSize){
        Page<RepoChannelSourceDO> page = channelSourceSerive.selectByListFree(pageNumber,pageSize);
        return Result.ok(page);
    }

    /**
     * 查询会员用户渠道来源
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @GetMapping("/listType")
    @ResponseBody
    public Result<Page<RepoChannelSourceDO>> listType(Integer pageNumber , Integer pageSize){
        Page<RepoChannelSourceDO> page = channelSourceSerive.selectBylistType(pageNumber,pageSize);
        return Result.ok(page);
    }

}
