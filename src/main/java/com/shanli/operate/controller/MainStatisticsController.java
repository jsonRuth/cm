package com.shanli.operate.controller;


import com.shanli.common.base.AdminBaseController;

import com.shanli.operate.domain.collect.*;
import com.shanli.operate.service.collect.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 首页统计图
 */
@RequestMapping("/operate/main")
@Controller
public class MainStatisticsController extends AdminBaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RepoOnlineUserService repoOnlineUserService;
    @Autowired
    private RepoActiveGroupService repoActiveGroupService;
    @Autowired
    private RepoStatisGroupService repoGroupStatisService;
    @Autowired
    private RepoStatisUserService repoStatisUserService;
    @Autowired
    private RepoUserSourceService repoUserSourceService;
    @Autowired
    private RepoChannelSourceSerive repoChannelSourceSerive;
    @Autowired
    private RepoGroupInfoService repoGroupInfoService;
    @Autowired
    private RepoUserInfoService RepoUserInfoService;
    /**
     * 实时在线用户数
     * @return
     */
    @PostMapping("/onlineUser")
    @ResponseBody
    public Map<String,Object> onlineUser() {
        List<RepoOnlineUserDO> list = repoOnlineUserService.getOnlineUserStatis();
        if(list.size() != 24){
            logger.info("查询在线用户数据异常============"+list);
        }
        Map<String,Object> map = new HashMap<>(2);
        List<String> xData = new LinkedList<String>();
        List<String> yData = new LinkedList<String>();
        list.forEach(item -> {
            if(item.getPointTime() != null){
                xData.add(item.getPointTime()+"：00");
                yData.add((item.getTotalCount() == null) ? "0":item.getTotalCount()+"");
            }
        });
        map.put("xData",xData);
        map.put("yData",yData);
        return map;
    }

    /**
     * 实时活跃群组数
     * @return
     */
    @PostMapping("/activeGroup")
    @ResponseBody
    public Map<String,Object> activeGroup() {
        List<RepoActiveGroupDO> list = repoActiveGroupService.getActiveGroupStatis();
        if(list.size() != 24){
            logger.info("查询活跃群组数据异常============"+list);
        }
        Map<String,Object> map = new HashMap<>(2);
        List<String> xData = new ArrayList<>(list.size());
        List<String> yData = new ArrayList<String>(list.size());
        list.forEach(item -> {
            if(item.getPointTime() != null){
                xData.add(item.getPointTime()+"：00");
                yData.add((item.getTotalCount() == null) ? "0":item.getTotalCount()+"");
            }
        });
        map.put("xData",xData);
        map.put("yData",yData);
        return map;
    }





    /**
     * 群组昨日关键数据统计
     * @return
     */
    @PostMapping("/groupYtdStatis")
    @ResponseBody
    public Map<String,Object> groupYtdStatis() {

        List<RepoStatisGroupDO> list = repoGroupStatisService.getGroupYtdStatis();
        if(list.size() != 8){
            logger.info("查询群组昨日关键数据异常============"+list);
        }
        Map<String,Object> map = new HashMap<>(4);
        List<String> yData1 = new ArrayList<>(2);
        List<String> yData2 = new ArrayList<>(2);
        List<String> yData3 = new ArrayList<>(2);
        List<String> yData4 = new ArrayList<>(2);
        list.forEach(item -> {
            if(item.getDataType() == 4){
                yData1.add(item.getTotalCount()+"");
            }else if(item.getDataType() == 1){
                yData2.add(item.getTotalCount()+"");
            }else if(item.getDataType() == 2){
                yData3.add(item.getTotalCount()+"");
            }else if(item.getDataType() == 3){
                yData4.add(item.getTotalCount()+"");
            }
        });
        map.put("yData1",yData1);
        map.put("yData2",yData2);
        map.put("yData3",yData3);
        map.put("yData4",yData4);
        return map;
    }
/*

    @RequestMapping("/groupStatistics")
    public String groupStatistic(@RequestParam String arriveType, Model model){
        model.addAttribute("arriveType",arriveType);
     return "operate/groupStatistics/groupStatistics";
    }

    @RequestMapping("/groupStatisticsArrive")
    public String groupStatisticArrive(){
        return "operate/groupStatistics/groupStatisticsArrive";
    }

    @RequestMapping("/groupStatisticsActive")
    public String groupStatisticsActive(){
        return "operate/groupStatistics/groupStatisticsActive";
    }
*/




    @RequestMapping("/userStatistics")
    public String userStatistic(@RequestParam String searchType, Model model){
        model.addAttribute("searchType",searchType);
        return "operate/userStatistics/userStatistics";
    }

    @RequestMapping("/userStatisticsArrive")
    public String userStatisticsArrive(@RequestParam String searchType, Model model){
        model.addAttribute("searchType",searchType);
        return "operate/userStatistics/userStatisticsArrive";
    }

    @RequestMapping("/userStatisticsActive")
    public String userStatisticsActive(@RequestParam String searchType, Model model){
        model.addAttribute("searchType",searchType);
        return "operate/userStatistics/userStatisticsActive";
    }

    @RequestMapping("/userStatisticsNewly")
    public String userStatisticsNewly(@RequestParam String searchType, Model model){
        model.addAttribute("searchType",searchType);
        return "operate/userStatistics/userStatisticsNewly";
    }



    /**
     * 用户昨日关键数据统计
     * @return
     */
    @PostMapping("/userYtdStatis")
    @ResponseBody
    public Map<String,Object> userYtdStatis() {

//        //用户到达值
//        List<RepoStatisUserDO> list =RepoUserInfoService.getTotalUser();
        List<RepoStatisUserDO> list = repoStatisUserService.getUserYtdStatis();
        if(list.size() != 9){
            logger.info("查询用户昨日关键数据异常============"+list);
        }
        Map<String,Object> map = new HashMap<>(3);
        List<String> yData1 = new ArrayList<>(2);
        List<String> yData2 = new ArrayList<>(2);
        List<String> yData3 = new ArrayList<>(2);
        list.forEach(item -> {
            if(item.getDataType() == 1){
                yData1.add(item.getTotalCount()+"");
            }else if(item.getDataType() == 2){
                yData2.add(item.getTotalCount()+"");
            }else if(item.getDataType() == 3){
                yData3.add(item.getTotalCount()+"");
            }
        });
        map.put("yData1",yData1);
        map.put("yData2",yData2);
        map.put("yData3",yData3);
        return map;
    }

    /**
     * 渠道来源数据统计
     * @return
     */
    @PostMapping("/userSourceChart")
    @ResponseBody
    public Map<String,Object> userSourceChart() {
        RepoChannelSourceDO list = repoChannelSourceSerive.getChannelSourceSratis();
        Map<String,Object> map = new HashMap<>(3);

        List<String> nameList = new ArrayList<>(5);
        List<Integer> valueList = new ArrayList<>(5);
        nameList.add("普通用户");
        valueList.add(list.getThree());
        nameList.add("电渠用户");
        valueList.add(list.getFive());
        nameList.add("toB来源用户");
        valueList.add(list.getToB());
        nameList.add("网上商城销售卡");
        valueList.add(list.getTenTwo());
        nameList.add("线下渠道");
        valueList.add(list.getTenThree());

        map.put("nameList",nameList);
        map.put("valueList",valueList);
        return map;
    }
}
