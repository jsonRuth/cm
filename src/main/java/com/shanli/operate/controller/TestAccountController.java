package com.shanli.operate.controller;


import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.shanli.common.utils.TimeUtils;
import com.shanli.operate.domain.cm.OrderinfoDO;
import com.shanli.operate.service.cm.OrderinfoService;
import com.shanli.operate.utils.DataUtil;
import com.shanli.operate.utils.EncryptUtils;
import com.shanli.operate.utils.ExcelUtil;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.annotation.Log;
import com.shanli.common.base.AdminBaseController;
import com.shanli.operate.domain.echat.AccountDO;
import com.shanli.operate.service.echat.AccountService;
import com.shanli.common.utils.Result;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * <pre>
 *
 * </pre>
 * <small> 2019-06-17 17:54:43 | niush</small>
 */
@Controller
@RequestMapping("/operate/testAccount")
public class TestAccountController extends AdminBaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AccountService userService;
    @Autowired
    private OrderinfoService orderinfoService;
    @Resource(name = "firstRedisTemplate")
    StringRedisTemplate stringRedisTemplate;

    @GetMapping()
    @RequiresPermissions("operate:testAccount:testAccount")
    String User() {
        return "operate/testAccount/testAccount";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("operate:testAccount:testAccount")
    public Result<Page<AccountDO>> list(AccountDO userDTO, String beginTime, String endTime) {

        Wrapper<AccountDO> wrapper = new EntityWrapper<AccountDO>().orderBy("test_create_time", false);
        if (!StringUtils.isEmpty(beginTime)) {
            wrapper = wrapper.ge("create_time", beginTime);
        }
        if (!StringUtils.isEmpty(endTime)) {
            wrapper = wrapper.le("create_time", endTime + " 23:59:59");
        }
        //账号
        if(!StringUtils.isEmpty(userDTO.getUserAccount())){
            wrapper = wrapper.like("User_Account",userDTO.getUserAccount());
        }
        //用户卡类型
        if(userDTO.getAccountType() > 0){
            wrapper = wrapper.eq("account_type",userDTO.getAccountType());
        }
        //用户类型
        if(userDTO.getServiceType() == 0){
            //免费用户
            wrapper = wrapper.addFilter("(User_ServiceEndTime < NOW() OR User_ServiceEndTime IS NULL)");
        }
        if(userDTO.getServiceType() == 1){
            //会员
            wrapper = wrapper.ge("User_ServiceEndTime",new Date());
        }
        //账号到期状态
        if(userDTO.getExpireEndStatus() == 0){
            //已到期
            wrapper = wrapper.lt("expire_time",new Date());
        }
        if(userDTO.getExpireEndStatus() == 1){
            //未到期
            wrapper = wrapper.ge("expire_time",new Date());
        }
        //账号状态
        if(userDTO.getIsUseable() == 0 || userDTO.getIsUseable() == 1){
            wrapper = wrapper.eq("is_useable",userDTO.getIsUseable());
        }
        //查询测试账号
        wrapper = wrapper.eq("is_test",1);

        //查询未删除状态的账号
        wrapper = wrapper.eq("isActive",1);

        Page<AccountDO> page = userService.selectPage(getPage(AccountDO.class), wrapper);
        page.getRecords().forEach(item -> {
            //账号到期状态
            if (item.getExpireTime() != null) {
                item.setExpireEndStatus(item.getExpireTime().compareTo(new Date()) >= 0 ? 1 : 0);
            }
            //用户类型
            if(item.getUserServiceendtime() != null){
                item.setServiceType(item.getUserServiceendtime().compareTo(new Date()) >= 0 ? 1 : 0);
            }else{
                item.setServiceType(0);
            }
        });
        return Result.ok(page);
    }

    /**
     * 禁用页面
     * @param model
     * @return
     */
    @GetMapping("/lock/{userId}")
    @RequiresPermissions("operate:testAccount:lock")
    public String lock(@PathVariable("userId") String userId,Model model) {

            AccountDO account = userService.selectById(userId);
            model.addAttribute("account", account);
            return "operate/testAccount/lock";
    }


    /**
     * 批量禁用页面
     * @param model
     * @return
     */
    @GetMapping("/batchLock/{userId}")
    @RequiresPermissions("operate:testAccount:lock")
    public String batchLock(@PathVariable("userId") String userId,Model model) {
            model.addAttribute("userIds",userId);
            return "operate/testAccount/batchLock";
    }




    @Log("测试账号禁用")
    @PostMapping("/forbidden")
    @ResponseBody
    @RequiresPermissions("operate:testAccount:lock")
    public Result<String> forbidden(AccountDO accountDO, String content) {
        Integer user_id = accountDO.getUser_id();
        if(user_id == null)return Result.build(Result.EnumStatusCode.EXPIRETIME_IS_EMPTY);
        Map<String,Object> param = new HashMap<>();
        param.put("user_id",user_id);
        param.put("isUseable",(byte)1);
        userService.updateAccountForbidden(param);
            //serService.updateAccountSelective(accountDO);
            return Result.ok();
    }





    @Log("测试账号批量禁用")
    @PostMapping("/batchForbidden")
    @ResponseBody
    @RequiresPermissions("operate:testAccount:lock")
    public Result<String> batchForbidden(String content,String userIds) {
          //批量处理
            String[] uids = userIds.split(",");
            Map<String ,Object> param = new HashMap<>( );
            param.put("uids",uids);
            userService.batchLockAccount(param);
            return Result.ok();
    }





    /**
     * 恢复页面
     * @param model
     * @return
     */
    @GetMapping("/unlock/{userId}")
    @RequiresPermissions("operate:testAccount:unLock")
    public String unlock(@PathVariable("userId") String userId,Model model) {
            AccountDO account = userService.selectById(userId);
            model.addAttribute("account", account);
            return "operate/testAccount/unlock";
    }

    /**
     * 批量恢复页面
     * @param model
     * @return
     */
    @GetMapping("/batchUnlock/{userId}")
    @RequiresPermissions("operate:testAccount:unLock")
    public String batchUnlock(@PathVariable("userId") String userId,Model model) {
            model.addAttribute("userIds", userId);
            return "operate/testAccount/batchUnlock";
    }



    @Log("测试账号恢复")
    @PostMapping("/unlock")
    @ResponseBody
    @RequiresPermissions("operate:testAccount:unLock")
    public Result<String> unLock(AccountDO accountDO, String content) {
        Integer user_id = accountDO.getUser_id();
        if(user_id == null)return Result.build(Result.EnumStatusCode.EXPIRETIME_IS_EMPTY);
        Map<String,Object> param = new HashMap<>();
        param.put("user_id",user_id);
        param.put("isUseable",(byte)0);
        userService.updateAccountForbidden(param);
            //userService.updateAccountSelective(accountDO);
            return Result.ok();
    }


    @Log("测试账号批量恢复")
    @PostMapping("/batchUnlock")
    @ResponseBody
    @RequiresPermissions("operate:testAccount:unLock")
    public Result<String> batchUnlock( String content,String userIds) {
        //批量处理
            String[] uids = userIds.split(",");
            Map<String ,Object> param = new HashMap<>( );
            param.put("uids",uids);
            userService.batchUnlockAccount(param);
            return Result.ok();
    }



    /**
     * 修改到期时间页面
     * @param userId
     * @param model
     * @return
     */
    @GetMapping("/edit/{userId}")
    @RequiresPermissions("operate:testAccount:edit")
    String edit(@PathVariable("userId") String userId, Model model) {

            AccountDO account = userService.selectById(userId);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            //账号到期时间
            String accountExpireTime = "";
            if(account.getExpireTime() != null){
                accountExpireTime = sf.format(account.getExpireTime());
            }
            //会员到期时间
            String memberExpireTime = "";
            if(account.getUserServiceendtime() == null || account.getUserServiceendtime().compareTo(new Date()) < 0){
                account.setServiceType(0);
                account.setServiceMonth(null);
            }else{
                memberExpireTime = sf.format(account.getUserServiceendtime());
                account.setServiceType(1);
            }
            model.addAttribute("account", account);
                model.addAttribute("accountExpireTime",accountExpireTime);
                model.addAttribute("memberExpireTime",memberExpireTime);
        return "operate/testAccount/edit";
    }

    @GetMapping("/batchEdit/{userId}")
    @RequiresPermissions("operate:testAccount:edit")
    String batchEdit(@PathVariable("userId") String userId, Model model) {
        model.addAttribute("userIds",userId);
        return "operate/testAccount/batchEdit";
    }



    @Log("测试账号修改")
    @ResponseBody
    @RequestMapping("/updateExpireTime")
    @RequiresPermissions("operate:testAccount:edit")
    public Result<String> updateExpireTime(AccountDO user,String content,String accountExpireTime,String memberExpireTime,String resetPassword) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(accountExpireTime == null) {
            return Result.build(Result.EnumStatusCode.EXPIRETIME_IS_EMPTY);
        }
        accountExpireTime += " 23:59:59";

        //对密码进行处理
        if(StringUtils.isNotEmpty(resetPassword)){
            resetPassword = EncryptUtils.encryptMD5ToString(resetPassword).toUpperCase();
        }

        //更新的数量
            try {
                user.setExpireTime(sf.parse(accountExpireTime));
                if(!StringUtils.isEmpty(memberExpireTime)){
                    memberExpireTime += " 23:59:59";
                    if(user.getServiceType() == 0){
                        //免费用户成为会员，修改会员开始时间
                        user.setUserServicebegintime(new Date());
                    }
                    user.setUserServiceendtime(sf.parse(memberExpireTime));
                }else {
                    if (user.getServiceType()==1){
                        user.setServiceType(0);
                        user.setUserServiceendtime(null);
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
                logger.info("时间转换错误");
            }
            user.setUserPassword(StringUtils.isNotEmpty(resetPassword) ? resetPassword:null);
            int updateCount = userService.updateAccountSelective(user);
            return updateCount > 0 ? Result.ok() : Result.fail();
    }

    @Log("测试账号批量修改")
    @ResponseBody
    @RequestMapping("/batchUpdateExpireTime")
    @RequiresPermissions("operate:testAccount:edit")
    public Result<String> batchUpdateExpireTime(String content,String accountExpireTime,String memberExpireTime,String userIds,String serviceMonth,String resetPassword) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(accountExpireTime == null) {
            return Result.build(Result.EnumStatusCode.EXPIRETIME_IS_EMPTY);
        }
        accountExpireTime += " 23:59:59";
        //对密码进行处理
        if(StringUtils.isNotEmpty(resetPassword)){
            resetPassword = EncryptUtils.encryptMD5ToString(resetPassword).toUpperCase();
        }
        Map<String,Object> param = new HashMap<>();
        param.put("ids",userIds.split(","));
        param.put("accountExpireTime",accountExpireTime);
        param.put("serviceMonth",serviceMonth);
        param.put("resetPassword",resetPassword);
        //判断memberExpireTime是否为空，1，为空表示设置为非会员，会员开始时间和结束时间设置为空，2.不为空
        if(StringUtils.isNotEmpty(memberExpireTime)){
            memberExpireTime += " 23:59:59";
            param.put("memberExpireTime",memberExpireTime);
        }else{
            param.put("memberExpireTime",null);
        }
        int update =  userService.batchUpdateExpireTime(param);
        return update > 0 ? Result.ok() : Result.fail();

    }



    /**
     * 删除页面
     * @param model
     * @return
     */
    @GetMapping("/remove/{userId}")
    @RequiresPermissions("operate:testAccount:remove")
    public String remove(@PathVariable("userId") String userId,Model model) {

            AccountDO account = userService.selectById(userId);
            model.addAttribute("account", account);
            return "operate/testAccount/remove";


    }

    /**
     * 删除页面
     * @param model
     * @return
     */
    @GetMapping("/batchRemove/{userId}")
    @RequiresPermissions("operate:testAccount:remove")
    public String batchRemove(@PathVariable("userId") String userId,Model model) {
        model.addAttribute("userIds",userId);
        return "operate/testAccount/batchRemove";

    }


    @Log("测试账号删除")
    @PostMapping("/delete")
    @ResponseBody
    @RequiresPermissions("operate:testAccount:remove")
    public Result<String> delete(AccountDO user,String content) {

            if(user == null){
                return Result.build(Result.EnumStatusCode.USER_INFO_ERROR);
            }
            //查询用户信息
            AccountDO userInfo = userService.selectById(user.getUser_id());
            if(userInfo == null){
                return Result.build(Result.EnumStatusCode.USER_INFO_ERROR);
            }
            if (userInfo.getAccountType()==1){
                userInfo.setIsactive(0);
            }if (userInfo.getAccountType()==2){
                //物联卡用户删除表示转为正式账号

                //设置为测试用户
                userInfo.setIsTest((byte) 0);

                //判断是否首次成为正式用户
                Map<String,Object> map = new HashMap<String,Object>();
                map.put("OperatorPhone", userInfo.getUserAccount());
                List<OrderinfoDO> list = orderinfoService.selectByMap(map);
                if(list == null || list.isEmpty()){
                    //无订单，并且会员有效期为空表示首次转为正式用户，会员有效期设置为空
                    if(userInfo.getUserOldServicebegintime() != null && userInfo.getUserOldServiceendtime() != null){
                        userInfo.setUserServicebegintime(userInfo.getUserOldServicebegintime());
                        userInfo.setUserServiceendtime(userInfo.getUserOldServiceendtime());
                    }else{
                        userInfo.setUserServicebegintime(null);
                        userInfo.setUserServiceendtime(null);
                        //放入redis
                        stringRedisTemplate.opsForList().leftPush("REDIS_TASK",userInfo.getUserAccount());
                    }
                }else{
                    //已有订单表示之前是正式用户，转为测试用户保存其会员有效期
                    if(userInfo.getUserOldServicebegintime() != null && userInfo.getUserOldServiceendtime() != null){
                        userInfo.setUserServicebegintime(userInfo.getUserOldServicebegintime());
                        userInfo.setUserServiceendtime(userInfo.getUserOldServiceendtime());
                    }else{
                        userInfo.setUserServicebegintime(null);
                        userInfo.setUserServiceendtime(null);
                    }
                }
            }
            try {
                userService.updateAllColumnById(userInfo);

            } catch (Exception e) {
                e.printStackTrace();
            }
        return Result.ok();
    }


    @Log("测试账号批量删除")
    @PostMapping("/batchDelete")
    @ResponseBody
    @RequiresPermissions("operate:testAccount:remove")
    public Result<String> batchDelete(String content,String userIds) {


        if(StringUtils.isNotEmpty(userIds)){
            String[] uids = userIds.split(",");
            Map<String,Object> param = new HashMap<>();
            param.put("uids",uids);
            try {
                userService.batchDeleteUserInfo(param);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            return Result.build(Result.EnumStatusCode.USER_INFO_ERROR);
        }
        return Result.ok();

    }




    /**
     * 生成APP账号页面
     * @return
     */
    @GetMapping("/addAppAccount")
    @RequiresPermissions("operate:testAccount:add")
    public String addAppAccount() {
        return "operate/testAccount/addAppAccount";
    }

    @Log("生成APP测试账号")
    @ResponseBody
    @PostMapping("/saveAppAccount")
    @RequiresPermissions("operate:testAccount:add")
    public Result<String> saveAppAccount(HttpServletResponse response, AccountDO accountDO,String testId, Integer count, String userExpireTime, String serviceendtime, String exportExcel) {

        String ExpireTime = userExpireTime;
        String serviceEndTime = serviceendtime;
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(userExpireTime == null){
            return Result.build(Result.EnumStatusCode.EXPIRETIME_IS_EMPTY);
        }

        userExpireTime += " 23:59:59";

        try {
            Date expireTime = sf.parse(userExpireTime);
            accountDO.setExpireTime(expireTime);
            if (!StringUtils.isBlank(serviceendtime)){
                serviceendtime +=" 23:59:59";
                Date userServiceendtime = sf.parse(serviceendtime);
                accountDO.setUserServiceendtime(userServiceendtime);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            logger.info("时间转换错误");
        };

        //取出原先的密码，用于导出excel

        String password = accountDO.getUserPassword();

        //将对象存入map集合,将用户账号设为key
        accountDO.setUserPassword(EncryptUtils.encryptMD5ToString(accountDO.getUserPassword()));
        accountDO.setIsTest((byte) 1);
        Date date = new Date();
        accountDO.setAccountType(1);
        accountDO.setTestCreateTime(date);
        accountDO.setUserServicebegintime(date);
        accountDO.setUserServiceendtime(accountDO.getUserServiceendtime());
        Map<String,Map<String,AccountDO>> parms = new HashMap<>();
        Map<String,AccountDO> map = new HashMap<>();
        List<String> list = new ArrayList<>(count);
        boolean flag = true;
        int[] temp = { 10, 11, 12};
        do{
            for (int i =0 ; i< count ; i++){
                    Long number = (long)(Math.random() * (999999999L - 100000000L) +100000000L);
                    int index = (int) (Math.random() * temp.length);
                    String userAccount = temp[index] + String.valueOf(number);
                    list.add(userAccount);
                    map.put(userAccount,accountDO);
            }
            Set<String> set = new HashSet<>(list);
            //2查询导入的账号是否已存在
            List<AccountDO> accountDOList = userService.getUserAccountByNames(list);
            flag = (list.size() != set.size() && accountDOList.size()>0);
            if (flag==true){
                logger.info("账号生成重复，重新生成");
                list.clear();
                map.clear();
            }
        }while (flag);

        parms.put("keys",map);
        //添加
        if(map.size() > 0){
            try {
                userService.addAccountBatch(parms);


                //账号信息导出   账号、密码、账号到期时间、会员到期时间、会员期限。
                if(StringUtils.isNotEmpty(exportExcel) && "1".equals(exportExcel)){
                    List<Map<String,String>> accoutList = new ArrayList<>();
                    Map<String,String> dataMap;
                    for(String item:list){
                        dataMap = new LinkedHashMap<>();
                        dataMap.put("accountNo",item);
                        dataMap.put("password",password);
                        dataMap.put("userExpireTime",ExpireTime);
                        dataMap.put("serviceendtime",StringUtils.isEmpty(serviceEndTime)? "":serviceEndTime);
                        dataMap.put("serviceMonth",accountDO.getServiceMonth()==null?"":accountDO.getServiceMonth().toString());
                        accoutList.add(dataMap);
                    }

                    //转json数据
                    String json = JSON.toJSONString(accoutList);
                    stringRedisTemplate.opsForValue().set(getUsername()+":appData",json);

                }
            } catch (DuplicateKeyException ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
            } catch (Exception ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_IMPORT_ERROR);
            }
        }
        return Result.ok();
    }

    private void exportCreateAppAccount(String fileName, List<LinkedHashMap<String, Object>> list, HttpServletResponse response) {

        //账号、密码、账号到期时间、会员到期时间、会员期限。
        String[] title = {"账号","密码","账号到期时间","会员到期时间","会员期限(天)"};
        ExcelUtil.exportExcel(list,response,title,"sheet1",fileName);

    }


    @GetMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response){

       String json = stringRedisTemplate.opsForValue().get(getUsername()+":appData");

       List<LinkedHashMap<String,Object>> data = JSON.parseObject(json,new TypeReference<List<LinkedHashMap<String,Object>>>(){});

       String fileName = "生成的APP账号" + System.currentTimeMillis() + ".xlsx";
                    exportCreateAppAccount(fileName, data, response);
       //stringRedisTemplate.delete(getUsername()+":appData");



    }


    /**
     * 绑定物联卡账号页面
     * @return
     */
    @GetMapping("/bindCardAccount")
    @RequiresPermissions("operate:testAccount:add")
    public String bindCardAccount() {
        return "operate/testAccount/bindCardAccount";
    }

    @Log("绑定测试物联卡账号")
    @ResponseBody
    @PostMapping("/saveBindCardAccount")
    @RequiresPermissions("operate:testAccount:add")
    public Result<String> saveBindCardAccount(AccountDO accountDO,String testId ,String accounts,String userExpireTime,String serviceendtime) {


        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(userExpireTime == null){
            return Result.build(Result.EnumStatusCode.EXPIRETIME_IS_EMPTY);
        }
        userExpireTime += " 23:59:59";
        try {
            Date expireTime = sf.parse(userExpireTime);
            accountDO.setExpireTime(expireTime);

            serviceendtime +=" 23:59:59";
            Date userServiceendtime = sf.parse(serviceendtime);
            accountDO.setUserServiceendtime(userServiceendtime);

        } catch (ParseException e) {
            e.printStackTrace();
            logger.info("时间转换错误");
        };
        //判断accounts是否为空
        if(StringUtils.isEmpty(accounts)){
            return Result.build(Result.EnumStatusCode.ACCOUNT_IS_EMPTY);
        }
        //将accounts切割并封装到String数组里面
        String[] account = accounts.split("\r\n");
        //判断数组长度不能大于10
        if(account.length > 10){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_OUT_RANGE);
        }
        //账号数组转为list
        List<String> list = Arrays.asList(account);
        for (String emailAddr : list) {
            if (emailAddr == null || emailAddr.length()==0 || emailAddr=="" || emailAddr.trim().isEmpty()){
                return Result.build(Result.EnumStatusCode.WRONG_ACCOUNT_FORMAT);
            }
            //账号不为空判断
            if (StringUtils.isEmpty(emailAddr)) {
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_INVALID);
            }
            //账号是否首位为1，长度是否为13位号码
            if (!emailAddr.substring(0, 1).equals("1") || emailAddr.length() != 13 || !emailAddr.matches("^[0-9]*$")) {
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_INVALID);
            }
        }
        //判断录入的中号中是否有重复账号
        Set<String> set = new HashSet<String>(list);
        if(set.size() != list.size()){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_DUPLICATE);
        }

        //已转为测试用户的集合
        Map<String,Object> exchangeMap = new HashMap<>();
        //需要转为测试用户的集合
        List<String> switchList = new ArrayList<>();

        //查询录入的账号是否已存在
        List<AccountDO> duplicateList = userService.getUserAccountByNames(list);
        if(duplicateList != null && duplicateList.size() > 0){
            List<AccountDO> testList = new ArrayList<>();

            //将已存在的正式账号转为测试账号
            duplicateList.forEach(item -> {
                if(item.getIsTest() == 0){
                    switchList.add(item.getUserAccount());
                    exchangeMap.put(item.getUserAccount(),item.getUserAccount());
                }else{
                    testList.add(item);
                }
            });
            //如果存在测试账号
            if(testList != null && !testList.isEmpty()){
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
            }
        }

        //查询录入的账号是否存在已删除的账号
        List<AccountDO> deletedList = userService.getUserAccountDeletedByNames(list);
        if(deletedList != null && deletedList.size() > 0){
            deletedList.forEach(item -> {
                switchList.add(item.getUserAccount());
                exchangeMap.put(item.getUserAccount(),item.getUserAccount());
            });
        }

        //将AccountDO对象存入map集合,将用户账号设为key
        accountDO.setUserPassword(EncryptUtils.encryptMD5ToString("111111").toUpperCase());
        accountDO.setIsTest((byte)1);
        accountDO.setAccountType(2);
        Date date = new Date();
        accountDO.setUserServicebegintime(date);
        accountDO.setTestCreateTime(date);

        //将已存在的正式账号、已删除账号转为测试账号
        if(switchList != null && switchList.size() > 0){
            userService.switchAccountTest(switchList,accountDO);
        }

        Map<String,AccountDO> map = new HashMap<>();
        //再将map集合进行map封装,key值设为相同的keys
        Map<String,Map<String,AccountDO>> parms = new HashMap<>();
        //循环导入的数据，放入Map集合
        for(String accountTmp:list) {
            //已转为测试账号，则不再添加
            if(exchangeMap.containsKey(accountTmp)){
                continue;
            }

            map.put(accountTmp,accountDO);
        }

        //添加
        if(map.size() > 0){
            parms.put("keys",map);
            try {
                userService.addAccountBatch(parms);
            } catch (DuplicateKeyException ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
            } catch (Exception ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_IMPORT_ERROR);
            }
        }
        return Result.ok();
    }

    /**
     * 导入物联卡账号页面
     * @return
     */
    @GetMapping("/importCardAccount")
    @RequiresPermissions("operate:testAccount:add")
    public String importCardAccount() {
        return "operate/testAccount/importCardAccount";
    }

    @Log("导入物联卡测试账号")
    @ResponseBody
    @RequestMapping("/saveImportCardAccount")
    @RequiresPermissions("operate:testAccount:add")
    public Result<String> saveImportCardAccount(MultipartFile file ,String testId) {
        List<Map<String, Object>> list;
        try {
            list = ExcelUtil.readExcelByFileForXlsx(file.getInputStream());
        } catch (Exception ex) {
            ex.printStackTrace();
            return Result.build(Result.EnumStatusCode.EXCEL_ERROR);
        }
        //读取不到数据
        if(list.size()==0){
            return Result.build(Result.EnumStatusCode.FORMAT_IS_WRONG);
        }
        Map<String,Object> mapTitle = list.get(0);
        if(!mapTitle.containsKey("账号") || !mapTitle.containsKey("账号到期时间")){
            return Result.build(Result.EnumStatusCode.FORMAT_IS_WRONG);
        }
        //导入数量限制1000
        if(list.size() > 1000){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_OUT_RANGE);
        }
        List<String> accountList = new ArrayList<>();
        List<AccountDO> addList = new ArrayList<>(list.size());


        //将导入的account放入集合
        Iterator<Map<String,Object>> it = list.iterator();
        while(it.hasNext()){
            Map<String, Object> item = it.next();
            if(DataUtil.ObjectIsNull(item.get("账号"))
                    && DataUtil.ObjectIsNull(item.get("账号到期时间"))
                    && DataUtil.ObjectIsNull(item.get("会员到期时间"))){
                it.remove();
            }else {
                accountList.add(item.get("账号").toString());
            }
        }

        //判断导入的中号中是否有重复账号
        Set<String> set = new HashSet<String>(accountList);
        if(set.size() != accountList.size()){
            return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_DUPLICATE);
        }

        //需要转为测试用户的集合
        List<String> switchList = new ArrayList<>();

        //查询导入的账号是否已存在
        List<AccountDO> duplicateList = userService.getUserAccountByNames(accountList);
        if(duplicateList != null && duplicateList.size() > 0){
            List<AccountDO> testList = new ArrayList<>();

            //将已存在的正式账号转为测试账号
            duplicateList.forEach(item -> {
                if(item.getIsTest() == 0){
                    switchList.add(item.getUserAccount());
                }else{
                    testList.add(item);
                }
            });
            //如果存在测试账号
            if(testList != null && !testList.isEmpty()){
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
            }
        }

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String pwd = EncryptUtils.encryptMD5ToString("111111").toUpperCase();
        Date curDate = new Date();
        //循环导入的数据，放入集合
        for(Map<String,Object> map:list){
            AccountDO accountDO = new AccountDO();
            //账号不为空判断
            if(map.get("账号") == null || map.get("账号")==""){
                return Result.build(Result.EnumStatusCode.ACCOUNT_IS_EMPTY);
            }
            //账号是否首位为1，长度是否为13位号码
            String account = map.get("账号").toString();
            if(!account.substring(0,1).equals("1") || account.length() != 13 || !account.matches("^[0-9]*$")){
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_INVALID);
            }
            accountDO.setUserAccount(map.get("账号").toString());
            accountDO.setUserName(map.get("账号").toString());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");


            if(map.get("账号到期时间") != null ){
                try {
                    Date date1 = format.parse(map.get("账号到期时间").toString());
                    Date date = new Date();
                    if (date1.getTime()>date.getTime()){
                        accountDO.setExpireTime(sf.parse(map.get("账号到期时间").toString()+" 23:59:59"));
                    }else {
                        return Result.build(Result.EnumStatusCode.DUE_TIME_ERROR);
                    }
                } catch (ParseException e) {
                    logger.info("时间格式化异常，日期为：" + map.get("账号到期时间"));
                    return Result.build(Result.EnumStatusCode.EXPIRETIME_ERROR);
                }
            }else {
                return Result.build(Result.EnumStatusCode.MEMBERSHIP_EXPIRY_DATE_ERROR);
            }
            //设置会员到期时间
//            Pattern pattern = Pattern.compile("[0-9]*");
//            String buyNumber = String.valueOf(map.get("会员有效期"));
//            if(map.get("会员有效期") != null && pattern.matcher(buyNumber).matches() && Integer.parseInt(map.get("会员有效期").toString())<=1080 && Integer.parseInt(map.get("会员有效期").toString())>=1){
//                accountDO.setServiceMonth(Integer.parseInt(map.get("会员有效期").toString()));
//                Calendar calendar = Calendar.getInstance();
//                calendar.add(Calendar.DATE,Integer.parseInt(map.get("会员有效期").toString()));
//                accountDO.setUserServiceendtime(calendar.getTime());
//            } else if (map.get("会员有效期") == null || map.get("会员有效期")==""|| Integer.parseInt(map.get("会员有效期").toString())==0){
//                accountDO.setUserServiceendtime(null);
//            } else {
//                return Result.build(Result.EnumStatusCode.MEMBERSHIP_EXPIRY_DATE_ERROR);
//            }

            if(map.get("会员到期时间") != null ){
                try {
                    Date accountExpire = format.parse(map.get("账号到期时间").toString());
                    Date date1 = format.parse(map.get("会员到期时间").toString());
                    Date date = new Date();
                    if (date1.getTime()>=date.getTime() && date1.getTime()<=accountExpire.getTime()){
                        accountDO.setUserServiceendtime(sf.parse(map.get("会员到期时间").toString()+" 23:59:59"));
                        //计算会员到期天数
                        int days = TimeUtils.daysBetween(date,date1);
                        accountDO.setServiceMonth(days+1);

                    }else if(date1.getTime()<date.getTime()){
                        return Result.build(Result.EnumStatusCode.DUE_TIME_ERROR_MEMBER);
                    }else if(date1.getTime() > accountExpire.getTime()){
                        return Result.build(Result.EnumStatusCode.DUE_TIME_ERROR_COMPARE);
                    }
                } catch (ParseException e) {
                    logger.info("时间格式化异常，日期为：" + map.get("会员到期时间"));
                    return Result.build(Result.EnumStatusCode.EXPIRETIME_ERROR);
                }
            }else {
                return Result.build(Result.EnumStatusCode.MEMBERSHIP_EXPIRY_DATE_ERROR);
            }


            accountDO.setAccountType(2);
            accountDO.setIsTest((byte)1);
            accountDO.setUserPassword(pwd);
            Date date = new Date();
            accountDO.setTestCreateTime(date);
            accountDO.setUserServicebegintime(curDate);
            addList.add(accountDO);
        }

        //添加
        if(addList.size() > 0){
            //保存正式转测试的账号的会员有效期
            if(switchList != null && switchList.size() > 0){
                userService.saveAccountService(switchList);
            }
            try {
//                userService.addBatch(addList);
                userService.addBatchOnUpdate(addList);
            } catch (DuplicateKeyException ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_EXIST);
            } catch (Exception ex) {
                ex.printStackTrace();
                return Result.build(Result.EnumStatusCode.IMPORTACCOUNT_IMPORT_ERROR);
            }
        }
        return Result.ok();
    }

    /**
     * 重置密码页面
     * @return
     */
    @GetMapping("/resetPwd")
    @RequiresPermissions("operate:testAccount:edit")
    public String resetPwd(Model model,Integer[] ids) {
        model.addAttribute("userIds", Arrays.toString(ids));
        return "operate/testAccount/resetPwd";
    }

    @Log("重置密码")
    @PostMapping("/saveResetPwd")
    @ResponseBody
    @RequiresPermissions("operate:testAccount:edit")
    public Result<String> saveResetPwd(String userIds,String password) {
        if (StringUtils.isEmpty(password) || StringUtils.isEmpty(userIds)) {
            return Result.fail();
        }
        userIds = userIds.substring(1,userIds.length()-1);
        String[] idStr = userIds.split(",");
        Integer[] idNum = (Integer[]) ConvertUtils.convert(idStr,Integer.class);
        password = EncryptUtils.encryptMD5ToString(password).toUpperCase();
        userService.resetTestAccountPwd(Arrays.asList(idNum),password);
        return Result.ok();
    }

}
