package com.shanli.operate.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.annotation.Log;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.utils.Result;
import com.shanli.operate.domain.collect.RepoActiveGroupDetailsDO;
import com.shanli.operate.domain.collect.RepoGroupInfoDO;
import com.shanli.operate.domain.echat.ChatGroupDO;
import com.shanli.operate.service.collect.RepoActiveGroupDetailsService;
import com.shanli.operate.service.collect.RepoGroupInfoService;
import com.shanli.operate.service.echat.AccountService;
import com.shanli.operate.service.echat.ChatGroupService;
import com.shanli.operate.service.echat.UserOfGroupService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/operate/groupStatistics")
public class GroupInfoController extends AdminBaseController {

    @Autowired
    private RepoGroupInfoService groupInfoService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private UserOfGroupService userOfGroupService;
    @Autowired
    ChatGroupService chatGroupService;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    private RepoActiveGroupDetailsService repoActiveGroupDetailsService;

    @GetMapping()
    @RequiresPermissions("operate:groupStatistics:groupStatistics")
    String groupStatistics(){
        return "operate/groupStatistics/groupStatistics";
    }


    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("operate:groupStatistics:groupStatistics")
    public Result<Page<RepoGroupInfoDO>> list(Integer pageNumber , Integer pageSize ,RepoGroupInfoDO groupInfo) {

        Page<RepoGroupInfoDO> page = groupInfoService.selectPageGroup(pageNumber,pageSize,groupInfo);
        return Result.ok(page);
    }

    @GetMapping("/groupStatisticsActive")
    public String groupStatisticArrive(){
        return "operate/groupStatistics/groupStatisticsActive";
    }


    /**
     * 群组活跃数据
     * @param pageNumber
     * @param pageSize
     * @param activeGroup
     * @return
     */
    @ResponseBody
    @GetMapping("/listActive")
    @RequiresPermissions("operate:groupStatistics:groupStatistics")
    public Result<Page<RepoActiveGroupDetailsDO>> listActive(Integer pageNumber , Integer pageSize , RepoActiveGroupDetailsDO activeGroup) {
        Page<RepoActiveGroupDetailsDO> page = repoActiveGroupDetailsService.selectActivePageGroup(pageNumber,pageSize,activeGroup);
        return Result.ok(page);
    }


    /**
     * 查询群组是否存在
     * @param id
     * @return
     */
    @ResponseBody
    @GetMapping("/userOfGroup")
    @RequiresPermissions("operate:groupStatistics:groupStatistics")
    Result<String> userOfGroup(Integer id) {

        if (id==null){
            return Result.build(Result.EnumStatusCode.USERS_NO_GROUP_ERROR);
        }
        List<Long> cgIds = userOfGroupService.selectByUserId(id);
        if (cgIds.size()==0){
            return Result.build(Result.EnumStatusCode.USERS_NO_GROUP_ERROR);
        }
        return Result.ok();
    }


    /**
     * 请求群组统计页面
     *
     * @return
     */
    @GetMapping("/groupList")
    @RequiresPermissions("operate:groupStatistics:groupStatistics")
    String userList(Integer userId,Model model) {
        model.addAttribute("userId",userId);
        return "operate/groupStatistics/groupStatisticsUser";
    }


    @ResponseBody
    @GetMapping("/groupListUser")
    @RequiresPermissions("operate:groupStatistics:groupStatistics")
    public Result<Page<RepoGroupInfoDO>> groupListUser(Integer pageNumber , Integer pageSize ,Integer userId ,RepoGroupInfoDO groupInfo) {
        List<Long> Cgids = userOfGroupService.selectByUserId(userId);
        Page<RepoGroupInfoDO> page = groupInfoService.selectPageByCgids(pageNumber,pageSize,Cgids ,groupInfo);
        return Result.ok(page);
    }


    //自定义列表页面
    @GetMapping("/customInput")
    @RequiresPermissions("operate:groupStatistics:custom")
    public String customList() {
        return "operate/groupStatistics/customList";
    }


    //解散群组页面
    @GetMapping("/dissolve/{id}")
    @RequiresPermissions("operate:groupStatistics:dissolve")
    public String dissolve(@PathVariable("id") Long Cg_ID,Model model){
        model.addAttribute("Cg_ID",Cg_ID);
        return "operate/groupStatistics/dissolve";
    }

    //解散群组
    @Log("解散群组")
    @PostMapping("/dissolveGroup")
    @ResponseBody
    @RequiresPermissions("operate:groupStatistics:dissolve")
    public Result<String> removeGroup(Long Cg_ID ,String content){
        try {
            groupInfoService.updateByIsActive(Cg_ID);
            userOfGroupService.updateByCgidIsActive(Cg_ID);
            chatGroupService.updateByChatGroupIsActive(Cg_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok();
    }


    //禁言页面
    @GetMapping("/banned/{id}")
    @RequiresPermissions("operate:groupStatistics:banned")
    public String banned(@PathVariable("id")Long Cg_ID,Model model){
        model.addAttribute("Cg_ID",Cg_ID);
        return "operate/groupStatistics/banned";
    }


    //禁言群组
    @Log("禁言群组")
    @PostMapping("/bannedGroup")
    @ResponseBody
    @RequiresPermissions("operate:groupStatistics:banned")
    public Result<String> bannedGroup(ChatGroupDO chatGroupDO,String content){
        redisTemplate.opsForValue().set(chatGroupDO.getCg_ID()+":group",1 ,chatGroupDO.getCgBannedLength() ,TimeUnit.HOURS);
        Integer cgBannedLength = chatGroupDO.getCgBannedLength();
        Date cgBannedTime = new Date();
        long time1 = cgBannedTime.getTime();
        long time = time1 + Long.valueOf(cgBannedLength*60*60*1000);
        cgBannedTime = new Date(time);
        try {
            chatGroupService.updateByIdChatGroup(chatGroupDO.getCg_ID(),cgBannedLength,cgBannedTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok();
    }


    //恢复禁言页面
    @GetMapping("/removeBanned/{id}")
    @RequiresPermissions("operate:groupStatistics:removeBanned")
    public String removeBanned(@PathVariable("id")Long Cg_ID,Model model){
        ChatGroupDO chatGroupDO = chatGroupService.selectById(Cg_ID);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String cgBannedTime = sf.format(chatGroupDO.getCgBannedTime());
        model.addAttribute("chatGroupDO",chatGroupDO);
        model.addAttribute("cgBannedTime",cgBannedTime);
        return "operate/groupStatistics/removeBanned";
    }

    /**
     * 恢复禁言功能
     * @param content
     * @param chatGroupDO
     * @return
     */
    @Log("恢复禁言")
    @PostMapping("/removeBanned")
    @ResponseBody
    @RequiresPermissions("operate:groupStatistics:removeBanned")
    public Result<String> removeBannedGroup(String content,ChatGroupDO chatGroupDO){
        try {
            chatGroupService.updateByChatGroup(chatGroupDO.getCg_ID());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok();
    }


}
