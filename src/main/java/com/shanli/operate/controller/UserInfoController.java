package com.shanli.operate.controller;


import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.annotation.Log;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.utils.Result;
import com.shanli.operate.domain.collect.RepoActiveUserDO;
import com.shanli.operate.domain.collect.RepoUserInfoDO;
import com.shanli.operate.domain.echat.AccountDO;
import com.shanli.operate.service.collect.RepoActiveUserService;
import com.shanli.operate.service.collect.RepoUserInfoService;
import com.shanli.operate.service.echat.AccountService;
import com.shanli.operate.service.echat.UserOfGroupService;
import com.shanli.sys.domain.UserDO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/operate/userStatistics")
public class UserInfoController extends AdminBaseController {

    @Autowired
    private RepoUserInfoService userInfoService;
    @Autowired
    private UserOfGroupService userOfGroupService;
    @Autowired
    private AccountService accountService;
    @Resource(name = "firstRedisTemplate")
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RepoActiveUserService repoActiveUserService;

    @GetMapping()
    @RequiresPermissions("operate:userStatistics:userStatistics")
    String userStatistics(){
        return "operate/userStatistics/userStatistics";
    }


    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("operate:userStatistics:userStatistics")
    public Result<Page<RepoUserInfoDO>> list(Integer pageNumber , Integer pageSize ,RepoUserInfoDO userInfo) {
        Page<RepoUserInfoDO> page = userInfoService.selectPageUser(pageNumber,pageSize,userInfo);
        return Result.ok(page);
    }

    /**
     * 查询活跃值数据
     * @param pageNumber
     * @param pageSize
     * @param userActive
     * @return
     */
    @ResponseBody
    @GetMapping("/listActive")
    @RequiresPermissions("operate:userStatistics:userStatistics")
    public Result<Page<RepoActiveUserDO>> listActive(Integer pageNumber , Integer pageSize ,RepoActiveUserDO userActive) {
        Page<RepoActiveUserDO> page = repoActiveUserService.selectPageUser(pageNumber,pageSize,userActive);
        return Result.ok(page);
    }
    /**
     * 禁用页面
     * @param user_id
     * @param model
     * @return
     */
    @GetMapping("/userBanned/{id}")
    @RequiresPermissions("operate:userStatistics:userBanned")
    public String banned(@PathVariable("id")Integer user_id,Model model){
        model.addAttribute("user_id",user_id);
        return "operate/userStatistics/userBanned";
    }

    @Log("禁用用户")
    @PostMapping("/userBanned")
    @ResponseBody
    @RequiresPermissions("operate:userStatistics:userBanned")
    public Result<String> userBanned(String content,AccountDO accountDO){
        redisTemplate.opsForValue().set(accountDO.getUser_id()+":account",2 ,accountDO.getUserBannedLength() ,TimeUnit.HOURS);
        Integer userBannedLength = accountDO.getUserBannedLength();
        Date userBannedTime = new Date();
        long time1 = userBannedTime.getTime();
        long time = time1 + Long.valueOf(userBannedLength*60*60*1000);
        userBannedTime = new Date(time);
        try {
            accountService.updateByIdUser(accountDO.getUser_id(),userBannedLength,userBannedTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok();
    }

    /**
     * 恢复页面
     * @param user_id
     * @param model
     * @return
     */
    @GetMapping("/userRecover/{id}")
    @RequiresPermissions("operate:userStatistics:userRecover")
    public String recover(@PathVariable("id")Integer user_id,Model model){
        AccountDO accountDO = accountService.selectById(user_id);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String userBannedTime = sf.format(accountDO.getUserBannedTime());
        model.addAttribute("accountDO",accountDO);
        model.addAttribute("userBannedTime",userBannedTime);
        return "operate/userStatistics/userRecover";
    }


    /**
     * 恢复用户
     * @param content
     * @param accountDO
     * @return
     */
    @Log("恢复用户")
    @PostMapping("/userRecover")
    @ResponseBody
    @RequiresPermissions("operate:userStatistics:userRecover")
    public Result<String> userRecover(String content,AccountDO accountDO){
        try {
            accountService.updateByUserRecover(accountDO.getUser_id());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok();
    }

    /**
     * 查询用户是否存在
     * @param Cgid
     * @return
     */
    @ResponseBody
    @GetMapping("/userOfGroup")
    @RequiresPermissions("operate:userStatistics:userStatistics")
    Result<String> userOfGroup(@RequestParam(value = "Cgid") Long Cgid) {
        List<Integer> userId = userOfGroupService.selectByCgid(Cgid);
        if (userId.size()==0){
            return Result.build(Result.EnumStatusCode.GROUP_NO_USERS_ERROR);
        }
        List<AccountDO> accountDOS = accountService.selectBatchIds(userId);
        if (accountDOS.size()==0){
            return Result.build(Result.EnumStatusCode.GROUP_NO_USERS_ERROR);
        }
        return Result.ok();
    }

    /**
     * 请求用户统计页面
     *
     * @return
     */
    @GetMapping("/userList")
    @RequiresPermissions("operate:userStatistics:userStatistics")
    String userList(Long Cgid, Model model) {
        model.addAttribute("Cgid",Cgid);
        return "operate/userStatistics/userStatisticsGroup";
    }


    /**
     * 根据群组id条件查询分页
     * @param pageNumber
     * @param pageSize
     * @param Cgid
     * @param userInfo
     * @return
     */
    @ResponseBody
    @GetMapping("/userListGroup")
    @RequiresPermissions("operate:userStatistics:userStatistics")
    public Result<Page<RepoUserInfoDO>> userList(Integer pageNumber , Integer pageSize ,Long Cgid, RepoUserInfoDO userInfo) {
        List<Integer> userId = userOfGroupService.selectByCgid(Cgid);
        Page<RepoUserInfoDO> page = userInfoService.selectByCgidPageUser(pageNumber, pageSize, userId, userInfo);
        return Result.ok(page);
    }



    @GetMapping("/saveColumn")
    @ResponseBody
    public Result<String> saveColumn(@RequestParam String json,@RequestParam String showType){
        UserDO user = getUser();
        stringRedisTemplate.opsForValue().set(user.getUsername()+":userColumn:"+showType,json);
        return Result.ok();
    }

    @ResponseBody
    @GetMapping("/getColumnInfo")
    public Result<String> getColumnInfo(@RequestParam String showType){
        UserDO user = getUser();
        String json = stringRedisTemplate.opsForValue().get(user.getUsername()+":userColumn:"+showType);
        return Result.ok(json);
    }



    @GetMapping("/saveGroupColumn")
    @ResponseBody
    public Result<String> saveGroupColumn(@RequestParam String json){
        UserDO user = getUser();
        stringRedisTemplate.opsForValue().set(user.getUsername()+":userGroupColumn",json);
        return Result.ok();
    }

    @ResponseBody
    @GetMapping("/getGroupColumnInfo")
    public Result<String> getGroupColumnInfo(){
        UserDO user = getUser();
        String json = stringRedisTemplate.opsForValue().get(user.getUsername()+":userGroupColumn");
        return Result.ok(json);
    }


}


