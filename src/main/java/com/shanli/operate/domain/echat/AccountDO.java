package com.shanli.operate.domain.echat;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import lombok.Data;

import java.util.Date;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2019-06-17 17:54:43 | niush</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("tb_user")
public class AccountDO {
   @TableId(type= IdType.AUTO)
   private Integer user_id;
   @TableField("User_Account")
  /** 用户帐号 */
 private String userAccount;
   @TableField("User_Password")
  /** 用户密码 */
 private String userPassword;
   @TableField("User_Name")
  /** 用户名称 */
 private String userName;
   @TableField("User_CompanyID")
  /** 组织ID */
 private Integer userCompanyid;
   @TableField("User_Type")
  /** 0:对讲用户。3：调度员 */
 private Integer userType;
   @TableField("User_DefaultChatGroup")
  /** 默认群组 */
 private Long userDefaultchatgroup;
   @TableField("User_ServiceBeginTime")
  /** 帐户使用期限（起始日期） */
 private Date userServicebegintime;
   @TableField("User_ServiceUpDateTime")
  /** 最后更新时间 */
 private Date userServiceupdatetime;
   @TableField("User_ServiceCreateTime")
  /** 该条数据创建时间 */
 private Date userServicecreatetime;
   @TableField("User_ServiceEndTime")
  /** 帐户使用期限（截至日期） */
 private Date userServiceendtime;
   @TableField("User_GPSswitch")
  /** GPS开关 */
 private Integer userGpsswitch;
   @TableField("User_GPSfrequency")
  /** GPS上报频率，单位秒 */
 private Integer userGpsfrequency;
   @TableField("User_QuickDialNum")
  /** 用户快拨号码 */
 private String userQuickdialnum;
   @TableField("User_Avatar")
  /** 用户头像信息 */
 private String userAvatar;
   @TableField("User_Sex")
  /** 性别 */
 private Integer userSex;
   @TableField("isActive")
  /** 删除标志1正常0删除 */
 private Integer isactive;
   @TableField("network_type")
  /**  */
 private Integer networkType;
   @TableField("update_time")
  /**  */
 private Date updateTime;
   @TableField("create_time")
  /** 创建时间 */
 private Date createTime;
   @TableField("sub_type")
  /** 数据来源0:系统内,1+:其他系统 */
 private String subType;
   @TableField("org_parent_id")
  /** tob企业id，与tob系统对接 */
 private Integer orgParentId;

    @TableField("is_test")
    /** 是否测试账号：0否，1是 */
    private Byte isTest;

    @TableField("account_type")
    /** 用户卡类型：1app账号，2物联卡 */
    private Integer accountType;

    @TableField("expire_time")
    /** 到期时间 */
    private Date expireTime;

    @TableField("is_useable")
    /** 是否禁用：0否，1是 */
    private Byte isUseable;

    @TableField("service_month")
    /** 会员期限（单位：天）*/
    private Integer serviceMonth;

    @TableField(exist = false)
    /** 用户类型：0免费用户，1会员 */
    private Integer serviceType;

    @TableField(exist = false)
    /** 账号到期状态：0已过期，1未到期  */
    private Integer expireEndStatus;


    @TableField("User_OldServiceBeginTime")
    /** 帐户原始使用期限（起始日期） */
    private Date userOldServicebegintime;
    @TableField("User_OldServiceEndTime")
    /** 帐户原始使用期限（截至日期） */
    private Date userOldServiceendtime;

    @TableField("User_banned_time")
    /** 用户禁用时间 */
    private Date userBannedTime;
    @TableField("User_banned_length")
    /** 用户禁用时长(小时) */
    private Integer userBannedLength;

    /** 测试账号创建时间 */
    @TableField("test_create_time")
    private Date testCreateTime;
}
