package com.shanli.operate.domain.echat;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("tb_chatgroup")
public class ChatGroupDO {

    @TableId(type= IdType.AUTO)
    private Long Cg_ID;

    @TableField("Cg_Name")
    private String cgName;

    @TableField("Cg_ComID")
    private Integer comid;

    @TableField("Cg_Creator")
    private Integer creator;

    @TableField("Cg_CreatorType")
    private Integer creatorType;

    @TableField("Cg_Type")
    private Integer cgType;

    @TableField("Cg_Prior")
    private Integer prior;

    @TableField("Cg_Speech_Limit_Second")
    private Integer speechLimitSecond;

    @TableField("isActive")
    private Integer isActive;

    @TableField("create_time")
    private Date createTime;

    @TableField("Cg_speech_status")
    private Integer speechStatus;

    @TableField("Cg_banned_time")
    private Date cgBannedTime;

    @TableField("Cg_banned_length")
    private Integer cgBannedLength;
}