package com.shanli.operate.domain.echat;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("tb_userofgroup")
public class UserOfGroupDo {
    @TableId(type= IdType.AUTO)
    private Integer UOG_autoid;

    //群组ID
    @TableField("UOG_Cgid")
    private Long Cgid;

    //用户ID
    @TableField("UOG_UserId")
    private Integer UserId;

    //用户所在群组中的优先级 (1：普通 2：中级 3：高级)
    @TableField("UOG_Priority")
    private Integer Priority;

    //用户自定义在组名称
    @TableField("UOG_InGroupName")
    private String InGroupName;

    //删除标志1正常0删除
    @TableField("isActive")
    private Integer isActive;

    //修改时间
    @TableField("update_time")
    private Date updateTime;

    //最后一次进组时间
    @TableField("last_join")
    private Date lastJoin;

    //创建时间
    @TableField("create_time")
    private Date createTime;

}
