package com.shanli.operate.domain.cm;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("tb_orderinfo")
public class OrderinfoDO {
    //订单号
    private String orderNumber;
    //订单来源
    private String orderSourceID;
    //客户编码
    private String customerNumber;
    //产品订单数量
    private String productOrderSum;
    //产品包订单号
    private String productOrderNumber;
    //产品订购关系操作类型
    private String subOprType;
    //产品编码
    private String productCode;
    //产品订购关系ID
    private String productOrderID;
    //产品订购生效时间
    private String effTime;
    //产品订购截止时间
    private String expTime;
    //产品计费开始时间
    private String billEffTime;
    //产品属性信息
    private String character;
    //产品包资费
    private String productOrderRatePlan;
    //提交人名称
    private String operator;
    //提交人电话
    private String operatorPhone;
    //紧急程度
    private String emergencyDegree;
    //操作时间
    private String oprTime;
    //提交省代码
    private String province;
    //状态：1.订单创建 2.支付失败 3.支付成功 4.结果反馈 5.归档成功',
    private String status;
    //3.支付成功',
    @TableField("pay_status")
    private String payStatus;
    //订单通知状态
    @TableField("eboss_status")
    private String ebossStatus;
    //订单通知信息
    @TableField("error_msg")
    private String errorMsg;
    //支付平台流水号
    @TableField("trade_no")
    private String tradeNo;
    //平台流水号
    @TableField("platform_flow_no")
    private String platformFlowNo;
    //资费类型:1) 3元/月, 2) 8元/季度, 3) 15元/半年, 4) 30元/年',
    @TableField("service_type")
    private String serviceType;
    //交易时间
    @TableField("trans_time")
    private String transTime;
}
