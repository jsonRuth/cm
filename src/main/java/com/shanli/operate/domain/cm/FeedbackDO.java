package com.shanli.operate.domain.cm;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("tb_feedback")
public class FeedbackDO {
    @TableId(type= IdType.AUTO)
    private Integer id;

    @TableField("user_account")
    private String userAccount;

    @TableField("content")
    private String content;

    @TableField("create_time")
    private Date createTime;

    @TableField("read_status")
    private Integer readStatus;

    @TableField("reply_content")
    private String replyContent;

    @TableField("device_id")
    private String deviveId;

    @TableField("manufacturer")
    private String manufacturer;

    @TableField("model")
    private String model;

    @TableField("os")
    private String os;

    @TableField("os_version")
    private String osVersion;

    @TableField("app_version")
    private String appVersion;

    @TableField("reply_account")
    private String replyAccount;

    @TableField("recovery_time")
    private Date recoveryTime;

}