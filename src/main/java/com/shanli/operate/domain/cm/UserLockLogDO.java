package com.shanli.operate.domain.cm;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

/**
 * 用户锁定日志表
 **/
@Data
@TableName("tb_userlocklog")
public class UserLockLogDO {

    @TableId(type= IdType.AUTO)
    private Integer ullId;

    @TableField("user_phone")
    private String userPhone;

    @TableField("lock_type")
    private Integer lockType;

    @TableField("lock_source")
    private Integer lockSource;

    @TableField("create_time")
    private Date createTime;


}