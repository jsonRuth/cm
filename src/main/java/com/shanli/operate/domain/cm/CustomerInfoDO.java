package com.shanli.operate.domain.cm;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

@Data
@TableName("tb_customerinfo")
public class CustomerInfoDO {
    @TableId(type= IdType.AUTO)
    private Long id;

    private String ebossnumber;

    private Byte oprtype;

    private String custtype;

    private String customernumber;

    private String customername;

    private String companyid;

    private String loginname;

    private String nationid;

    private String province;

    private String city;

    private String ictype;

    private String icnumber;

    private String phone;

    private String email;

    private String fax;

    private String address;

    private String zipcode;

    private String web;

    private String gender;

    private String nationality;

    private String marriedstatus;

    private String education;

    private String occupation;

    private String salary;

    private String nickname;

    private String attribute;
    private String oprtime;


    private String notes;

    private String subtype;

    private Byte status; //状态, 1:有效, 0:注销

    private String userconfig;


}