package com.shanli.operate.domain.cm;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.shanli.common.base.BaseDO2;
import lombok.Data;

import java.util.Date;

@Data
@SuppressWarnings("serial")
@TableName("tb_importaccount")
public class ImportAccountDO extends BaseDO2 {
    @TableId(type= IdType.AUTO)
    private Integer id;
    @TableField("account")
    /** 账号 */
    private String account;
    @TableField("account_source")
    /** 账号来源：1网上商城 */
    private Integer accountSource;
    @TableField("fee_type")
    /** 功能费类型：1年卡会员-网上商城版 */
    private Integer feeType;
    @TableField("buy_count")
    /** 购买数量 */
    private Integer buyCount;
    @TableField("account_status")
    /** 状态：0未使用,1已使用 */
    private Integer accountStatus;
    @TableField("create_time")
    /** 创建时间 */
    private Date createTime;
    @TableField("update_time")
    /** 更新时间 */
    private Date updateTime;
}

