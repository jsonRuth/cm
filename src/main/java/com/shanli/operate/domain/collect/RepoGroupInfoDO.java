package com.shanli.operate.domain.collect;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("repo_groupinfo")
public class RepoGroupInfoDO {

    @TableId(type= IdType.INPUT)
        private Long id;

    //群组名称
    @TableField("group_name")
    private String  groupName;

    //创建账号
    @TableField("create_user")
    private String  createUser;

    //群组用户数
    @TableField("group_users")
    private Integer  groupUsers;

    //群组类型：1.小于10人群组,2.10-30人,3.大于30人
    @TableField("group_type")
    private Integer  groupType;

    //对讲状态：0.禁言，1正常
    @TableField("speech_status")
    private Integer  speechStatus;

    //群组在线用户数
    @TableField("online_users")
    private Integer  onlineUsers;

    //群组离线用户数
    @TableField("offline_users")
    private Integer  offlineUsers;

    //创建时间
    @TableField("create_time")
    private Date  createTime;

    //更新时间
    @TableField("update_time")
    private Date updateTime;

    //删除标志1正常0删除
    @TableField("isActive")
    private Integer isActive;

    //是否禁言 0.禁言，1正常
    private Integer cgSpeechStatus;

    //是否是到达值类型
    private Integer arriveType;
}
