package com.shanli.operate.domain.collect;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

@Data
@TableName("tb_messagerecord")
public class MessageRecordDO {

    @TableId(type= IdType.AUTO)
    private Long id;

    @TableField("msg_type")
    private Byte msgType;

    @TableField("body")
    private String body;

    @TableField("msg_time")
    private Long msgTime;

    @TableField("sender")
    private Long sender;

    @TableField("receiver")
    private Long receiver;

    @TableField("receiver_type")
    private Byte receiverType;

}