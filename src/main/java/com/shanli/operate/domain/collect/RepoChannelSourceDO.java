package com.shanli.operate.domain.collect;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("repo_channelsource")
public class RepoChannelSourceDO {

    @TableId(type = IdType.AUTO)
    private Integer id;

    //归属省份
    @TableField("province")
    private String provinceId;

    //3.普通客户
    @TableField("three")
    private Integer three;

    //5.电渠用户
    @TableField("three")
    private Integer five;

    //10001.toB来源用户
    @TableField("to_b")
    private Integer toB;

    //10002.网上商城销售卡
    @TableField("ten_two")
    private Integer tenTwo;

    //10003.线下渠道
    @TableField("ten_three")
    private Integer tenThree;

    //用户类型：0免费，1会员
    @TableField("user_type")
    private Integer userType;

    //创建时间
    @TableField("create_time")
    private Date createTime;

    //更新时间
    @TableField("update_time")
    private Date  updateTime;

}
