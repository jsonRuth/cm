package com.shanli.operate.domain.collect;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("repo_feedback")
public class RepoFeedbackDO {

    @TableId(type= IdType.AUTO)
    private Integer id;

    @TableField("total_count")
    private Integer totalCount;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private String updateTime;
}
