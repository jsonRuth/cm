package com.shanli.operate.domain.collect;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@TableName("repo_userlogin")
@Data
public class RepoUserLoginLogDO {

    @TableId(type= IdType.AUTO)
    private Integer id;

    //用户id
    @TableField("user_id")
    private String  userId;

    //用户账号
    @TableField("user_account")
    private String  userAccount;

    //用户卡类型：1物联卡 2大王卡
    @TableField("card_type")
    private Integer  cardType;

    //归属省份
    @TableField("province")
    private String  provinceId;

    //归属城市
    @TableField("city")
    private String  cityId;

    //运营商名称：移动，联通，电信，其他
    @TableField("carrier")
    private String  carrier;

    //渠道来源：3.普通客户,5.电渠用户,10001.toB来源用户,10002.网上商城销售卡,10003.线下渠道
    @TableField("sub_type")
    private String  subType;

    //注册时间
    @TableField("register_time")
    private Date registerTime;

    //用户类型：0免费，1会员
    @TableField("user_type")
    private Integer  userType;

    //账号类型：1测试，0正式
    @TableField("is_test")
    private Byte  isTest;

    //登录时间
    @TableField("login_time")
    private Date  loginTime;

    //创建时间
    @TableField("create_time")
    private Date  createTime;

    //更新时间
    @TableField("update_time")
    private Date  updateTime;

}
