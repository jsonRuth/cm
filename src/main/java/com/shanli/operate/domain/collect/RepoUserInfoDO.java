package com.shanli.operate.domain.collect;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("repo_userinfo")
public class RepoUserInfoDO {

    @TableId(type= IdType.AUTO)
    private Integer id;

    //用户账号
    @TableField("user_account")
    private String  userAccount;

    //用户卡类型：1物联卡 2大王卡
    @TableField("card_type")
    private Integer  cardType;

    //用户状态：0离线，1在线
    @TableField("online_status")
    private Integer  onlineStatus;

    //归属省份
    @TableField("province")
    private String  provinceId;

    //归属城市
    @TableField("city")
    private String  cityId;

    //运营商名称：移动，联通，电信，其他
    @TableField("carrier")
    private String  carrier;

    //渠道来源：3.普通客户,5.电渠用户,10001.toB来源用户,10002.网上商城销售卡,10003.线下渠道
    @TableField("sub_type")
    private String  subType;

    //注册时间
    @TableField("register_time")
    private Date registerTime;

    //用户类型：0免费，1会员
    @TableField("user_type")
    private Integer  userType;

    //付费类型:1) 3元/月, 2) 8元/季度, 3) 15元/半年, 4) 30元/年
    @TableField("service_type")
    private Integer  serviceType;

    //付费时间
    @TableField("pay_time")
    private Date payTime;

    //账号状态：0未禁用，1禁用
    @TableField("account_status")
    private Byte  accountStatus;

    //账号类型：1测试，0正式
    @TableField("is_test")
    private Byte  isTest;

    //所属群组数
    @TableField("belong_group")
    private Integer  belongGroup;

    //登录时间
    @TableField("login_time")
    private Date  loginTime;

    //创建群组数
    @TableField("create_group")
    private Integer  createGroup;

    //网络类型，例如4G
    @TableField("network_type")
    private String  networkType;

    //IP
    @TableField("ip")
    private String  ip;

    //连续登录失败次数
    @TableField("login_fail")
    private Integer  loginFail;

    //风险等级
    @TableField("risk_level")
    private String  riskLevel;

    //敏感词次数
    @TableField("sensitive_count")
    private Integer  sensitiveCount;

    //设备ID
    @TableField("device_id")
    private String  deviceId;

    //设备品牌
    @TableField("manufacturer")
    private String  manufacturer;

    //设备品牌型号
    @TableField("model")
    private String  model;

    //设备系统
    @TableField("os")
    private String  os;

    //设备系统版本
    @TableField("os_version")
    private String  osVersion;

    //删除标志1正常0删除
    @TableField("isActive")
    private Integer isActive;

    //软件版本
    @TableField("app_version")
    private String  appVersion;

    //创建时间
    @TableField("create_time")
    private Date  createTime;

    //更新时间
    @TableField("update_time")
    private Date  updateTime;

    //是否禁用：0否，1是
    private Integer isUseable;

    //查询类型1：到达数，2：活跃数，3:新增数
    @TableField(exist = false)
    private Integer searchType;


}


