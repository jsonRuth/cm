package com.shanli.operate.domain.collect;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("repo_CommonLogin")
public class RepoCommonLoginDO {

    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("user_id")
    private Integer userId;

    @TableField("user_account")
    private String userAccount;

    @TableField("device_id")
    private String deviceId;

    @TableField("province")
    private String province;

    @TableField("local_create_time")
    private Date localCreateTime;

    @TableField("local_update_time")
    private Date localUpdateTime;

    @TableField("device_create_time")
    private Date deviceCreateTime;

    @TableField("device_update_time")
    private Date deviceUpdateTime;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    private String ip;

}
