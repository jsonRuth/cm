package com.shanli.operate.domain.collect;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("repo_statisuser")
public class RepoStatisUserDO {

    @TableId(type= IdType.AUTO)
    private Integer id;

    @TableField("total_count")
    private Integer totalCount;

    @TableField("data_type")
    private Integer dataType;

    @TableField("statis_type")
    private Integer statisType;

    @TableField("create_time")
    private Date createTime;


}
