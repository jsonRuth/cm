package com.shanli.demo.dao;

import com.shanli.demo.domain.DemoDO;
import com.shanli.common.base.BaseDao;

/**
 * 
 * <pre>
 * 基础表
 * </pre>
 * <small> 2018-07-27 23:38:24 | Aron</small>
 */
public interface DemoDao extends BaseDao<DemoDO> {

}
