package com.shanli.demo.service.impl;

import org.springframework.stereotype.Service;

import com.shanli.demo.dao.DemoDao;
import com.shanli.demo.domain.DemoDO;
import com.shanli.demo.service.DemoService;
import com.shanli.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 基础表
 * </pre>
 * <small> 2018-07-27 23:38:24 | Aron</small>
 */
@Service
public class DemoServiceImpl extends CoreServiceImpl<DemoDao, DemoDO> implements DemoService {

}
