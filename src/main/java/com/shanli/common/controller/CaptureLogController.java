package com.shanli.common.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.utils.Result;
import com.shanli.operate.domain.collect.RepoUserChargeDO;
import com.shanli.operate.domain.collect.RepoUserLoginLogDO;
import com.shanli.operate.service.collect.RepoUserChargeService;
import com.shanli.operate.service.collect.RepoUserLoginLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/common/captureLog")
public class CaptureLogController extends AdminBaseController {

    @Autowired
    private RepoUserChargeService userChargeService;

    @GetMapping()
    String log() {
        return  "common/log/captureLog";
    }


    @ResponseBody
    @GetMapping("/list")
    public Result<Page<RepoUserChargeDO>> list(RepoUserChargeDO userLoginLogDO) {
        Wrapper<RepoUserChargeDO> wrapper = new EntityWrapper<RepoUserChargeDO>().orderBy("pay_time", false);
        //账号
        if(!StringUtils.isEmpty(userLoginLogDO.getUserAccount())){
           wrapper.like("User_Account",userLoginLogDO.getUserAccount());
        }
        //用户卡类型
        if(userLoginLogDO.getCardType() > 0){
            wrapper.eq("card_type",userLoginLogDO.getCardType());
        }
        //归属省份
        if (!"-1".equals(userLoginLogDO.getProvinceId())){
            wrapper.eq("province", userLoginLogDO.getProvinceId());
        }
        //归属城市
        if (!"-1".equals(userLoginLogDO.getCityId())){
            wrapper.eq("city", userLoginLogDO.getCityId());
        }
        //运营商类型
        if ("1".equals(userLoginLogDO.getCarrier())){
            wrapper.eq("carrier", "移动");
        }
        if ("2".equals(userLoginLogDO.getCarrier())){
            wrapper.eq("carrier", "联通");
        }
        if ("3".equals(userLoginLogDO.getCarrier())){
            wrapper.eq("carrier", "电信");
        }
        if ("4".equals(userLoginLogDO.getCarrier())){
            wrapper.eq("carrier", "其他");
        }
        //渠道来源
        if (!"-1".equals(userLoginLogDO.getSubType())){
            wrapper.eq("sub_type", userLoginLogDO.getSubType());
        }
        //用户类型
        if (userLoginLogDO.getUserType()==0){
            //免费用户
            wrapper.eq("user_type",userLoginLogDO.getUserType());
        }if (userLoginLogDO.getUserType()==1){
            //会员用户
            wrapper.eq("user_type",userLoginLogDO.getUserType());
        }

        Page<RepoUserChargeDO> page = userChargeService.selectPage(getPage(RepoUserChargeDO.class), wrapper);

        return Result.ok(page);
    }

}
