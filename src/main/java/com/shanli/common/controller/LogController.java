package com.shanli.common.controller;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.shanli.common.annotation.Log;
import com.shanli.common.base.AdminBaseController;
import com.shanli.common.domain.LogDO;
import com.shanli.common.service.LogService;
import com.shanli.common.utils.Result;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * <pre>
 * 日志
 * </pre>
 * 
 * <small> 2018年3月23日 | Aron</small>
 */
@RequestMapping("/common/log")
@Controller
public class LogController extends AdminBaseController {
    @Autowired
    LogService logService;
    String prefix = "common/log";
    
    @GetMapping()
    String log() {
        return prefix + "/log";
    }

    @ResponseBody
    @GetMapping("/list")
    public Result<Page<LogDO>> list(LogDO logDTO,String beginTime,String endTime) {
    	EntityWrapper<LogDO> wrapper = (EntityWrapper<LogDO>) logService.convertToEntityWrapper().orderBy("gmtCreate", false);
        wrapper.ge(!StringUtils.isEmpty(beginTime),"gmtCreate", beginTime);
        wrapper.le(!StringUtils.isEmpty(endTime),"gmtCreate", endTime + " 23:59:59");
        if (logDTO.getIsTest()==1){
            wrapper.eq("is_test", logDTO.getIsTest());
        }if (logDTO.getIsTest()==2){
            wrapper.ne("is_test", 1);
        }
        wrapper.like("username", logDTO.getUsername());
        Page<LogDO> page = logService.selectPage(getPage(LogDO.class), wrapper);
        return Result.ok(page);
    }

    /**
     * 查看操作理由
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/checkReason/{id}")
    public String checkReason(@PathVariable("id")Integer id, Model model){
        LogDO logDO = logService.selectById(id);
        model.addAttribute("logDO",logDO);
        return "common/log/checkReason";

    }

    @GetMapping("/checkObject/{id}")
    public String checkObject(@PathVariable("id")Integer id, Model model){
        LogDO logDO = logService.selectById(id);
        String operateObject = logDO.getOperateObject();
        model.addAttribute("logDO",logDO);
        return "common/log/checkObject";

    }


    @Log("删除系统日志")
    @ResponseBody
    @PostMapping("/remove")
    Result<String> remove(Long id) {
        logService.deleteById(id);
        return Result.ok();
    }
    
    @Log("批量删除系统日志")
    @ResponseBody
    @PostMapping("/batchRemove")
    Result<String> batchRemove(@RequestParam("ids[]") Long[] ids) {
        logService.deleteBatchIds(Arrays.asList(ids));
        return Result.ok();
    }
}
