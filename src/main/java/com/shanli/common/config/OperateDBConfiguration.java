package com.shanli.common.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

/**
 * operate_manage 数据源
 */
@Configuration
@MapperScan(basePackages = {"com.shanli.api.dao","com.shanli.common.dao","com.shanli.demo.dao",
        "com.shanli.email.dao","com.shanli.generator.dao","com.shanli.job.dao",
        "com.shanli.oss.dao","com.shanli.sys.dao","com.shanli.wxmp.dao"}, sqlSessionFactoryRef = "sqlSessionFactory3")
public class OperateDBConfiguration {
    private Logger logger = LoggerFactory.getLogger(OperateDBConfiguration.class);


    @Bean(name="dataSource3")
    public DataSource dataSource(@Value("${mysql.driver}") String jdbcDriver,
                                 @Value("${mysql.operate.url}") String jdbcUrl,
                                 @Value("${mysql.username}") String jdbcUsername,
                                 @Value("${mysql.password}") String jdbcPassword) {
        DruidDataSource source = new DruidDataSource();
        source.setDriverClassName(jdbcDriver);
        source.setUrl(jdbcUrl);
        source.setUsername(jdbcUsername);
        source.setPassword(jdbcPassword);
        //source.setInitialSize(10);
        //source.setMinIdle(10);
        //source.setMaxActive(100);
        //source.setMaxWait(60*1000);
        source.setTimeBetweenEvictionRunsMillis(60*1000);
        source.setMinEvictableIdleTimeMillis(300000);
        source.setValidationQuery("select 1");
        source.setTestWhileIdle(true);
        source.setTestOnBorrow(false);
        source.setTestOnReturn(false);
        source.setPoolPreparedStatements(true);
        source.setMaxPoolPreparedStatementPerConnectionSize(20);
        source.setKeepAlive(true);
        try {
            source.setFilters("stat,wall");
            source.addConnectionProperty("druid.stat.mergeSql", "true");
            source.addConnectionProperty("druid.stat.slowSqlMillis", "1000");
            source.init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return source;
    }

    @Bean
    public DataSourceTransactionManager transactionManager3(@Qualifier("dataSource3")DataSource dataSource) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource);
        return transactionManager;
    }

    @Bean(name="sqlSessionFactory3")
    public FactoryBean<SqlSessionFactory> sqlSessionFactory(@Qualifier("dataSource3")DataSource source) throws IOException {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(source);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactory.setMapperLocations(resolver.getResources("classpath:mapper/**/*Mapper.xml"));

        //添加拦截器（分页插件）
        sqlSessionFactory.setPlugins(new Interceptor[]{new PaginationInterceptor()});
        return sqlSessionFactory;
    }

}
