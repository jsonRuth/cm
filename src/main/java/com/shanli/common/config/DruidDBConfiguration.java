package com.shanli.common.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

/**
 * cm数据库
 */
@Configuration
@MapperScan(basePackages = {"com.shanli.operate.dao.cm"}, sqlSessionFactoryRef = "sqlSessionFactory1")
public class DruidDBConfiguration {
    private Logger logger = LoggerFactory.getLogger(DruidDBConfiguration.class);


    @Bean(name="dataSource1")
    @Primary // 在同样的DataSource中，首先使用被标注的DataSource
    public DataSource dataSource(@Value("${mysql.driver}") String jdbcDriver,
                                 @Value("${mysql.cm.url}") String jdbcUrl,
                                 @Value("${mysql.username}") String jdbcUsername,
                                 @Value("${mysql.password}") String jdbcPassword) {
        DruidDataSource source = new DruidDataSource();
        source.setDriverClassName(jdbcDriver);
        source.setUrl(jdbcUrl);
        source.setUsername(jdbcUsername);
        source.setPassword(jdbcPassword);
        //source.setInitialSize(10);
        //source.setMinIdle(10);
        //source.setMaxActive(100);
        //source.setMaxWait(60*1000);
        source.setTimeBetweenEvictionRunsMillis(60*1000);
        source.setMinEvictableIdleTimeMillis(300000);
        source.setValidationQuery("select 1");
        source.setTestWhileIdle(true);
        source.setTestOnBorrow(false);
        source.setTestOnReturn(false);
        source.setPoolPreparedStatements(true);
        source.setMaxPoolPreparedStatementPerConnectionSize(20);
        source.setKeepAlive(true);
        try {
            source.setFilters("stat,wall");
            source.addConnectionProperty("druid.stat.mergeSql", "true");
            source.addConnectionProperty("druid.stat.slowSqlMillis", "1000");
            source.init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return source;
    }

    @Bean
    @Primary
    public DataSourceTransactionManager transactionManager1(@Qualifier("dataSource1")DataSource dataSource) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource);
        return transactionManager;
    }

    @Bean(name="sqlSessionFactory1")
    @Primary
    public FactoryBean<SqlSessionFactory> sqlSessionFactory(@Qualifier("dataSource1")DataSource source) throws IOException {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(source);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactory.setMapperLocations(resolver.getResources("classpath:mapper/**/*Mapper.xml"));

        //添加拦截器（分页插件）
        sqlSessionFactory.setPlugins(new Interceptor[]{new PaginationInterceptor()});
        return sqlSessionFactory;
    }

    @Bean
    public ServletRegistrationBean druidServlet() {
        ServletRegistrationBean reg = new ServletRegistrationBean();
        reg.setServlet(new StatViewServlet());
        reg.addUrlMappings("/druid/*");
//        reg.addInitParameter("allow", ""); // 白名单
        reg.addInitParameter("deny", "");
        reg.addInitParameter("loginUsername", "admin");
        reg.addInitParameter("loginPassword", "shanli@2019");
        return reg;
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico");
        filterRegistrationBean.addInitParameter("profileEnable", "true");
        filterRegistrationBean.addInitParameter("principalCookieName", "USER_COOKIE");
        filterRegistrationBean.addInitParameter("principalSessionName", "USER_SESSION");
//        filterRegistrationBean.addInitParameter("DruidWebStatFilter", "/*");
        return filterRegistrationBean;
    }
}
