package com.shanli.common.service;

import org.springframework.stereotype.Service;

import com.shanli.common.base.CoreService;
import com.shanli.common.domain.LogDO;

/**
 * <pre>
 * </pre>
 * <small> 2018年3月22日 | Aron</small>
 */
@Service
public interface LogService extends CoreService<LogDO> {
    
}
