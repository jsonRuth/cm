package com.shanli.common.service.impl;

import org.springframework.stereotype.Service;

import com.shanli.common.base.CoreServiceImpl;
import com.shanli.common.dao.LogDao;
import com.shanli.common.domain.LogDO;
import com.shanli.common.service.LogService;

/**
 * <pre>
 * </pre>
 * <small> 2018年3月22日 | Aron</small>
 */
@Service
public class LogServiceImpl extends CoreServiceImpl<LogDao, LogDO> implements LogService {

}
