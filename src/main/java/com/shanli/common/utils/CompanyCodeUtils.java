package com.shanli.common.utils;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * **
 * .*                   @
 * .*           @@@@     @@
 * .*        @@@    @@   @* @             /@/@@/@/@@/@/
 * .*      @@@       @@  @ * @           /@/  /@/  /@/
 * .*    @@         @@  @ * @           /@/  /@/  /@/
 * .*  @@          @@  @ * @           /@/  /@/  /@/
 * .* @@          @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 * .*  @@          @@  @ * @     @ @   \@\  \@\  \@\
 * .*    @@         @@  @ * @    @ @    \@\  \@\  \@\
 * .*      @@@       @@  @ * @  @ * @    \@\  \@\  \@\
 * .*        @@@    @@   @* @  @ * * @    \@\@@\@\@@\@\
 * .*           @@@@     @@    @@*@*@@
 * .*                   @        ***
 * **
 * create by zhangsong 2019/4/11
 */
@Component
public class CompanyCodeUtils {
    public static String COMPANYCODEPRE = "COMPANYCODEPRE";
    /**
     * id,areaname
     */
    public static HashMap<Integer, String> areaCode = new HashMap<>();
    /**
     * id,parentid
     */
    public static HashMap<Integer, Integer> idParent = new HashMap<>();

    /**
     * areaname,id
     */
    public static HashMap<String, Integer> province = new HashMap<>();

    /**
     * areaname,id
     */
    public static HashMap<String, Integer> city = new HashMap<>();

}
