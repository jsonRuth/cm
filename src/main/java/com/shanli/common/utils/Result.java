package com.shanli.common.utils;

/**
 * <pre>
 * 
 * 自定义响应结构
 * </pre>
 * 
 * @author Aron
 * @date 2017年5月9日
 */
public class Result<T> {

    public final static Integer CODE_SUCCESS = 0;
    public final static Integer CODE_FAIL = 1;
    public final static String MSG_SUCCESS = "操作成功";
    public final static String MSG_FAIL = "操作失败";

    // 响应业务状态 0 成功， 1失败
    private Integer code;

    // 响应消息
    private String msg;

    // 响应中的数据
    private T data;

    public static <T> Result<T> build(Integer status, String msg, T data) {
        return new Result<T>(status, msg, data);
    }

    public static <T> Result<T> ok(T data) {
        return new Result<T>(data);
    }

    public static <T> Result<T> ok() {
        return new Result<T>(CODE_SUCCESS, MSG_SUCCESS, null);
    }

    public static <T> Result<T> fail() {
        return new Result<T>(CODE_FAIL, MSG_FAIL, null);
    }

    public Result() {

    }

    public static <T> Result<T> build(Integer status, String msg) {
        return new Result<T>(status, msg, null);
    }

    public static <T> Result<T> build(EnumStatusCode code) {
        return new Result<T>(code.getCode(), code.getMsg(), null);
    }

    public static <T> Result<T> getResult(T t) {
        Result<T> result = new Result<>(t);
        return result;
    }

    public Result(Integer status, String msg, T data) {
        this.code = status;
        this.msg = msg;
        this.data = data;
    }

    public Result(T data) {
        this.code = 0;
        this.msg = MSG_SUCCESS;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public enum EnumStatusCode {
        USER_PWD_CHANGE_FIVE_REPEAT(4001, "连续5次更换的密码重复"),
        EXCEL_ERROR(4002, "上传excel异常"),
        IMPORTACCOUNT_DUPLICATE(4003, "录入账号中有重复账号，请修改后重试"),
        IMPORTACCOUNT_IMPORT_ERROR(4004, "导入异常"),
        IMPORTACCOUNT_OUT_RANGE(4005,"数量超过上限，请修改后重试"),
        IMPORTACCOUNT_INVALID(4006,"录入账号中存在无效号码，请修改后重试"),
        IMPORTACCOUNT_EXIST(4007, "录入账号中有已注册账号，请修改后重试"),
        ACCOUNT_IS_EMPTY(4008, "请导入有效的账号"),
        OLD_PASSWORD_ERROR(4009,"旧密码错误"),
        FORMAT_IS_WRONG(4010,"请导入正确的格式及数据"),
        ACCOUNT_SOURCE_ERROR(4011,"请导入有效的账号来源"),
        FUNCTION_FEE_TYPE_ERROR(4012,"请导入有效的功能费类型"),
        PURCHASE_QUANTITY_ERROR(4013,"请导入有效的购买数量"),
        IMPORTACCOUNT_DELETE_ERROR(4014,"账号已使用，删除失败"),
        MEMBERSHIP_EXPIRY_DATE_ERROR(4015,"请导入1-1080天的会员有效期"),
        DUE_TIME_ERROR(4016,"账号到期时间小于今天，请修改后重试"),

        USER_PWD_CHANGE_INCLUDE_REPEAT(4017, "密码不能包含用户名"),
        MOBILE_PHONE_ERROR(4018, "请输入正确的手机号"),
        SEND_VERIFICATION_CODE(4019, "发送验证码失败！"),
        MOBILE_PHONE_TO_REPEAT(4020, "手机号已绑定，请重新输入！"),
        CODE_ERROR(4021, "验证码错误！"),
        MOBILE_REPEAT_ERROR(4022, "手机绑定失败！"),
        MOBILE_CODE__ERROR(4023, "该手机号当日获取验证码次数已用完！"),
        GROUP_NO_USERS_ERROR(4024, "该群组目前没有用户"),
        USERS_NO_GROUP_ERROR(4025, "该用户目前没有群组"),
        WRONG_ACCOUNT_FORMAT(4026,"录入账号格式有误，请修改后重试"),
        EXPIRETIME_IS_EMPTY(5001,"账号到期时间不能为空"),
        EXPIRETIME_ERROR(5002,"账号到期时间格式异常"),
        USER_INFO_ERROR(5003,"用户信息异常"),
        DUE_TIME_ERROR_MEMBER(5004,"会员到期时间小于今天，请修改后重试"),
        DUE_TIME_ERROR_COMPARE(5005,"账号到期时间小于会员到期时间，请修改后重试"),
        ;


        private int code;
        private String msg;

        public int getCode() {
            return code;
        }

        public String getCodeStr() {
            return code + "";
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public EnumStatusCode setMsg(String msg) {
            this.msg = msg;
            return this;
        }

        EnumStatusCode(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public static String getMsgByCode(int code) {
            EnumStatusCode[] values = EnumStatusCode.values();
            for (EnumStatusCode ec : values) {
                if (ec.code == code) {
                    return ec.msg;
                }
            }
            return "";
        }
    }
}
