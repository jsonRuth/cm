package com.shanli.common.base;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.Version;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@SuppressWarnings("serial")
public class BaseDO2 implements Serializable {
	/** 由mybatis-plus.global-config.sql-injector:com.baomidou.mybatisplus.mapper.LogicSqlInjector自动维护 */
	@TableLogic
	@TableField("is_delete")
	private boolean isDeleted;
	/** 由MyBatisConfig.optimisticLockerInterceptor自动维护 */
//	@Version
//	private int version;
	/** 由MySQL自动维护 */
	@TableField("create_time")
	private Date createTime;
	@TableField("update_time")
	private Date updateTime;
	/** 由LogAspect.logMapper自动维护 */
//	@TableField("create_by")
//	private Long createBy;
//	@TableField("update_by")
//	private Long updateBy;
}
