package com.shanli.common.shiro.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <pre>
 *
 * </pre>
 * <small> 2018/8/27 21:50 | Aron</small>
 */
@Component
@ConfigurationProperties(prefix = "shanli.shiro")
@Data
public class ShiroProperties {
    private String sessionKeyPrefix = "shanli:session";
    private String jsessionidKey = "SESSION";
}
