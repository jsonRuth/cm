package com.shanli.common.dao;

import com.shanli.common.base.BaseDao;
import com.shanli.common.domain.ConfigDO;

/**
 * 
 * 
 * @author Aron
 * @email izenglong@163.com
 * @date 2018-04-06 01:05:22
 */
public interface ConfigDao extends BaseDao<ConfigDO> {

}
