package com.shanli.common.dao;

import com.shanli.common.base.BaseDao;
import com.shanli.common.domain.LogDO;

/**
 * 系统日志
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 15:45:42
 */
public interface LogDao extends BaseDao<LogDO> {
}
