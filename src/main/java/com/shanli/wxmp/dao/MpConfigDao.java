package com.shanli.wxmp.dao;

import com.shanli.wxmp.domain.MpConfigDO;
import com.shanli.common.base.BaseDao;

/**
 * <pre>
 * 微信配置表
 * </pre>
 * <small> 2018-04-11 23:27:06 | Aron</small>
 */
public interface MpConfigDao extends BaseDao<MpConfigDO> {
    
}
