package com.shanli.wxmp.dao;

import com.shanli.wxmp.domain.MpFansDO;
import com.shanli.common.base.BaseDao;

/**
 * <pre>
 * 微信粉丝表
 * </pre>
 * <small> 2018-04-11 23:27:06 | Aron</small>
 */
public interface MpFansDao extends BaseDao<MpFansDO> {
    
}
