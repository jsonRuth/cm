package com.shanli.wxmp.service.impl;

import org.springframework.stereotype.Service;

import com.shanli.wxmp.dao.MpConfigDao;
import com.shanli.wxmp.domain.MpConfigDO;
import com.shanli.wxmp.service.MpConfigService;
import com.shanli.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 微信配置表
 * </pre>
 * <small> 2018-04-11 23:27:06 | Aron</small>
 */
@Service
public class MpConfigServiceImpl extends CoreServiceImpl<MpConfigDao, MpConfigDO> implements MpConfigService {

}
