package com.shanli.wxmp.service.impl;

import org.springframework.stereotype.Service;

import com.shanli.wxmp.dao.MpFansDao;
import com.shanli.wxmp.domain.MpFansDO;
import com.shanli.wxmp.service.MpFansService;
import com.shanli.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 微信粉丝表
 * </pre>
 * <small> 2018-04-11 23:27:06 | Aron</small>
 */
@Service
public class MpFansServiceImpl extends CoreServiceImpl<MpFansDao, MpFansDO> implements MpFansService {

}
