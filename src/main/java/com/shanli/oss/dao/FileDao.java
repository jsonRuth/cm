package com.shanli.oss.dao;

import com.shanli.common.base.BaseDao;
import com.shanli.oss.domain.FileDO;

/**
 * 文件上传
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 15:45:42
 */
public interface FileDao extends BaseDao<FileDO> {

}
