var prefix = '/operate/main';
$().ready(function () {
    //实时在线用户折线图
    onlineUserChart();
    //实时活跃群组折线图
    activeGroupChart();
    //群组昨日关键数据统计
    groupYtdStatis();
    //用户昨日关键数据统计
    userYtdStatis();
    //渠道来源数据统计
    userSourceChart();


});


//实时在线用户折线图
function onlineUserChart() {
    var myChart = echarts.init(document.getElementById('onlineUser'));
    //ajax请求数据
    $.ajax({
        url: prefix + "/onlineUser",
        type: "post",
        data: {},
        success: function (data) {
            var xData = data.xData;
            var yData = data.yData;
            var title = "实时在线用户数";
            var seriesName = "在线用户数";
            showLineEchart(myChart, title, seriesName, xData, yData);
        }
    });
}

//实时活跃群组折线图
function activeGroupChart() {
    var myChart = echarts.init(document.getElementById('activeGroup'));
    //ajax请求数据
    $.ajax({
        url: prefix + "/activeGroup",
        type: "post",
        data: {},
        success: function (data) {
            var xData = data.xData;
            var yData = data.yData;
            var title = "实时活跃群组数";
            var seriesName = "活跃群组数";
            showLineEchart(myChart, title, seriesName, xData, yData);
        }
    });
}

//折线图设置
function showLineEchart(myChart, title, seriesName, xData, yData) {
    var option = {
        title: {
            text: title
        },
        grid: {
            x: '9%',
            y: '10%',
            width: '89%',
            height: '70%',
            x2: '2%'
        },
        tooltip: {
            trigger: 'axis',//鼠标悬浮
            backgroundColor: '#FFF0AE',//设置背景颜色
            textStyle: {//设置字体颜色、大小
                color: '#000000',
                fontSize: '11'
            }
        },
        calculable: true,
        xAxis: [
            {
                type: 'category',
                splitLine: {
                    show: false  //是否显示分割线
                },
                boundaryGap: false,
                data: xData
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: seriesName,
                type: "line",//统计图类型
                symbol: 'emptyCircle',//折线拐点样式
                itemStyle: {
                    normal: {
                        color: '#00CACA',//折线拐点颜色
                        lineStyle: {
                            color: '#00CACA'//折线颜色
                        }
                    }
                },
                "data":yData
            }
        ]
    };
    myChart.setOption(option);
}

//群组昨日关键数据统计
function groupYtdStatis() {
    var myChart = echarts.init(document.getElementById('groupYtdStatis'));


    // ajax请求数据
    $.ajax({
        url: prefix + "/groupYtdStatis",
        type: "post",
        data: {},
        success: function (data) {
            var option = {
                title: {
                    text: '群组昨日关键数据统计'
                },
                grid: {
                    x: '13%',
                    width: '81%',
                    x2: '2%',
                    y2: '16%'
                },
                tooltip: {
                    trigger: 'axis',//鼠标悬浮
                    axisPointer: {
                        type: 'shadow',//鼠标悬浮时，坐标轴指示器样式
                        shadowStyle: {
                            color: 'rgba(150,150,150,0.2)'
                        },
                     triggerOn: 'click'

                    },
                    backgroundColor: '#FFF0AE',//设置背景颜色
                    textStyle: {//设置字体颜色、大小
                        color: '#000000',
                        fontSize: '11'
                    }
                },
                legend: {
                    data: ['全部群组', '小于10人群组', '10-30人群组', '大于30人群组'],
                    y: 'bottom'
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        data: ['到达值', '活跃值'],
                        triggerEvent: true
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '全部群组',
                        type: "bar",//统计图类型
                        itemStyle: {
                            normal: {
                                color: '#00CACA'//颜色
                            }
                        },
                        "data": data.yData1,
                    },
                    {
                        name: '小于10人群组',
                        type: "bar",//统计图类型
                        itemStyle: {
                            normal: {
                                color: '#DCB5FF'
                            }
                        },
                        "data": data.yData2
                    },
                    {
                        name: '10-30人群组',
                        type: "bar",//统计图类型
                        itemStyle: {
                            normal: {
                                color: '#66B3FF'
                            }
                        },
                        "data": data.yData3
                    },
                    {
                        name: '大于30人群组',
                        type: "bar",//统计图类型
                        itemStyle: {
                            normal: {
                                color: '#FF9D6F'
                            }
                        },
                        "data": data.yData4
                    }

                ]

            };
            myChart.setOption(option);
        }
    });
    //
   /* myChart.on('click', function (params) {
        if (params.componentType === 'markPoint') {
            alert("markPoint");
        }


            if(params.componentType === 'series'){

               if(params.name == '到达值'){
                    groupDataStatistics(1);
                }else{
                  groupDataStatistics(0);
                }


            }
    });*/

    myChart.getZr().on('click',function(params){
        /*var x = params.offsetX;
        var y = params.offsetY;
        if(y>=61 && y<=335){
            if(x>=70 && x<=285 ){
                groupDataStatistics(1);
            }else if(x>=288 && x<=503 ){
                groupDataStatistics(0);
            }
        }*/
        var pointInPixel= [params.offsetX, params.offsetY];
        if(myChart.containPixel('grid',pointInPixel)){
            var xIndex=myChart.convertFromPixel({seriesIndex:0},[params.offsetX, params.offsetY])[0];
           if(xIndex == 0){
               groupDataStatistics(1);
           }else if(xIndex == 1){
               groupDataStatistics(0);
           }
        }
    });


}

//群组到达值的详情表
function groupDataStatistics(arriveType){

    var title;
    var url;
    if(arriveType ==1){
        title = "群组到达详情";
        url = "/operate/groupStatistics";
    }else{
        title = "群组活跃详情";
         url = "/operate/groupStatistics/groupStatisticsActive";
    }


    openTabPage(url,title);
}





//用户昨日关键数据统计
function userYtdStatis() {
    var myChart = echarts.init(document.getElementById('userYtdStatis'));
    //ajax请求数据
    $.ajax({
        url: prefix + "/userYtdStatis",
        type: "post",
        data: {},
        success: function (data) {
            var option = {
                title: {
                    text: '用户昨日关键数据统计'
                },
                grid: {
                    x: '14%',
                    width: '80%',
                    x2: '2%',
                    y2: '16%'
                },
                tooltip: {
                    trigger: 'axis',//鼠标悬浮
                    axisPointer: {
                        type: 'shadow',//鼠标悬浮时，坐标轴指示器样式
                        shadowStyle: {
                            color: 'rgba(150,150,150,0.2)'
                        },
                    triggerOn: 'click'
                    },
                    backgroundColor: '#FFF0AE',//设置背景颜色
                    textStyle: {//设置字体颜色、大小
                        color: '#000000',
                        fontSize: '11'
                    }
                },
                legend: {
                    data: ['总用户', '免费用户', '会员用户'],
                    y: 'bottom'
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        data: ['到达数量', '活跃数量', '新增数量']
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '总用户',
                        type: "bar",//统计图类型
                        itemStyle: {
                            normal: {
                                color: '#00CACA'//颜色
                            }
                        },
                        "data": data.yData1
                    },
                    {
                        name: '免费用户',
                        type: "bar",//统计图类型
                        itemStyle: {
                            normal: {
                                color: '#DCB5FF'
                            }
                        },
                        "data": data.yData2
                    },
                    {
                        name: '会员用户',
                        type: "bar",//统计图类型
                        itemStyle: {
                            normal: {
                                color: '#66B3FF'
                            }
                        },
                        "data": data.yData3
                    }
                ]
            };
            myChart.setOption(option);
        }
    });

 /*   myChart.on('click', function (params) {


        if(params.componentType === 'series'){

            if(params.name == '到达数量'){
                userDataStatistics(1);
            }else if(params.name == '活跃数量'){
                userDataStatistics(2);
            }else{  //新增数量
                userDataStatistics(3);
            }
        }
    });*/


    myChart.getZr().on('click',function(params){
     /*   var x = params.offsetX;
        var y = params.offsetY;
        if(y>=61 && y<=335){
            if(x>=76 && x<=215){
                userDataStatistics(1)
            }else if(x>=218 && x<=360 ){
                userDataStatistics(2);
            } else if (x>=362 && x<=503){
                userDataStatistics(3);
            }
        }*/

        var pointInPixel= [params.offsetX, params.offsetY];
        if(myChart.containPixel('grid',pointInPixel)) {
            var xIndex = myChart.convertFromPixel({seriesIndex: 0}, [params.offsetX, params.offsetY])[0];

            if(xIndex == 0){
                userDataStatistics(1)
            }else if(xIndex == 1){
                userDataStatistics(2);
            }else if(xIndex == 2){
                userDataStatistics(3);
            }
        }
    });
}

//用户昨日关键数据统计
function userDataStatistics(searchType){
    var title;
    var url;
    if(searchType == 1){
        title = "用户到达详情";
        url = "/operate/main/userStatisticsArrive?searchType="+searchType;
    }else if(searchType == 2){
        title = "用户活跃详情";
        url = "/operate/main/userStatisticsActive?searchType="+searchType;
    }else{
        title = "用户新增详情";
        url = "/operate/main/userStatisticsNewly?searchType="+searchType;
    }

    openTabPage(url,title);

};





//渠道来源数据统计
function userSourceChart() {
    var myChart = echarts.init(document.getElementById('userSourceChart'));
    // ajax请求数据
    $.ajax({
        url: prefix + "/userSourceChart",
        type: "post",
        data: {},
        success: function (data) {
            var arr = [];
            for (var i = 0; i <= data.nameList.length; i++) {
                arr.push({
                    name: data.nameList[i],
                    value: data.valueList[i]
                });
            }
            var option = {
                title: {
                    text: '渠道来源数据统计'
                },
                tooltip: {
                    trigger: 'item',//鼠标悬浮
                    backgroundColor: '#FFF0AE',//设置背景颜色
                    formatter: "{b} : {c} ({d}%)",
                    textStyle: {//设置字体颜色、大小
                        color: '#000000',
                        fontSize: '11'
                    }
                },
                legend: {
                    data: ['普通用户', '电渠用户', 'toB来源用户', '网上商城销售卡', '线下渠道'],
                    y: 'bottom'
                },
                calculable: true,
                series: [
                    {
                        type: "pie",//统计图类型
                        radius: '55%',
                        // roseType: 'radius',//南丁格尔玫瑰图模式
                        label: {
                            color: 'rgba(0, 0, 0, 0.8)',//文本颜色
                            formatter: '{d}%'//文本显示百分号数据
                        },
                        color: ['#FF9D6F', '#DCB5FF', '#00CACA', '#66B3FF', '#FF7575'],
                        data: arr
                    }
                ]
            };
            myChart.setOption(option);
            myChart.on("click", pieConsole);
        }
    });
}


function pieConsole() {
    var url = "/operate/channelSource"
    openTabPage(url,"渠道来源")

}

function feedAccountList(){
    var url = "/operate/feedAccount";
    openTabPage(url,"意见反馈详情");
}

function loginFailLock(){
    var url = "/operate/loginFailAccount";
    openTabPage(url,"连续登录失败详情");
}

function openTabPage(url, title) {
    var wpd = $(window.parent.document);
    var mainContent = wpd.find('.J_mainContent');
    var thisIframe = mainContent.find("iframe[data-id='"+ url +"']");
    var pageTabs = wpd.find('.J_menuTabs .page-tabs-content ');
    pageTabs.find(".J_menuTab.active").removeClass("active");
    mainContent.find("iframe").css("display", "none");
    if(thisIframe.length > 0){ // 选项卡已打开
        thisIframe.css("display", "inline");
        pageTabs.find(".J_menuTab[data-id='"+ url +"']").addClass("active");
    }else{
        var menuItem = wpd.find("a.J_menuItem[href='"+ url +"']");
        var dataIndex = title == undefined ? menuItem.attr("data-index") : '9999';
        var _title = title == undefined ? menuItem.find('.nav-label').text() : title;
        var iframe = '<iframe class="J_iframe" name="iframe'+ dataIndex +'" width="100%" height="100%" src="' + url + '" frameborder="0" data-id="' + url
            + '" seamless="" style="display: inline;"></iframe>';
        pageTabs.append(
            '<a href="javascript:;" class="J_menuTab active" data-id="'+url+'">' + _title + ' <i class="fa fa-times-circle"></i></a>');
        mainContent.append(iframe);
        //显示loading提示
        // var loading = top.layer.load();
        // mainContent.find('iframe:visible').load(function () {
        //     //iframe加载完成后隐藏loading提示
        //     top.layer.close(loading);
        // });
    }

}