$().ready(function() {
	validateRule();
    $("#password").val(userPassword(8))
});
var adIds = "";
var status = "";
$.validator.setDefaults({

	submitHandler : function() {
		save();
	}
});
function getCheckedRoles() {

	$("input:checkbox[name=role]:checked").each(function(i) {
		if (0 == i) {
			adIds = $(this).val();
		} else {
			adIds += ("," + $(this).val());
		}
	});
	return adIds;
}

function save() {
    status = $("input[name='status']:checked").val();
	if (status == "undefined"){
        parent.layer.msg("请选择状态");
        return false;
	}
	$("#roleIds").val(getCheckedRoles());
    if(adIds == ''){
        parent.layer.msg("请选择角色");
        return false;
    }
	$.ajax({
		cache : true,
		type : "POST",
		url : "/sys/user/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}

function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			name : {
				required : true,
                isFormat:true
			},
			username : {
                required : true ,
				remote : {
					url : "/sys/user/exist", // 后台处理程序
					type : "post", // 数据发送方式
					dataType : "json", // 接受数据格式
					data : { // 要传递的数据
						username : function() {
							return $("#username").val();
						}
					}
				}
			},
            deptName:{
                required : true
			},
			email : {
				required : true,
				email : true
			},
			topic : {
				required : "#newsletter:checked",
				minlength : 2
			},
			agree : "required"
		},
		messages : {
			name : {
				required : icon + "请输入姓名",
                isFormat: icon + "请输入必须是中文或者数字或者字母"
			},
			username : {
				required : icon + "请输入您的用户名",
				minlength : icon + "用户名必须两个字符以上",
				remote : icon + "用户名已经存在"
			},
			email : icon + "请输入您的E-mail"
		}
	})
}

var openDept = function(){
	layer.open({
		type:2,
		title:"选择部门",
		area : [ '300px', '450px' ],
		content:"/sys/dept/treeView"
	})
};

//随机生成8位数密码数字加字母
function userPassword(len) {
    charSet ='0123456789abcdefghijklmnopqrstuvwxyz~!@#$^?*()|{}:;,.<>/-_';
    var password = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        password += charSet.substring(randomPoz,randomPoz+1);
    }
    var regNumber = /\d+/;
    var regString = /[a-zA-Z]+/;
    var reg=/[~!@#$^?*()|{}:;,.<>/_]/gi;
    if(regNumber.test(password) && regString.test(password) && reg.test(password)){
        $("#password").val(password);
    }else{
        userPassword(8);
    }
    return password;
}

function loadDept( deptId,deptName){
	$("#deptId").val(deptId);
	$("#deptName").val(deptName);

}