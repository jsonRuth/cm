var prefix = "/sys/user"
$(function () {
    laydate({
        elem : '#birth'
    });
    var maxCount = 200;  // 最高字数，这个值可以自己配置
    $("#updateReason").on('keyup', function () {
        var len = getStrLength(this.value);
        if(len > maxCount){
            len = maxCount;
        }
        $("#count").html( len + "/200");
    })
});
/**
 * 基本信息提交
 */
$("#base_save").click(function () {
    var hobbyStr = getHobbyStr();
    $("#hobby").val(hobbyStr);
    if($("#basicInfoForm").valid()){
            $.ajax({
                cache : true,
                type : "POST",
                url :"/sys/user/updatePeronal",
                data : $('#basicInfoForm').serialize(),
                async : false,
                error : function(request) {
                    laryer.alert("Connection error");
                },
                success : function(data) {
                    if (data.code == 0) {
                        parent.layer.msg("更新成功");
                    } else {
                        parent.layer.alert(data.msg)
                    }
                }
            });
        }

});
$("#pwd_save").click(function () {
    if($("#modifyPwd").valid()){
        $.ajax({
            cache : true,
            type : "POST",
            url :"/sys/user/resetPwd",
            data : $('#modifyPwd').serialize(),
            async : false,
            error : function(request) {
                parent.laryer.alert("Connection error");
            },
            success : function(data) {
                if (data.code == 0) {
                    // parent.layer.msg("更新密码成功");
                    parent.layer.alert("更新密码成功");
                    window.setTimeout("window.location.href=\"/logout\"",1000);
                    $("#photo_info").click();
                } else {
                    parent.layer.alert(data.msg)
                }
            }
        });
    }
});
function getHobbyStr(){
    var hobbyStr ="";
    $(".hobby").each(function () {
        if($(this).is(":checked")){
            hobbyStr+=$(this).val()+";";
        }
    });
   return hobbyStr;
}

// 中文字符判断
function getStrLength(str) {
    var len = str.length;
    var reLen = 0;
    for (var i = 0; i < len; i++) {
        if (str.charCodeAt(i) < 27 || str.charCodeAt(i) > 126) {
            // 全角
            //reLen += 2;
            reLen++;
        } else {
            reLen++;
        }
    }
    return reLen;
}

//取消按钮
function cancelOption() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}