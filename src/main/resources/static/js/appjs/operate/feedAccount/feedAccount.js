var prefix = "/operate/feedAccount";
$(function () {
    load();
});

function load() {
    $('#exampleTable')
        .bootstrapTable(
            {
                method: 'get', // 服务器数据的请求方式 get or post
                url: prefix + "/list", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize: 'outline',
                toolbar: '#exampleToolbar',
                striped: true, // 设置为true会有隔行变色效果
                dataType: "json", // 服务器返回的数据类型
                pagination: true, // 设置为true会在底部显示分页条
                singleSelect: false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize: 10, // 如果设置了分页，每页数据条数
                pageNumber: 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns: false, // 是否显示内容下拉框（选择显示的列）
                sidePagination: "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParamsType: "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams: function (params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber: params.pageNumber,
                        pageSize: params.pageSize,
                        beginTime: $('#beginTime').val(),
                        endTime: $('#endTime').val(),
                        userAccount: $('#userAccount').val(),
                        readStatus:$("#readStatus").val(),
                        appVersion: $('#appVersion').val(),
                        deviveId: $('#deviveId').val(),
                        manufacturer: $('#manufacturer').val(),
                        model: $('#model').val(),
                        osVersion: $('#osVersion').val()
                    };
                },
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                responseHandler: function (res) {
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns: [
                    {
                        checkbox: true,
                        formatter: function(value,row,index){
                            if(row.accountType ==2){
                                return {disabled:true}
                            }
                        }
                    },
                    {
                        field: 'id',
                        title: 'ID',
                        visible:false
                    },
                    {
                        field: 'userAccount',
                        title: '反馈账号',
                        formatter:function geTel(value){
                            var reg = /^(\d{3})\d{4}(\d{4})|(\d{6})$/;
                            return value.replace(reg, "$1****$2");
                        }
                    },
                    {
                        field: 'createTime',
                        title: '反馈时间',
                        formatter: function (value) {
                            if (value != null) {
                                value=value.replace(new RegExp(/-/gm) ,"/");
                                var date = new Date(value);
                                var result = date.getFullYear() + "-";
                                if (date.getMonth() < 9) {
                                    result = result + "0";
                                }result = result + (date.getMonth() + 1) + "-";

                                if (date.getDate() <= 9) {
                                    result = result + "0";
                                }result = result + date.getDate()+" ";

                                if (date.getHours()<=9){
                                    result = result + "0";
                                }
                                result = result + date.getHours()+":";
                                if (date.getMinutes()<=9){
                                    result = result + "0";
                                }
                                result = result + date.getMinutes()+":";
                                if (date.getSeconds()<=9){
                                    result = result + "0";
                                }result = result + date.getSeconds();

                                return result;

                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: 'deviveId',
                        title: '设备ID'
                    },
                    {
                        field: 'manufacturer',
                        title: '设备品牌'
                    },
                    {
                        field: 'model',
                        title: '设备品牌型号'
                    },
                    {
                        field: 'osVersion',
                        title: '设备系统版本'
                    },
                    {
                        field: 'appVersion',
                        title: '软件版本'
                    },
                    {
                        field: 'content',
                        title: '意见反馈',
                        width: 150,
                        class:'colStyle',
                        formatter: paramsMatter

                    },
                    {
                        field: 'readStatus',
                        title: '状态',
                        formatter: function (value) {
                            if (value == 1) {
                                return "已回复";
                            } else if (value == 0) {
                                return "未回复";
                            }
                            else if (value == 2) {
                                return "无需回复";
                            }
                            else if (value == 3) {
                                return "其他";
                            }
                        }
                    },
                    {
                        title: '操作',
                        //field: 'id',
                        align: 'center',
                        formatter: function (value, row) {
                            var e = '<a class="btn btn-primary btn-sm" href="#" mce_href="#" type="text"  ' +
                                'onclick="edit(\'' + row.id
                                + '\')">查看</a> ';
                            var f = '<a class="btn btn-primary btn-sm" href="#" mce_href="#" type="text" ' +
                                'onclick="edit(\'' + row.id
                                + '\')">回复</a> ';
                            if(row.readStatus == 0){
                                return f
                            }else{
                                return e;
                            }
                        }
                    }]
            });
}

function reLoad() {
    $('#exampleTable').bootstrapTable('refresh');
}

function paramsMatter(value, row, index) {
    var values = row.content;
    var span=document.createElement('span');
    span.setAttribute('title',values);
    span.innerHTML = row.content;
    return span.outerHTML;
}

//查看
function edit(id) {
    layer.open({
        type: 2,
        title: '查看',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['800px', '450px'],
        content: prefix + '/edit/' + id // iframe的url
    });
}


function exportSelected() {
    var rows = $('#exampleTable').bootstrapTable('getSelections'); // 返回所有选择的行，当没有选择的记录时，返回一个空数组
    if (rows.length == 0) {
        layer.msg("请选择要导出的数据");
        return;
    }
    layer.confirm("确认要导出选中的'" + rows.length + "'条数据吗?", {
        btn: ['确定', '取消']
        // 按钮
    }, function () {
        var ids = "";
        // 遍历所有选择的行数据，取每条数据对应的ID
        $.each(rows, function (i, row) {
            if (i > 0) {
                ids += "&";
            }
            ids += "ids=" + row['id'];
        });

        window.open(prefix + "/exportSelected?" + ids);
        layer.msg("操作完成");
        return;
        // location.href = prefix + "/exportSelected?" + ids;
    }, function () {

    });
}

function exportAll() {
    layer.confirm("确认要导出全部数据吗?", {
        btn: ['确定', '取消']
        // 按钮
    }, function () {
        var param = "beginTime=" + $('#beginTime').val() + "&endTime=" + $('#endTime').val() +
            "&userAccount=" + $('#userAccount').val()+
            "&readStatus="+ $('#readStatus').val();
        window.open(prefix + "/exportAll?" + param);
        layer.msg("操作完成");
    }, function () {

    });
}






