var prefix = "/operate/feedAccount";

$().ready(function () {
    validateRule();
});

$.validator.setDefaults({
    submitHandler: function () {
        save();
    }
});

function typeclick(value) {
    var selectedOption=value.options[value.selectedIndex];
    if (selectedOption.value =="2") {
        var ele = document.getElementById("replyContent");
        ele.value = "无需回复";
    }else if(selectedOption.value =="3"){
        var ele = document.getElementById("replyContent");
        ele.value = "其他";
    }else {
        var ele = document.getElementById("replyContent");
        ele.value = "";
    }
}

function save() {
    var readStatus = $("#readStatus").val();
    console.log(readStatus)
    if (readStatus == -1){
        parent.layer.msg("请选择回复类型");
        return false;
    }
    $.ajax({
        cache: true,
        type: "POST",
        url: "/operate/feedAccount/updateReplyContent",
        data:
            // $('#signupForm').serialize(),// 你的formid
            {
                id: $('#id').val(),
                userAccount:$("#userAccount").val(),
                createTime: $('#createTime').val(),
                replyContent: $('#replyContent').val(),
                readStatus: $('#readStatus').val()
            }
        ,
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {
            if (data.code == 0) {
                parent.layer.msg("操作成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);

            } else {
                parent.layer.alert(data.msg)
            }

        }
    });

}

//输入校验
function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules: {
            replyContent: {
                required: true
            }
        },
        messages: {
            replyContent: {
                required: icon + "请输入回复信息"
            }
        }
    })
}

function cancelOption() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}