
var prefix = "/operate/userStatistics"
$(function() {
   // load();
});


var columns = [{
    checkbox : true

},
    {
        field : 'id',
        title : '用户id',
        visible: false
    },
    {
        field : 'userAccount',
        title : '用户账号',
        formatter:function geTel(value){
            var reg = /^(\d{3})\d{4}(\d{4})|(\d{6})$/;
            return value.replace(reg, "$1****$2");
        }

    },
    {
        field : 'cardType',
        title : '用户卡类型',
        formatter: function(value){
            if(value == 1){
                return "物联卡";
            }
            if(value == 2){
                return "大网卡";
            }
        }
    },
    // {
    //     field : 'onlineStatus',
    //     title : '用户状态',
    //     formatter:function (value) {
    //         if (value==0){
    //             return "离线"
    //         } if (value==1){
    //             return "在线"
    //         }
    //     }
    //
    // },
    {
        field : 'provinceId',
        title : '归属省份'
    },
    {
        field : 'cityId',
        title : '归属城市'
    },
    {
        field : 'carrier',
        title : '运营商类型'
    },
    {
        field : 'subType',
        title : '渠道来源',
        formatter: function (value) {
            if (value=='3') {
                return "普通用户";
            }
            if (value == '5') {
                return "电渠用户";
            }
            if (value == '10001') {
                return "toB来源用户";
            }
            if (value == '10002') {
                return "网上商城销售卡";
            }
            if (value == '10003') {
                return "线下渠道";
            }
        }
    },
    {
        field : 'registerTime',
        title : '注册时间',
        formatter: function (value) {
            if (value != null) {
                value=value.replace(new RegExp(/-/gm) ,"/");
                var date = new Date(value);
                var result = date.getFullYear() + "-";
                if (date.getMonth() < 9) {
                    result = result + "0";
                }
                result = result + (date.getMonth() + 1) + "-";
                if (date.getDate() <= 9) {
                    result = result + "0";
                }
                result = result + date.getDate();
                return result;
            } else {
                return "";
            }
        }
    },
    {
        field : 'userType',
        title : '用户类型',
        formatter: function (value) {
            if (value==0 ||value==null){
                return "免费用户"
            }
            if (value==1){
                return "会员用户"
            }
        }
    },
    {
        field: 'serviceType',
        title: '付费类型',
        formatter: function (value) {
            if (value==1) {
                return "3元/月";
            }
            if (value == 2) {
                return "8元/季度";
            }
            if (value == 3) {
                return "15元/半年";
            }
            if (value == 4) {
                return "30元/年";
            }
        }
    },
    {
        field: 'payTime',
        title: '付费时间',
        formatter: function (value) {
            if (value != null) {
                value=value.replace(new RegExp(/-/gm) ,"/");
                var date = new Date(value);
                var result = date.getFullYear() + "-";
                if (date.getMonth() < 9) {
                    result = result + "0";
                }
                result = result + (date.getMonth() + 1) + "-";
                if (date.getDate() <= 9) {
                    result = result + "0";
                }
                result = result + date.getDate();
                return result;
            } else {
                return "";
            }
        }
    },
    {
        field: 'isUseable',
        title: '账号状态',
        formatter: function (value) {
            if (value == 1) {
                return "禁用";
            }
            if (value == 0) {
                return "正常";
            }
        }
    }, {
        field: 'isTest',
        title: '账号类型',
        formatter: function (value) {
            if (value == 1) {
                return "测试账号";
            }if (value == 0) {
                return "正式账号";
            }
        }
    },
    {
        field: 'belongGroup',
        title: '所属群组数',
        formatter : function (value, row) {
            if (value>0){
                return '<a class="" style="color:blue" href="#" title="所属群组数"  mce_href="#" ' +
                    'onclick="groupList(\'' + row.id + '\')">'+value+'</a> ';
            }else if (value == null){
                return '<a class="" style="color:blue" href="#" title="所属群组数"  mce_href="#">0</a> ';
            }
            else {
                return '<a class="" style="color:blue" href="#" title="所属群组数"  mce_href="#">'+value+'</a> ';
            }

        }

    },
    {
        field: 'loginTime',
        title: '登录时间',
        formatter: function (value) {
            if (value != null) {
                value=value.replace(new RegExp(/-/gm) ,"/");
                var date = new Date(value);
                var result = date.getFullYear() + "-";
                if (date.getMonth() < 9) {
                    result = result + "0";
                }
                result = result + (date.getMonth() + 1) + "-";
                if (date.getDate() <= 9) {
                    result = result + "0";
                }
                result = result + date.getDate();
                return result;
            } else {
                return "";
            }
        }
    }
    ,{
        field: 'createGroup',
        title: '创建群组数',
        formatter : function (value, row) {
            if (value == null){
                return 0;
            }else {
                return value;
            }

        }
    }
    ,{
        field: 'networkType',
        title: '网络类型'
    }
    ,{
        field: 'ip',
        title: '登录IP'
    }
    ,{
        field: 'loginFail',
        title: '连续登录失败次数'
    }
    // ,{
    //     field: 'riskLevel',
    //     title: '风险等级'
    // }
    // ,{
    //     field: 'sensitiveCount',
    //     title: '敏感词次数'
    // }
    ,{
        field: 'deviceId',
        title: '设备ID'
    }
    ,{
        field: 'manufacturer',
        title: '设备品牌'
    }
    ,{
        field: 'model',
        title: '设备品牌型号'
    }
    ,{
        field: 'os',
        title: '设备系统'
    }
    ,{
        field: 'osVersion',
        title: '设备系统版本'
    }
    ,{
        field: 'appVersion',
        title: '软件版本'
    }

    // ,{
    //     title : '账号操作',
    //     field: 'id',
    //     align : 'center',
    //     formatter: function (value, row, index) {
    //         var q = '<a class="btn btn-success btn-sm" href="#" title="解散"  mce_href="#" ' +
    //             'onclick="remove(\'' + row.id
    //             + '\')"><i class="fa fa-remove"></i></a> ';
    //         var d = '<a class="btn btn-warning btn-sm" href="#" title="禁言"  mce_href="#" ' +
    //             'onclick="lock(\'' + row.id
    //             + '\')"><i class="fa fa-key"></i></a> ';
    //         var f = '<a class="btn btn-success btn-sm" href="#" title="恢复"  mce_href="#" ' +
    //             'onclick="unLock(\'' + row.id
    //             + '\')"><i class="fa fa-key"></i></a> ';
    //         if(row.speechStatus == 0){
    //             return  q + d;
    //         }else{
    //             return q + f;
    //         }
    //     }
    // }
    ,{
        title : '账号操作',
        field: 'doAccount',
        align : 'center',
        formatter: function (value, row, index) {
            if (row.isTest == 0){
                var d = '<a class="btn btn-warning btn-sm" href="#" mce_href="#" ' +
                    'onclick="userBanned(\'' + row.id
                    + '\')">禁用</a> ';
                var f = '<a class="btn btn-success btn-sm" href="#" mce_href="#" ' +
                    'onclick="userRecover(\'' + row.id
                    + '\')">恢复</a> ';
                if(row.isUseable == 0){
                    return d;
                }else{
                    return f;
                }
            } else if(row.isTest == 1){
                var d = '<a class="btn btn-warning btn-sm" href="#" title="禁用"  mce_href="#" ' +
                    'onclick="lock(\'' + row.id
                    + '\')">禁用</a> ';
                var f = '<a class="btn btn-success btn-sm" href="#" title="恢复"  mce_href="#" ' +
                    'onclick="unLock(\'' + row.id
                    + '\')">恢复</a> ';
                if(row.isUseable == 0){
                    return d;
                }else{
                    return f;
                }
            }

        }
    }
];


//测试账号禁用
function lock(id) {
    layer.open({
        type: 2,
        title: '禁用账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '400px'],
        content: '/operate/testAccount/lock/' + id // iframe的url
    });
}

//测试账号解禁
function unLock(id) {
    layer.open({
        type: 2,
        title: '恢复账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '400px'],
        content: '/operate/testAccount/unlock/' + id // iframe的url
    });

}

function load() {
    $('#exampleTable')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : prefix + "/listActive", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                singleSelect : false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParamsType : "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams : function(params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber : params.pageNumber,
                        pageSize : params.pageSize,
                        userAccount: $('#userAccount').val(),
                        cardType: $('#cardType').val(),
                        provinceId: $('#provinceId').val(),
                        cityId: $('#cityId').val(),
                        isTest: $('#isTest').val(),
                        carrier: $('#carrier').val(),
                        subType: $('#subType').val(),
                        userType: $('#userType').val(),
                        loginFail: $('#loginFail').val(),
                        searchType:$('#searchType').val()
                    };
                },
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                responseHandler : function(res){
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns : columnsTemp
            });
}

//禁用功能
function userBanned(id) {
    layer.open({
        type: 2,
        title: '禁用提示',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '350px'],
        content: prefix + '/userBanned/'+id // iframe的url
    })
}

//恢复功能
function userRecover(id) {
    layer.open({
        type: 2,
        title: '恢复提示',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '400px'],
        content: prefix + '/userRecover/'+id // iframe的url
    })
}

function groupList(value){
    $.ajax({
        cache: true,
        type: "get",
        url: "/operate/groupStatistics/userOfGroup?id="+ value,
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {
            if (data.code == 0) {
                var url = "/operate/groupStatistics/groupList?userId=" + value;
                openTabPage(url,"所属群组详情");

            } else {
                parent.layer.alert(data.msg)
            }
        }
    });
}

function openTabPage(url, title) {
    var wpd = $(window.parent.document);
    var mainContent = wpd.find('.J_mainContent');
    var thisIframe = mainContent.find("iframe[data-id='"+ url +"']");
    var pageTabs = wpd.find('.J_menuTabs .page-tabs-content ');
    pageTabs.find(".J_menuTab.active").removeClass("active");
    mainContent.find("iframe").css("display", "none");
    if(thisIframe.length > 0){ // 选项卡已打开
        thisIframe.css("display", "inline");
        pageTabs.find(".J_menuTab[data-id='"+ url +"']").addClass("active");
    }else{
        var menuItem = wpd.find("a.J_menuItem[href='"+ url +"']");
        var dataIndex = title == undefined ? menuItem.attr("data-index") : '9999';
        var _title = title == undefined ? menuItem.find('.nav-label').text() : title;
        var iframe = '<iframe class="J_iframe" name="iframe'+ dataIndex +'" width="100%" height="100%" src="' + url + '" frameborder="0" data-id="' + url
            + '" seamless="" style="display: inline;"></iframe>';
        pageTabs.append(
            '<a href="javascript:;" class="J_menuTab active" data-id="'+url+'">' + _title + ' <i class="fa fa-times-circle"></i></a>');
        mainContent.append(iframe);
        //显示loading提示
        // var loading = top.layer.load();
        // mainContent.find('iframe:visible').load(function () {
        //     //iframe加载完成后隐藏loading提示
        //     top.layer.close(loading);
        // });
    }

}

function reLoad() {
    $('#exampleTable').bootstrapTable('refresh');
}

//页面加载时执行列的更新
window.onload = function (ev) {

    //ajax请求从Redis数据库中取出leftdata、rightdataTemp
    $.ajax({
        url:prefix+"/getColumnInfo?showType=active",    //请求的url地址
        dataType:"json",   //返回格式为json
        async:false,//请求是否异步，默认为异步，这也是ajax重要特性
        //data:{"json":json},    //参数值
        type:"GET",   //请求方式
        success:function(data){
            //请求成功时处
            var json = data.data;
            var arr = JSON.parse(json);

            if(arr!=null && arr.length !=0) {
                leftdataTemp = arr[0];
                rightdataTemp = arr[1];
            }
            else {
                 leftdataTemp=leftdata ;
                 rightdataTemp = rightdata ;
            }
        },
        error:function(){
            alert("获取数据失败");
        }
    });


    refreshColumnsTemp();


    load();
}

//自定义列表项
//<div><input type="checkbox">用户账号</div>
var leftdata = [{"id":"userAccount","name":"用户账号"},{"id":"cardType","name":"用户卡类型"},
    {"id":"provinceId","name":"归属省份"},{"id":"cityId","name":"归属城市"},{"id":"carrier","name":"运营商类型"},
    {"id":"subType","name":"渠道来源"},{"id":"registerTime","name":"注册时间"},{"id":"userType","name":"用户类型"},
    {"id":"serviceType","name":"付费类型"},{"id":"payTime","name":"付费时间"},{"id":"isUseable","name":"账号状态"},
    {"id":"isTest","name":"账号类型"},{"id":"belongGroup","name":"所属群组数"}];
var rightdata= [{"id":"loginTime","name":"登录时间"}, {"id":"createGroup","name":"创建群组数"},{"id":"networkType","name":"网络类型"},
    {"id":"ip","name":"登录IP"},{"id":"loginFail","name":"连续登录失败次数"},{"id":"deviceId","name":"设备ID"},
    {"id":"manufacturer","name":"设备品牌"},{"id":"model","name":"设备品牌型号"},{"id":"os","name":"设备系统"},
    {"id":"osVersion","name":"设备系统版本"},{"id":"appVersion","name":"软件版本"}];
var leftdataTemp = new Array();
var rightdataTemp = new Array();
var columnsTemp = new Array();

//自定义列表
function handInput() {


//判断leftDataTemp是否为空，为空取默认的列，

/*    if(leftdataTemp.length==0) Object.assign(leftdataTemp,leftdata);
    if(rightdataTemp.length==0) Object.assign(rightdataTemp,rightdata);*/

    //清空全部子节点
    removeDivNodesLeft();
    removeDivNodesRight();
    //拼接节点
    jointLeftNodes(leftdataTemp);
    jointRightNodes(rightdataTemp);
    $("#selfModalId").modal("show");

}

//模态框的确定按钮
function selfConfirm(){

    // 1.修改table表格的展示顺序，2.将columnsTemp、leftdataTemp、rightdataTemp存入到Redis缓存
    var data = [leftdataTemp,rightdataTemp];

    var json = JSON.stringify(data);

    $.ajax({
        url:prefix+"/saveColumn",    //请求的url地址
        dataType:"json",   //返回格式为json
        async:false,//请求是否异步，默认为异步，这也是ajax重要特性
        data:{"json":json,showType:"active"},    //参数值
        type:"GET",   //请求方式
        success:function(data){
            //请求成功时处
            //alert(data.code);
        },
        error:function(){
            alert("自定义列保存失败");
        }
    });

    refreshColumnsTemp();
    location.reload();
    $("#selfModalId").modal('hide');

}

//左边的移动
/**
 *
 * @param directType
 */
function leftMove(directType){

    var parentNode = document.getElementById("leftList");
    var nodes=parentNode.getElementsByTagName("div");
    move(parentNode,nodes,directType,1);
}

//右边的移动

function rightMove(directType){
    var parentNode = document.getElementById("rightList");
    var nodes=parentNode.getElementsByTagName("div");
    move(parentNode,nodes,directType,2);

}



//点击改变颜色
/**
 *
 * @param obj
 * @param position  1表示改变左边的颜色
 */
function  changeColorLeft(obj){

    var nodes=document.getElementById("leftList").getElementsByTagName("div");
    for(var i = 0;i<nodes.length;i++){
        nodes[i].style.background='';
    };
    obj.parentNode.style.background = 'gray';
}


function  changeColorRight(obj){
    var nodes=document.getElementById("rightList").getElementsByTagName("div");
    for(var i = 0;i<nodes.length;i++){
        nodes[i].style.background='';
    };
    obj.parentNode.style.background = 'gray';

}

/**
 *
 * @param parentNode
 * @param nodes
 * @param directType 1表示向上移动  2表示向下移动
 * @param area  :1表示左边的，2：表示右边的
 */
function move(parentNode,nodes,directType,area){
    var index;
    //获取选中元素的位置
    for(var i = 0;i<nodes.length;i++){
        var color = nodes[i].style.background;
        if(color == 'gray'){
            index = i;
            break;
        }
    };

    if(directType == 1){
        if(index == 0) return;
        var node = nodes[index];
        parentNode.removeChild(nodes[index]);
        parentNode.insertBefore(node,nodes[index-1]);
        //左边的数据进行交换leftdata 第i号位置和第i-1号位置互换
        if(area ==1){
            var temp = leftdataTemp[i];
            leftdataTemp[i] = leftdataTemp[i-1];
            leftdataTemp[i-1] = temp;

        }else if(area == 2){
            var temp = rightdataTemp[i];
            rightdataTemp[i] = rightdataTemp[i-1];
            rightdataTemp[i-1] = temp;
        }

        //更新数组
    }else if (directType==2) {
        if(index ==nodes.length-1)return;
        var node = nodes[index];
        parentNode.removeChild(nodes[index]);
        //如果目标节点是最后一个节点
        if(index == nodes.length-1){
            parentNode.append(node);
        }else{
            parentNode.insertBefore(node,nodes[i+1]);
        }
        //更新左边数据来源
        if(area ==1){
            var temp = leftdataTemp[i];
            leftdataTemp[i] = leftdataTemp[i+1];
            leftdataTemp[i+1] = temp;
        }else if(area == 2){
            var temp = rightdataTemp[i];
            rightdataTemp[i] =rightdataTemp[i+1];
            rightdataTemp[i+1] = temp;
        }
    }

}

//左右移动
/**
 *
 * @param direction 1表示从右边移动到左边，2：表示从左边移动到右边
 */
function moveToSide(direction) {

    var rightParentNode = document.getElementById("rightList");
    var rightDivNodes = rightParentNode.getElementsByTagName("div");
    var rightInputNodes = rightParentNode.getElementsByTagName("input");
    var rightSpanNodes = rightParentNode.getElementsByTagName("span");


    var leftParentNode = document.getElementById("leftList");
    var leftDivNodes = leftParentNode.getElementsByTagName("div");
    var leftInputNodes = leftParentNode.getElementsByTagName("input");
    var leftSpanNodes = leftParentNode.getElementsByTagName("span");


    //splice()
    if(direction == 1){
        for(var i = rightInputNodes.length-1;i>=0;i--){
            if(rightInputNodes[i].checked){
                //去除可能存在的阴影
                rightDivNodes[i].style.background='';
                //去除勾选状态
                rightInputNodes[i].checked = false;
                //修改点击事件
                rightSpanNodes[i].onclick=function(){changeColorLeft(this)};
                //删除右边的节点
                var nodeTemp = rightDivNodes[i];
                rightParentNode.removeChild(rightDivNodes[i]);
                //在左边添加节点
                leftParentNode.append(nodeTemp);
                //将leftdataTemp和rightdataTemp的数据进行更新
                //添加到leftdataTemp
                leftdataTemp.push(rightdataTemp[i]);
                //删除rightdataTemp中数据
                rightdataTemp.splice(i,1);
            }
        }
    }else if (direction == 2){
        for(var i = leftInputNodes.length-1;i>=0;i--){
            if(leftInputNodes[i].checked){
                //去除可能存在的阴影
                leftDivNodes[i].style.background='';
                //去除勾选状态
                leftInputNodes[i].checked = false;
                //修改点击事件
                leftSpanNodes[i].onclick=function(){changeColorRight(this)};
                //删除左边的节点
                var nodeTemp = leftDivNodes[i];
                leftParentNode.removeChild(leftDivNodes[i]);
                //在左边添加节点
                rightParentNode.append(nodeTemp);
                //将leftdata和rightdataTemp的数据进行更新
                //添加到rightdataTemp
                rightdataTemp.push(leftdataTemp[i]);
                //删除leftdata中数据
                leftdataTemp.splice(i,1);

            }
        }
    }
}

//删除左边的节点
function removeDivNodesLeft(){
    //清空左边
    var leftParentNode = document.getElementById("leftList");
    var leftDivNodes = leftParentNode.getElementsByTagName("div");
    for(var i = leftDivNodes.length-1;i>=0;i--){
        leftParentNode.removeChild(leftDivNodes[i]);
    }

}
//删除右边的全部节点

function removeDivNodesRight(){
    //清空右边
    var rightParentNode = document.getElementById("rightList");
    var rightDivNodes = rightParentNode.getElementsByTagName("div");
    for(var i = rightDivNodes.length-1;i>=0;i--){
        rightParentNode.removeChild(rightDivNodes[i]);
    }
}



//左边检索
function searchLeft(ev){
    var nodesTemp = new Array();
    if(ev.keyCode == 13){
        removeDivNodesLeft();
        var value = $("#searchLeft").val();
        //遍历leftdataTemp查看
        for(var i = 0 ;i<leftdataTemp.length;i++){
            if(leftdataTemp[i].name.match(value)){
                nodesTemp.push(leftdataTemp[i]);
            }
        }
        if(nodesTemp.length==0)return;
        //拼接
        jointLeftNodes(nodesTemp);
    }
}
//右边检索
function searchRight(ev){
    var nodesTemp = new Array();
    if(ev.keyCode == 13){
        removeDivNodesRight();
        var value = $("#searchRight").val();
        //遍历rightdataTempTemp查看
        for(var i = 0 ;i<rightdataTemp.length;i++){
            if(rightdataTemp[i].name.match(value)){
                nodesTemp.push(rightdataTemp[i]);
            }
        }
        if(nodesTemp.length==0)return;
        //拼接
        jointRightNodes(nodesTemp);
    }
}






//拼接左边节点
function jointLeftNodes(data){

    var $leftUL = $("#leftList");
    //添加左边的数据
    for (var i = 0; i < data.length; i++) {
        var $myLi = $("<div><input type='checkbox' name='"+data[i].id+"'/><span onclick=\"changeColorLeft(this)\">"+data[i].name+"</span></div>");
        $leftUL.append($myLi);
    }
}

//拼接右边的节点
function jointRightNodes(data){
    var $rightUL = $("#rightList");
    //添加右边的数据
    for (var i = 0; i < data.length; i++) {
        var $myLi = $("<div><input type='checkbox' name='"+data[i].id+"'/><span onclick=\"changeColorRight(this)\">"+data[i].name+"</span></div>");
        $rightUL.append($myLi);
    }

}

function refreshColumnsTemp(){

    //根据展示的列leftdataTemp给columnsTemp赋值1、当leftdataTemp为空时取默认展示的列，2leftdata不为空
    columnsTemp.splice(0,columnsTemp.length);

        columnsTemp.push(columns[0]);
        columnsTemp.push(columns[1]);
        for(var i = 0 ;i<leftdataTemp.length ; i++){
            for(var j = 2;j<columns.length-1 ; j++){
                if(leftdataTemp[i].id == columns[j].field){
                    columnsTemp.push(columns[j]);
                    break;  //找到一个结束内层循环，只能找到一个列
                }
            }
        }
        //添加最后一个元素
        columnsTemp.push(columns[columns.length-1])





}






