var prefix = "/operate/testAccount";
$(function () {
    load();
});

function load() {
    $('#exampleTable')
        .bootstrapTable(
            {
                method: 'get', // 服务器数据的请求方式 get or post
                url: prefix + "/list", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize: 'outline',
                toolbar: '#exampleToolbar',
                striped: true, // 设置为true会有隔行变色效果
                dataType: "json", // 服务器返回的数据类型
                pagination: true, // 设置为true会在底部显示分页条
                singleSelect: false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize: 10, // 如果设置了分页，每页数据条数
                pageNumber: 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns: false, // 是否显示内容下拉框（选择显示的列）
                sidePagination: "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParamsType: "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams: function (params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber: params.pageNumber,
                        pageSize: params.pageSize,
                        beginTime: $('#beginTime').val(),
                        endTime: $('#endTime').val(),
                        userAccount: $('#userAccount').val(),
                        accountType: $('#accountType').val(),
                        serviceType: $('#serviceType').val(),
                        expireEndStatus: $('#expireEndStatus').val(),
                        isUseable: $('#isUseable').val()
                        // name:$('#searchName').val(),
                        // username:$('#searchName').val()
                    };
                },
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                responseHandler: function (res) {
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns: [
                    {
                        checkbox: true,
                        formatter: function(value,row,index){
                            /*if(row.accountType ==2){
                                return {disabled:true}
                            }*/
                        }
                    },
                    {
                        field: 'user_id',
                        title: 'ID',
                        visible:false
                    },
                    {
                        field: 'userAccount',
                        title: '测试账号'
                    },
                    {
                        field: 'accountType',
                        title: '用户卡类型',
                        formatter: function (value,row,index) {
                            if (value == 1) {
                                return "APP测试账号";
                            } else if (value == 2) {
                                return "物联卡号";
                            }
                        }
                    },
                    {
                        field: 'serviceType',
                        title: '用户类型',
                        formatter: function (value) {
                            if (value == 1) {
                                return "会员用户";
                            } else {
                                return "免费用户";
                            }
                        }
                    },
                    {
                        field: 'expireTime',
                        title: '账号到期时间',
                        formatter: function (value) {
                            if (value != null) {
                                value=value.replace(new RegExp(/-/gm) ,"/");
                                var date = new Date(value);
                                var result = date.getFullYear() + "-";
                                if (date.getMonth() < 9) {
                                    result = result + "0";
                                }
                                result = result + (date.getMonth() + 1) + "-";
                                if (date.getDate() <= 9) {
                                    result = result + "0";
                                }
                                result = result + date.getDate();
                                return result;
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: 'expireEndStatus',
                        title: '账号到期状态',
                        formatter: function (value) {
                            if (value == 1) {
                                return "未到期";
                            } else {
                                return "已过期";
                            }
                        }
                    },
                    {
                        field: 'serviceMonth',
                        title: '会员期限',
                        formatter: function(value,row){
                            if(row.serviceType == 1){
                                if(value != null){
                                    return value+"天";
                                }
                            }
                        }
                    },
                    {
                        field: 'userServiceendtime',
                        title: '会员到期时间',
                        formatter: function (value,row) {
                            if(row.serviceType == 1) {
                                if (value != null) {
                                    value=value.replace(new RegExp(/-/gm) ,"/");
                                    var date = new Date(value);
                                    var result = date.getFullYear() + "-";
                                    if (date.getMonth() < 9) {
                                        result = result + "0";
                                    }
                                    result = result + (date.getMonth() + 1) + "-";
                                    if (date.getDate() <= 9) {
                                        result = result + "0";
                                    }
                                    result = result + date.getDate();
                                    return result;
                                } else {
                                    return "";
                                }
                            }
                        }
                    },
                    {
                        field: 'testCreateTime',
                        title: '创建时间',
                        formatter: function (value) {
                            if (value != null) {
                                value=value.replace(new RegExp(/-/gm) ,"/");
                                var date = new Date(value);
                                var result = date.getFullYear() + "-";
                                if (date.getMonth() < 9) {
                                    result = result + "0";
                                }
                                result = result + (date.getMonth() + 1) + "-";
                                if (date.getDate() <= 9) {
                                    result = result + "0";
                                }
                                result = result + date.getDate();
                                return result;
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: 'isUseable',
                        title: '账号状态',
                        formatter: function (value) {
                            if (value == 0) {
                                return "正常";
                            } else {
                                return "已禁用";
                            }
                        }
                    },
                    {
                        title: '操作',
                        field: 'id',
                        align: 'center',
                        formatter: function (value, row, index) {
                            var e = '<a class="btn btn-primary btn-sm" href="#" mce_href="#" title="修改" ' +
                                'onclick="edit(\'' + row.user_id
                                + '\')">修改<!--<i class="fa fa-edit">--></i></a> ';

                             var d = '<a class="btn btn-warning btn-sm" href="#" title="禁用"  mce_href="#" ' +
                                'onclick="lock(\'' + row.user_id
                                 + '\')">禁用<!--<i class="fa fa-key"></i>--></a> ';
                             var f = '<a class="btn btn-success btn-sm" href="#" title="恢复"  mce_href="#" ' +
                                 'onclick="unLock(\'' + row.user_id
                                 + '\')">恢复<!--<i class="fa fa-key"></i>--></a> ';

                            var q = '<a class="btn btn-danger btn-sm" href="#" title="删除"  mce_href="#" ' +
                                'onclick="remove(\'' + row.user_id
                                + '\')">删除<!--<i class="fa fa-remove">--></i></a> ';
                            if(row.isUseable == 0){
                                return e + d + q  ;
                            }else{
                                return e + f + q;
                            }
                        }
                    }]
            });
}

function reLoad() {
    $('#exampleTable').bootstrapTable('refresh');
}

//修改到期时间
function edit(id) {
    layer.open({
        type: 2,
        title: '修改到期时间',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['800px', '440px'],
        content: prefix + '/edit/' + id // iframe的url
    });
}

//删除账号
function remove(id) {
    layer.open({
        type: 2,
        title: '删除账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '350px'],
        content: prefix + '/remove/' + id // iframe的url
    });
}

//禁用账号
function lock(id) {
    layer.open({
        type: 2,
        title: '禁用账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '350px'],
        content: prefix + '/lock/' + id // iframe的url
    });
}

//解禁账号
function unLock(id) {
    layer.open({
        type: 2,
        title: '恢复账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '350px'],
        content: prefix + '/unlock/' + id // iframe的url
    });

}

//生成APP账号
function addAppAccount() {
    layer.open({
        type: 2,
        title: '生成APP账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['800px', '500px'],
        content: prefix + '/addAppAccount' // iframe的url
    });
}

//绑定物联卡账号
function bindCardAccount(){
    layer.open({
        type: 2,
        title: '绑定物联卡账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['800px', '611px'],
        content: prefix + '/bindCardAccount' // iframe的url
    });
}
//导入物联卡账号
function importCardAccount(){
    layer.open({
        type: 2,
        title: '导入物联卡账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['600px', '360px'],
        content: prefix + '/importCardAccount' // iframe的url
    });
}

//重置密码
function batchResetPwd() {
    var rows = $('#exampleTable').bootstrapTable('getSelections'); // 返回所有选择的行，当没有选择的记录时，返回一个空数组
    if (rows.length == 0) {
        layer.msg("请选择数据");
        return;
    }
    var ids = new Array();
    // 遍历所有选择的行数据，取每条数据对应的ID

    for(var i = 0 ;i<rows.length;i++){
        ids[i] = rows[i].user_id;
        if(rows[i].accountType == 2){
            layer.msg("您勾选账号包含物联卡账号!请剔除后再重置");
            return;
        }
    }

    layer.open({
        type: 2,
        title: '重置密码',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['400px', '260px'],
        content: prefix + '/resetPwd?ids=' + ids // iframe的url
    });
}

function getCheckedRows(){

    var rows = $('#exampleTable').bootstrapTable('getSelections'); // 返回所有选择的行，当没有选择的记录时，返回一个空数组
    if (rows.length == 0) {
        layer.msg("请选择数据");
        return;
    }

    var ids = new Array();
    // 遍历所有选择的行数据，取每条数据对应的ID
    $.each(rows, function (i, row) {
        ids[i] = row['user_id'];
    });
    return ids.join();

}

/**
 * 批量修改
 */
function batchEdit(){
    var id = getCheckedRows();
    if(id ==null || id ==''){
        return;
    }

    layer.open({
        type: 2,
        title: '修改到期时间',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['800px', '440px'],
        content: prefix + '/batchEdit/' + id // iframe的url
    });
}

/**
 * 批量删除
 */
function  batchRemove() {
    var id = getCheckedRows();
    if(id ==null || id ==''){
        return;
    }
    layer.open({
        type: 2,
        title: '删除账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '350px'],
        content: prefix + '/batchRemove/' + id // iframe的url
    });
}

/**
 * 批量禁用
 */
function batchLock(){

    var rows = $('#exampleTable').bootstrapTable('getSelections'); // 返回所有选择的行，当没有选择的记录时，返回一个空数组
    if (rows.length == 0) {
        layer.msg("请选择数据");
        return;
    }

    var ids = new Array();
    // 遍历所有选择的行数据，取每条数据对应的ID   您勾选账号包含物联卡账号
    // 请剔除后再重置
    for(var i = 0 ;i<rows.length;i++){
        ids[i] = rows[i].user_id;
        if(rows[i].isUseable != "0"){
            layer.msg("您勾选账号包含已禁用账号，请剔除后再禁用");
            return;
        }
    }

    var id = ids.join();
    if(id == null || id == '')return;

    layer.open({
        type: 2,
        title: '禁用账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '350px'],
        content: prefix + '/batchLock/' + id // iframe的url
    });

}

function batchUnlock() {

    var rows = $('#exampleTable').bootstrapTable('getSelections'); // 返回所有选择的行，当没有选择的记录时，返回一个空数组
    if (rows.length == 0) {
        layer.msg("请选择数据");
        return;
    }

    var ids = new Array();
    // 遍历所有选择的行数据，取每条数据对应的ID   您勾选账号包含物联卡账号
    // 请剔除后再重置
    for(var i = 0 ;i<rows.length;i++){
        ids[i] = rows[i].user_id;
        if(rows[i].isUseable == "0"){
            layer.msg("您勾选账号包含未禁用账号，请剔除后再解禁");
            return;
        }
    }

    var id = ids.join();
    if(id == null || id == '')return;

    layer.open({
        type: 2,
        title: '恢复账号',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '350px'],
        content: prefix + '/batchUnlock/' + id // iframe的url
    });
}

