
var prefix = "/operate/testAccount";

layui.use('upload', function () {
    var upload = layui.upload;
    //执行实例
    upload.render({
        elem: '#btn-excel',
        url: prefix + '/saveImportCardAccount', //上传接口
        auto: false,
        data: { testId: ""},
        //,multiple: true
        bindAction: '#btn-excel-sure',
        size: 2048, //最大允许上传的文件大小 2M
        accept: 'file', //允许上传的文件类型
        exts:'xlsx',//只上传pdf文档
        done: function(res){
            if(res.code == 0){//成功的回调
                parent.layer.msg("导入成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);
            }else{
                parent.layer.alert(res.msg);
            }
        },
        error: function (r) {
            layer.msg(r.msg);
        }
    });
});

//下载excel模板
function downloadTemplate(){
    window.location.href = '/operate/importAccount/downloadTemplate?fileName=TestImportTemplate.xlsx'
}

function cancelOption() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}