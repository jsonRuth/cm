var prefix = "/operate/testAccount";

$().ready(function () {
    //validateRule();
    showAccountCount();
   /* checkUserExpireTime();
    checkAccounts();*/
  /*  $('#serviceMonth').keyup (function () {
        blul(this.value)
        updateShowTime(this.value);
    })*/

    $("#userExpireTime").bind("change",function(){
        checkUserExpireTime();
        dateCompare();
    });

    $("#serviceendtime").bind("change",function(){
            dateCompare();
            updateServiceMonth();

    });


    //绑定请输入账号改变事件
    $("#accounts").bind("change",function () {
        checkAccounts();
    });

});

/*$.validator.setDefaults({
    submitHandler: function () {
        save();
    }
});*/

function submitData(){

    checkUserExpireTime();
    checkAccounts();
    save();
}

function checkUserExpireTime(){
    var accountExpireTime = $("#userExpireTime").val();
    if(accountExpireTime ==null || accountExpireTime == ""){
        $("#accountExpireTimeId").text("请输入到期时间");
    }else{
        $("#accountExpireTimeId").text("");
    }
}

function checkAccounts(){
    var accountsValue = $("#accounts").val();
    if(accountsValue == null || accountsValue == ""){
        $("#accountsId").text("请输入账号");
    }else {
        $("#accountsId").text("");
    }

}




function save() {
    var serviceMonthId = $("#serviceMonthId").text();
    if(serviceMonthId !="")return;

    var accountExpireTimeId =  $("#accountExpireTimeId").text();
    if(accountExpireTimeId !="")return;

    var accountsId =  $("#accountsId").text();
    if(accountsId !="")return;



    $.ajax({
        cache: true,
        type: "POST",
        url: "/operate/testAccount/saveBindCardAccount",
        data: $('#signupForm').serialize(),// 你的formid
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {
            if (data.code == 0) {
                parent.layer.msg("操作成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);

            } else {
                parent.layer.alert(data.msg)
            }

        }
    });

}

//输入校验
function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules: {
            userExpireTime: {
                required: true
            },
            accounts: {
                required: true
            }
        },
        messages: {
            userExpireTime: {
                required: icon + "请输入到期时间"
            },
            accounts: {
                required: icon + "请输入账号"
            }
        }
    })
}

//设置账号录入换行
function setEnter() {
    document.getElementById('accounts').onkeydown = function () {
        if (this.value.length == 13) {
            this.value = this.value + '\n';
        }
        if (this.value.length > 13 * 2 && this.value.length % 14 == 13) {
            this.value = this.value + '\n';
        }
    }
}


//效验会员有限期是否为正整数
function blul(serviceMonth) {
    var  reg = /^[0-9]{1,4}$/ ;
    if(serviceMonth != ''){
        if(serviceMonth == 0){
            parent.layer.msg("会员有效期不能为0");
            $("#serviceMonth").val("");
            $("#serviceendtime").val("");
            return false;
        }
        if(!reg.test(serviceMonth)){
            parent.layer.msg("会员有效期为正整数");
            $("#serviceMonth").val("");
            $("#serviceendtime").val("");
            return false;
        }
    }

}

//显示输入了几个账号
function showAccountCount() {
    $('#accounts').keyup(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var inputRows = $("#accounts").val().split("\n").length; //获取行数
        if (keycode != '' && inputRows <= 10) { //小于10行
            $("#count").html(inputRows + "/10");//替换默认数量0
        }
    })
}

//显示输入有效期并生成到期时间
function formatDate(date, format) {
    if (!date) return;
    if (!format) format = "yyyy-MM-dd";
    switch(typeof date) {
        case "string":
            date = new Date(date.replace(/-/, "/"));
            break;
        case "number":
            date = new Date(date);
            break;
    }
    if (!date instanceof Date) return;
    var dict = {
        "yyyy": date.getFullYear(),
        "M": date.getMonth() + 1,
        "d": date.getDate(),
        "H": date.getHours(),
        "m": date.getMinutes(),
        "s": date.getSeconds(),
        "MM": ("" + (date.getMonth() + 101)).substr(1),
        "dd": ("" + (date.getDate() + 100)).substr(1),
        "HH": ("" + (date.getHours() + 100)).substr(1),
        "mm": ("" + (date.getMinutes() + 100)).substr(1),
        "ss": ("" + (date.getSeconds() + 100)).substr(1)
    };
    return format.replace(/(yyyy|MM?|dd?|HH?|ss?|mm?)/g, function() {
        return dict[arguments[0]];
    });
}
function updateShowTime(num) {
    var oldTime = new Date().getTime();
    if (num==""){
        $("#serviceendtime").val("");
        return true
    }
    if (num==1){
        var m = oldTime
        var timestamp4 = new Date(m);
        var date = formatDate(timestamp4, "yyyy-MM-dd");
        $("#serviceendtime").val(date);
        $("#serviceMonth").val(num);
        return true;
    }
    var number = (num-1)*86400000;
    var m = oldTime + number;
    var timestamp4 = new Date(m);
    var date = formatDate(timestamp4, "yyyy-MM-dd");
    $("#serviceendtime").val(date);
    $("#serviceMonth").val(num);
    return true;
}
//日期处理
/**
 * 更新天数
 */
function updateServiceMonth(){
    var valueS = $("#serviceendtime").val();
    if(valueS == null || valueS ==''){
        $("#serviceMonth").val("");
        return;
    }
    //当都不为空时计算
    if(valueS !=null &&valueS !=''){
        //会员事件和现在进行比较
        var nowTime = dateToString(new Date());
        var memberDays = getDays(valueS,nowTime);
        $("#serviceMonth").val(memberDays+1);
    }
}

/**
 * 账号到期日期和会员到期日期比较
 * @returns {boolean}
 */
function dateCompare(){
    var valueS = $("#serviceendtime").val();
    var valueE = $("#userExpireTime").val();
    if(valueS !=null &&valueS !=''&&valueE !=null && valueE !=''){
        var days = getDays(valueE,valueS);
        if(days<0){
            $("#serviceMonthId").text("日期有误：会员到期时间不能大于账号到期时间");
            return false;
        }else{
            $("#serviceMonthId").text("");
        }
    }else{
        $("#serviceMonthId").text("");
    }
    return true;
}

/**
 * 获取两个日期的差值
 * @param strDateStart
 * @param strDateEnd
 * @returns {number}
 */
function getDays(strDateStart,strDateEnd){
    var strSeparator = "-"; //日期分隔符
    var oDate1;
    var oDate2;
    var iDays;
    oDate1= strDateStart.split(strSeparator);
    oDate2= strDateEnd.split(strSeparator);
    var strDateS = new Date(oDate1[0], oDate1[1]-1, oDate1[2]);
    var strDateE = new Date(oDate2[0], oDate2[1]-1, oDate2[2]);
    iDays = parseInt((strDateS - strDateE ) / 1000 / 60 / 60 /24);//把相差的毫秒数转换为天数
    return iDays ;
}

/**
 *
 * @param date
 * @returns {string}
 */
function dateToString(date){
    var year = date.getFullYear();
    var month =(date.getMonth() + 1).toString();
    var day = (date.getDate()).toString();
    if (month.length == 1) {
        month = "0" + month;
    }
    if (day.length == 1) {
        day = "0" + day;
    }
    var dateTime = year + "-" + month + "-" + day;
    return dateTime;
}
