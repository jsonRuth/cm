$().ready(function () {

/*  validateRule();
    $('#serviceMonth').keyup (function () {
        updateShowTime(this.value);
    })*/
    //checkData();
    dateCompare();
    updateServiceMonth();

    $("#accountExpireTime").bind("change",function(){
        checkData();
        dateCompare();
    });

    $("#memberExpireTime").bind("change",function(){
        dateCompare();
        updateServiceMonth();
    });


});

/*$.validator.setDefaults({
    submitHandler: function () {
        checkServiceMonth();
    }
});*/

/*$("#signupForm").submit(function(){
    //checkData();
    update();
});*/
function submitData(){
    checkData();
    update();
}


function checkData(){
    var accountExpireTime = $("#accountExpireTime").val();
    if(accountExpireTime ==null || accountExpireTime == ""){
        $("#accountExpireTimeId").text("请输入到期时间");
    }else{
        $("#accountExpireTimeId").text("");
    }
}



function update() {

    var serviceMonthId = $("#serviceMonthId").text();
    if(serviceMonthId !="")return false;
    var accountExpireTimeId = $("#accountExpireTimeId").text();
    if(accountExpireTimeId !="")return false;

    $.ajax({
        cache: true,
        type: "POST",
        url: "/operate/testAccount/updateExpireTime",
        data: $('#signupForm').serialize(),// 你的formid
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {
            if (data.code == 0) {
                parent.layer.msg("修改成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);

            } else {
                parent.layer.alert(data.msg)
            }
        }
    });
}




//表单验证
function validateRule() {

    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        ignore: [],
        rules: {
            accountExpireTimeName: {
                required: true
            }
        },
        messages: {
            accountExpireTimeName: {
                required: icon + "请输入到期时间"
            }
        }
    })
}

//更新会员到期时间
function updateShowTime(value) {
    var  reg = /^[0-9]{1,4}$/ ;
    if (value==""){
        $('#memberExpireTime').val("");
        return true
    }
    if(reg.test(value)){
        var now = new Date();
        if (value==1){
            var targetMilliseconds = now.getTime() ;
            now.setTime(targetMilliseconds);
            $('#memberExpireTime').val(getDateFormat(now));
            return true
        }

        var targetMilliseconds = now.getTime() + 1000 * 60 * 60 * 24 * (value-1) ;
        now.setTime(targetMilliseconds);
        $('#memberExpireTime').val(getDateFormat(now));
        return true
    }
}

//会员有效期校验
function checkServiceMonth() {
    var oldServiceMonth = $('#oldServiceMonth').val();
    var serviceMonth = $('#serviceMonth').val();
    var  reg = /^[0-9]{1,4}$/ ;
    if(serviceMonth != ''){
        if(serviceMonth == 0){
            parent.layer.msg("会员有效期不能为0");
            return false;
        }
        if(!reg.test(serviceMonth)){
            parent.layer.msg("会员有效期为正整数");
            return false;
        }
    }


}

//设置日期格式
function getDateFormat(value) {
    var result = value.getFullYear() + "-";
    if (value.getMonth() < 9) {
        result = result + "0";
    }
    result = result + (value.getMonth() + 1) + "-";
    if (value.getDate() <= 9) {
        result = result + "0";
    }
    result = result + value.getDate();
    return result;
}

function getDays(strDateStart,strDateEnd){
    var strSeparator = "-"; //日期分隔符
    var oDate1;
    var oDate2;
    var iDays;
    oDate1= strDateStart.split(strSeparator);
    oDate2= strDateEnd.split(strSeparator);
    var strDateS = new Date(oDate1[0], oDate1[1]-1, oDate1[2]);
    var strDateE = new Date(oDate2[0], oDate2[1]-1, oDate2[2]);
    iDays = parseInt((strDateS - strDateE ) / 1000 / 60 / 60 /24);//把相差的毫秒数转换为天数
    return iDays ;
}



function dateCompare(){
    var valueS = $("#memberExpireTime").val();
    var valueE = $("#accountExpireTime").val();
    if(valueS !=null &&valueS !=''&&valueE !=null && valueE !=''){
        var days = getDays(valueE,valueS);
        if(days<0){
            $("#serviceMonthId").text("日期有误：会员到期时间不能大于账号到期时间");
            return false;
        }else{
            $("#serviceMonthId").text("");
        }
    }else{
        $("#serviceMonthId").text("");
    }
    return true;
}
/**
 * 更新天数
 */
function updateServiceMonth(){
    var valueS = $("#memberExpireTime").val();
    if(valueS == null || valueS ==''){
        $("#serviceMonth").val("");
        return;
    }
    //当都不为空时计算
    if(valueS !=null &&valueS !=''){
            //会员事件和现在进行比较
            var nowTime = dateToString(new Date());
            var memberDays = getDays(valueS,nowTime);
            //$("#serviceMonthId").text("");
            $("#serviceMonth").val(memberDays+1);
    }
}

Date.prototype.format = function (format) {
    var args = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
        "S": this.getMilliseconds()
    };
    if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var i in args) {
        var n = args[i];
        if (new RegExp("(" + i + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length));
    }
    return format;
};


 function dateToString(date){
    var year = date.getFullYear();
    var month =(date.getMonth() + 1).toString();
    var day = (date.getDate()).toString();
    if (month.length == 1) {
        month = "0" + month;
    }
    if (day.length == 1) {
        day = "0" + day;
    }
    var dateTime = year + "-" + month + "-" + day;
    return dateTime;
}



/**
 * 重置密码规则
 * @param min
 * @param max
 */
function pwdNew(min,max) {
    var returnStr = "",
        range = (max ? Math.round(Math.random() * (max-min)) + min : min),
        charStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for(var i=0; i<range; i++){
        var index = Math.round(Math.random() * (charStr.length-1));
        returnStr += charStr.substring(index,index+1);
    }
    var regNumber = /\d+/;
    var regString = /[a-zA-Z]+/;
    if(regNumber.test(returnStr) && regString.test(returnStr)){
        $("#resetPassword").val(returnStr);
    }else{
        pwdNew(6);
    }
}

function resetPassword() {
    pwdNew(6);
}