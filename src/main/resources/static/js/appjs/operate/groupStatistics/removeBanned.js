$().ready(function () {
    validateRule()

});

$.validator.setDefaults({
    submitHandler: function () {
        removeBanned2();
    }
});

function removeBanned2() {
    $.ajax({
        cache: true,
        type: "POST",
        url: "/operate/groupStatistics/removeBanned",
        data: $('#signupForm').serialize(),// 你的formid
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {

            if (data.code == 0) {
                parent.layer.msg("恢复成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);

            } else {
                parent.layer.alert(data.msg)
            }
        }
    });

}

//输入校验
function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules: {
            content: {
                required: true
            }
        },
        messages: {
            content: {
                required: icon + "请输入恢复理由!"
            }
        }
    })
}

function cancelOption() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}



