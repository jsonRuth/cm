
var prefix = "/operate/groupStatistics"
$(function() {
    load();
});

function load() {
    $('#exampleTable')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : prefix + "/listArrive", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                singleSelect : false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParamsType : "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams : function(params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber : params.pageNumber,
                        pageSize : params.pageSize,
                        id: $('#id').val(),
                        groupName: $('#groupName').val(),
                        createUser: $('#createUser').val(),
                        groupType: $('#groupType').val(),
                        cgSpeechStatus: $('#cgSpeechStatus').val(),
                        arriveType:$('#arriveTypeId').val()
                    };
                },
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                responseHandler : function(res){
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns : [
                    {
                        checkbox : true

                    },
                    {
                        field : 'id',
                        title : '群组id'

                    },
                    {
                        field : 'groupName',
                        title : '群组名称'
                    },
                    {
                        field : 'createTime',
                        title : '创建时间'
                    },
                    {
                        field : 'createUser',
                        title : '创建账号'
                    },
                    {
                        field : 'groupUsers',
                        title : '群组用户数',
                        formatter : function (value, row,index) {
                            if (value>0){
                                return '<a class="" style="color:blue" href="#" title="群组用户数"  mce_href="#" ' +
                                    'onclick="userList(\'' + row.id + '\')">'+value+'</a> ';
                            } else {
                                return '<a class="" style="color:blue" href="#" title="群组用户数"  mce_href="#" >'+value+'</a> ';
                            }

                        }
                    },
                    {
                        field : 'groupType',
                        title : '群组类型',
                        formatter: function(value){
                            if(value == 1){
                                return "小于10人";
                            }
                            if(value == 2){
                                return "10-30人";
                            }
                            if(value == 3){
                                return "大于30人";
                            }
                        }
                    },

                    {
                        field : 'cgSpeechStatus',
                        title : '对讲状态',
                        formatter: function(value){
                            if(value == 1){
                                return "正常";
                            }
                            if(value == 0){
                                return "禁言";
                            }
                        }
                    }
                    // {
                    //     field : 'onlineUsers',
                    //     title : '群组在线用户数'
                    // },
                    // {
                    //     field : 'offlineUsers',
                    //     title : '群组离线用户数'
                    // }
                    ,{
                        title : '操作',
                        field: 'id',
                        align : 'center',
                        formatter: function (value, row, index) {
                            var q = '<a class="btn btn-success btn-sm" href="#" mce_href="#" ' +
                                'onclick="dissolve(\'' + row.id
                                + '\')">解散</a> ';
                            var d = '<a class="btn btn-warning btn-sm" href="#" mce_href="#" ' +
                                'onclick="banned(\'' + row.id
                                + '\')">禁言</a> ';
                            var f = '<a class="btn btn-success btn-sm" href="#" mce_href="#" ' +
                                'onclick="removeBanned(\'' + row.id
                                + '\')">恢复</a> ';
                            if(row.cgSpeechStatus == 0){
                                return  q + f;
                            }else{
                                return q + d;
                            }
                        }
                    }
                ]
            });
}

//解散功能
function dissolve(id) {
    layer.open({
        type: 2,
        title: '解散群组',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '400px'],
        content: prefix + '/dissolve/'+id // iframe的url
    });

}

//禁言功能
function banned(id) {
    layer.open({
        type: 2,
        title: '禁言提示',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '400px'],
        content: prefix + '/banned/'+id // iframe的url
    });

}

//解除禁言功能
function removeBanned(id) {
    layer.open({
        type: 2,
        title: '恢复提示',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['500px', '400px'],
        content: prefix + '/removeBanned/'+id // iframe的url
    });

}
function userList(value){
    $.ajax({
        cache: true,
        type: "get",
        url: "/operate/userStatistics/userOfGroup?Cgid="+ value,
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {
            if (data.code == 0) {
                var url = "/operate/userStatistics/userList?Cgid=" + value;
                openTabPage(url,"群成员详情");

            } else {
                parent.layer.alert(data.msg)
                reLoad();
            }
        }
    });
}


function openTabPage(url, title) {
    var wpd = $(window.parent.document);
    var mainContent = wpd.find('.J_mainContent');
    var thisIframe = mainContent.find("iframe[data-id='"+ url +"']");
    var pageTabs = wpd.find('.J_menuTabs .page-tabs-content ');
    pageTabs.find(".J_menuTab.active").removeClass("active");
    mainContent.find("iframe").css("display", "none");
    if(thisIframe.length > 0){ // 选项卡已打开
        thisIframe.css("display", "inline");
        pageTabs.find(".J_menuTab[data-id='"+ url +"']").addClass("active");
    }else{
        var menuItem = wpd.find("a.J_menuItem[href='"+ url +"']");
        var dataIndex = title == undefined ? menuItem.attr("data-index") : '9999';
        var _title = title == undefined ? menuItem.find('.nav-label').text() : title;
        var iframe = '<iframe class="J_iframe" name="iframe'+ dataIndex +'" width="100%" height="100%" src="' + url + '" frameborder="0" data-id="' + url
            + '" seamless="" style="display: inline;"></iframe>';
        pageTabs.append(
            '<a href="javascript:;" class="J_menuTab active" data-id="'+url+'">' + _title + ' <i class="fa fa-times-circle"></i></a>');
        mainContent.append(iframe);
        //显示loading提示
        // var loading = top.layer.load();
        // mainContent.find('iframe:visible').load(function () {
        //     //iframe加载完成后隐藏loading提示
        //     top.layer.close(loading);
        // });
    }

}
function reLoad() {
    $('#exampleTable').bootstrapTable('refresh');
}


















