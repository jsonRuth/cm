var prefix = "/operate/importAccount";

$().ready(function () {
    validateRule();
    // setEnter();
    showAccountCount();

});

$.validator.setDefaults({
    submitHandler: function () {
        save();
    }
});

function save() {
    $.ajax({
        cache: true,
        type: "POST",
        url: "/operate/importAccount/saveAccount",
        data: $('#signupForm').serialize(),// 你的formid
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {
            if (data.code == 0) {
                parent.layer.msg("操作成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);

            } else {
                parent.layer.alert(data.msg)
            }

        }
    });

}
//效验会员有限期是否为正整数
function buyConut(buyCount) {
    if (!(/(^[1-9]\d*$)/.test(buyCount))) {
        parent.layer.msg('请输入的正整数');
        $("#buyCount").val("");
        return false;
    }
    else {
        return true;
    }
}

//输入校验
function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules: {
            buyCount: {
                required: true
            },
            accounts: {
                required: true
            }
        },
        messages: {
            buyCount: {
                required: icon + "请输入数量"
            },
            accounts: {
                required: icon + "请输入卡号"
            }
        }
    })
}

//设置账号录入换行
function setEnter() {
    document.getElementById('accounts').onkeydown = function () {
        if (this.value.length == 13) {
            this.value = this.value + '\n';
        }
        if (this.value.length > 13 * 2 && this.value.length % 14 == 13) {
            this.value = this.value + '\n';
        }
    }
}

//显示输入了几个账号
function showAccountCount() {
    // $('#accounts').on('keyup',function(){
    //     var length = this.value.length;
    //     var count = parseInt((length+1) / 14);
    //     $("#count").html(count + "/10");
    // })
    $('#accounts').keyup(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var inputRows = $("#accounts").val().split("\n").length; //获取行数
        if (keycode != '' && inputRows <= 10) { //小于10行
            $("#count").html(inputRows + "/10");//替换默认数量0
        }
    })
}

function cancelOption() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}