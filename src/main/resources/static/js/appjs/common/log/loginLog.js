
var prefix = "/common/loginLog"
$(function() {
    load();
});

function load() {
    $('#exampleTable')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : prefix + "/list", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                singleSelect : false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParamsType : "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams : function(params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber : params.pageNumber,
                        pageSize : params.pageSize,
                        userAccount: $('#userAccount').val(),
                        cardType: $('#cardType').val(),
                        provinceId: $('#provinceId').val(),
                        cityId: $('#cityId').val(),
                        isTest: $('#isTest').val(),
                        carrier: $('#carrier').val(),
                        subType: $('#subType').val(),
                        userType: $('#userType').val()
                    };
                },
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                responseHandler : function(res){
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns : [
                    {
                        checkbox : true

                    },
                    {
                        field : 'userAccount',
                        title : '用户账号',
                        formatter:function geTel(value){
                            var reg = /^(\d{3})\d{4}(\d{4})|(\d{6})$/;
                            return value.replace(reg, "$1****$2");
                        }

                    },
                    {
                        field : 'cardType',
                        title : '用户卡类型',
                        formatter: function(value){
                            if(value == 1){
                                return "物联卡";
                            }
                            if(value == 2){
                                return "大网卡";
                            }
                        }
                    },
                    {
                        field : 'provinceId',
                        title : '归属省份'
                    },
                    {
                        field : 'cityId',
                        title : '归属城市'
                    },
                    {
                        field : 'carrier',
                        title : '运营商类型'
                    },
                    {
                        field : 'subType',
                        title : '渠道来源',
                        formatter: function (value) {
                            if (value=='3') {
                                return "普通用户";
                            }
                            if (value == '5') {
                                return "电渠用户";
                            }
                            if (value == '10001') {
                                return "toB来源用户";
                            }
                            if (value == '10002') {
                                return "网上商城销售卡";
                            }
                            if (value == '10003') {
                                return "线下渠道";
                            }
                        }
                    },
                    {
                        field : 'registerTime',
                        title : '注册时间',
                        formatter: function (value) {
                            if (value != null) {
                                value=value.replace(new RegExp(/-/gm) ,"/");
                                var date = new Date(value);
                                var result = date.getFullYear() + "-";
                                if (date.getMonth() < 9) {
                                    result = result + "0";
                                }
                                result = result + (date.getMonth() + 1) + "-";
                                if (date.getDate() <= 9) {
                                    result = result + "0";
                                }
                                result = result + date.getDate();
                                return result;
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field : 'userType',
                        title : '用户类型',
                        formatter: function (value) {
                            if (value==0 ||value==null){
                                return "免费用户"
                            }
                            if (value==1){
                                return "会员用户"
                            }
                        }
                    },
                    {
                        field: 'loginTime',
                        title: '登录时间',
                        formatter: function (value) {
                            if (value != null) {
                                value=value.replace(new RegExp(/-/gm) ,"/");
                                var date = new Date(value);
                                var result = date.getFullYear() + "-";
                                if (date.getMonth() < 9) {
                                    result = result + "0";
                                }result = result + (date.getMonth() + 1) + "-";

                                if (date.getDate() <= 9) {
                                    result = result + "0";
                                }result = result + date.getDate()+" ";

                                if (date.getHours()<=9){
                                    result = result + "0";
                                }
                                result = result + date.getHours()+":";
                                if (date.getMinutes()<=9){
                                    result = result + "0";
                                }
                                result = result + date.getMinutes()+":";
                                if (date.getSeconds()<=9){
                                    result = result + "0";
                                }result = result + date.getSeconds();

                                return result;
                            } else {
                                return "";
                            }
                        }
                    },
                    {
                        field: 'isTest',
                        title: '账号类型',
                        formatter: function (value) {
                            if (value == 1) {
                                return "测试账号";
                            }if (value == 0) {
                                return "正式账号";
                            }
                        }
                    },
                ]
            });
}

function reLoad() {
    $('#exampleTable').bootstrapTable('refresh');
}


